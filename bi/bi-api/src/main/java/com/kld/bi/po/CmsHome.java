package com.kld.bi.po;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by anpushang on 2016/3/23.
 */
public class CmsHome implements Serializable {

    private static final long serialVersionUID = -6412336864460406894L;
    /**
     * id
     */
    private Integer brandIndexId;

    /**
     * cms内部使用
     */
    private String brandName;

    /**
     * banwei code
     */
    private String brandColumn;

    /**
     * 内容
     */
    private String brandContent;

    /**
     * 0 首页轮播-不选择商品，只是上传图片而已  1图片
     */
    private Integer brandType;

    /**
     * 0 pc 1wap 2，ios，3：andorid
     */
    private Integer site;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后修改人
     */
    private String lastModifiedUser;

    /**
     * 最后修改时间
     */
    private Date lastModifiedTime;

    /**
     * 0未启用 1启用
     */
    private Integer status;


    /**
     * 头部url
     */
    private String heardUrl;
    /***
     * 1,无，2，有 更多
     */
    private Integer isMore;
    /***
     * 要跳转的链接
     */
    private String moreTargetUrl;

    /***
     * 1，url，2，商品ID，3，类目id，4，关键字，5，专题id，6,圈子ID
     */
    private Integer moreTargetType;

    public Integer getBrandIndexId() {
        return brandIndexId;
    }

    public void setBrandIndexId(Integer brandIndexId) {
        this.brandIndexId = brandIndexId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandColumn() {
        return brandColumn;
    }

    public void setBrandColumn(String brandColumn) {
        this.brandColumn = brandColumn;
    }

    public String getBrandContent() {
        return brandContent;
    }

    public void setBrandContent(String brandContent) {
        this.brandContent = brandContent;
    }

    public Integer getBrandType() {
        return brandType;
    }

    public void setBrandType(Integer brandType) {
        this.brandType = brandType;
    }

    public Integer getSite() {
        return site;
    }

    public void setSite(Integer site) {
        this.site = site;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getHeardUrl() {
        return heardUrl;
    }

    public void setHeardUrl(String heardUrl) {
        this.heardUrl = heardUrl;
    }

    public Integer getIsMore() {
        return isMore;
    }

    public void setIsMore(Integer isMore) {
        this.isMore = isMore;
    }

    public String getMoreTargetUrl() {
        return moreTargetUrl;
    }

    public void setMoreTargetUrl(String moreTargetUrl) {
        this.moreTargetUrl = moreTargetUrl;
    }

    public Integer getMoreTargetType() {
        return moreTargetType;
    }

    public void setMoreTargetType(Integer moreTargetType) {
        this.moreTargetType = moreTargetType;
    }
}
