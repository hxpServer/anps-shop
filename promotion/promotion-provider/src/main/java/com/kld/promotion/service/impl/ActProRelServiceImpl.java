package com.kld.promotion.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.product.po.Product;
import com.kld.promotion.api.IActProRelService;
import com.kld.promotion.dao.ActProRelDao;
import com.kld.promotion.po.ActProRel;
import com.kld.promotion.po.Actorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * todo ..insertWithTrans   setActProRule
 */
@Service("actProRelService")
public class ActProRelServiceImpl implements IActProRelService {

    Logger logger = LoggerFactory.getLogger(ActProRelServiceImpl.class);

    @Resource
    private ActProRelDao actProRelDao;

    @Override
    public int insertSelective(ActProRel record) {
        return actProRelDao.insert("insertSelective",record);
    }

    @Override
    public ActProRel selectByPrimaryKey(BigDecimal id) {
        return actProRelDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(ActProRel record) {
        return actProRelDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public List<ActProRel> getActprorelByActid(Integer actid) {
        return actProRelDao.find("getActprorelByActid",actid);
    }

    @Override
    public List<ActProRel> getActProList(Map<String, Object> map) {
        return actProRelDao.find("getActProList",map);
    }
    @Override
    public PageInfo getActProList(Map<String, Object> map, int PageNum, int PageSize){
        PageHelper.startPage(PageNum,PageSize);
        List<Actorder> list = actProRelDao.find("getActProList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @Override
    public List<Product> getProList(Map<String, Object> map) {
        return actProRelDao.find("getProList",map);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = {Exception.class})
    public ResultMsg insertWithTrans(BigDecimal actid, List<Integer> proids, String username) throws Exception{
        // TODO: 2016/3/28 商品相关。
        ResultMsg rmsg = new ResultMsg();
//        Integer n = 0;
//        for(Integer id:proids){
//            HashMap<String,Object> map = new HashMap<>();
//            map.put("actid",actid);
//            map.put("id",BigDecimal.valueOf(id));
//            ActProRel appr = actProRelDao.get("getActPro",map );
//            int i = 0 ;
//            //如果已添加，无须再次添加
//            if(appr==null) {
//                Product p = productService.getProductById(id);
//                if(p!=null) {
//                    ActProRel apr = new ActProRel();
//                    apr.setActid(actid);
//                    apr.setPoints(BigDecimal.valueOf(p.getPoints()));
//                    apr.setProductid(BigDecimal.valueOf(id));
//                    apr.setCreatetime(new Date());
//                    apr.setCreator(username);
//                    apr.setNum(BigDecimal.valueOf(0));
//                    apr.setSalenum(BigDecimal.valueOf(0));
//                    apr.setIsdel(BigDecimal.valueOf(0));
//                    apr.setDiscountrate(BigDecimal.valueOf(100));  //活动优惠率默认100
//                    if(p.getMctoriginprice()!=null && p.getMctoriginprice().doubleValue()>0) {
//                        apr.setMctoriginprice(new BigDecimal(new DecimalFormat("#.##").format(p.getMctoriginprice() * 1.07)));
//                        apr.setMctoriginpoints(new BigDecimal(new PointPriceServiceImpl().suggestPoints(apr.getMctoriginprice().doubleValue())));
//                    }else {
//                        apr.setMctoriginprice(BigDecimal.valueOf(0));
//                        apr.setMctoriginpoints(BigDecimal.valueOf(0));
//                    }
//                    i = insertSelective(apr);
//                }
//            }else {
//                i=1;
//            }
//            n+=i;
//        }
//        if(n!=proids.size()){
//            logger.error("活动商品添加失败：实际商品添加数与预期数不一致");
//            throw new Exception("活动添加失败");
//        }else {
//            rmsg.setResult(true);
//            rmsg.setMsg("添加成功");
//        }
        return rmsg;
    }

    @Override
    public ResultMsg deleteLogic(String idsStr) {
        ResultMsg rmsg = new ResultMsg();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            String[] idsArr = idsStr.split(",");
            for (String id : idsArr) {
                ids.add(Integer.parseInt(id));
            }
        }catch (Exception ex){
            logger.error("删除活动失败，参数有误："+ex.getMessage(),ex);
            rmsg.setResult(false);
            rmsg.setMsg("删除失败，参数有误");
            return rmsg;
        }
        if(ids.size()==0){
            rmsg.setResult(false);
            rmsg.setMsg("未选择要删除的记录");
            return rmsg;

        }
        int ii = actProRelDao.delete("deleteLogic",ids);
        if(ii>0){
            rmsg.setResult(true);
            rmsg.setMsg("删除成功");
        }else {
            rmsg.setResult(false);
            rmsg.setMsg("删除失败");
        }
        return rmsg;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public ResultMsg setActProRule(String idsStr, BigDecimal discountrate, BigDecimal points, BigDecimal num) throws Exception{
        ResultMsg rmsg = new ResultMsg();
        // TODO: 2016/3/28 product is not done...
//        List<Integer> ids = new ArrayList<Integer>();
//        try {
//            String[] idsArr = idsStr.split(",");
//            for (String id : idsArr) {
//                ids.add(Integer.parseInt(id));
//            }
//        }catch (Exception ex){
//            logger.error("设置促销商品规则失败，参数有误："+ex.getMessage(),ex);
//            rmsg.setResult(false);
//            rmsg.setMsg("设置失败，参数有误");
//            return rmsg;
//        }
//        if(ids.size()==0){
//            rmsg.setResult(false);
//            rmsg.setMsg("未选择要设置的记录");
//            return rmsg;
//        }
//
//        if(discountrate.intValue()==0 && points.intValue()==0){
//            rmsg.setResult(false);
//            rmsg.setMsg("优惠率和优惠积分不能同时为0");
//            return rmsg;
//        }
//        int n = 0;
//        Activity activity = null;
//        for(Integer id:ids){
//            ActProRel apr = actProRelDao.get("selectByPrimaryKey",BigDecimal.valueOf(id));
//            if(n==0){ activity = activityService.selectByPrimaryKey(apr.getActid());}
//            if(discountrate!=null && discountrate.doubleValue()>0){
//                Product p = productService.getProductById(apr.getProductid().intValue());
//                points = new BigDecimal(new PointPriceServiceImpl().suggestPoints(p.getMctprice() * discountrate.doubleValue() / 100));
//            }
//            apr.setDiscountrate(discountrate);
//            apr.setNum(num);
//            apr.setPoints(points);
//            apr.setModifytime(new Date());
//
//            int upd = updateByPrimaryKeySelective(apr);
//            if(upd>0) {
//                if (activity.getState().intValue() == EnumList.ActState.Launching.value) {
//                    //更新商品运营积分
//                    int upd2 = productService.updateActPoints(apr.getPoints().intValue(), apr.getActid().intValue(), apr.getProductid().intValue());
//                    if(upd2<=0){
//                        upd=0;
//                    }
//                }
//            }
//            n+=upd;
//        }
//        if(n!=ids.size()){
//            rmsg.setResult(false);
//            rmsg.setMsg("设置商品数与实际成功设置数结果不一致");
//            throw new Exception("活动商品设置异常");
//        }else {
//                rmsg.setResult(true);
//                rmsg.setMsg("设置成功");
//        }
        return rmsg;
    }

    @Override
    public List<ActProRel> pickRepeatActProducts(Integer actid) {
        List<ActProRel> list = actProRelDao.find("pickRepeatActProducts",actid);
        return list==null?new ArrayList<ActProRel>():list;
    }

    @Override
    public ResultMsg setActProWeight(Integer id, BigDecimal weight) {
        ActProRel actProRel = selectByPrimaryKey(BigDecimal.valueOf(id));
        ResultMsg rmsg = new ResultMsg();
        if(actProRel==null || actProRel.getIsdel().intValue()==1){
            rmsg.setResult(false);
            rmsg.setMsg("促销商品不存在");
            return rmsg;
        }
        actProRel.setWeight(weight);
        int upd = updateByPrimaryKeySelective(actProRel);
        if(upd>0){
            rmsg.setResult(true);
            rmsg.setMsg("设置成功");
        }else {
            rmsg.setResult(false);
            rmsg.setMsg("设置失败");
        }
        return rmsg;
    }

    @Override
    public ResultMsg cancelActProWeight(List<Integer> ids) {
        ResultMsg rmsg = new ResultMsg();
        if(ids.size()==0){
            rmsg.setResult(false);
            rmsg.setMsg("未选择商品");
            return rmsg;
        }
        int r = actProRelDao.delete("cancelActProWeight",ids);
        if(r>0){
            rmsg.setResult(true);
            rmsg.setMsg("取消特推成功");
        }else {
            rmsg.setResult(false);
            rmsg.setMsg("取消特推成功");
        }
        return rmsg;
    }

    @Override
    public List<ActProRel> getActProTradeList(Map<String, Object> map) {

        return actProRelDao.find("getActProTradeList",map);
    }
}
