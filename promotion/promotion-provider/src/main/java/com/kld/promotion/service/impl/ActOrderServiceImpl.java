package com.kld.promotion.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.Page;
import com.kld.promotion.api.IActOrderService;
import com.kld.promotion.dao.ActorderDao;
import com.kld.promotion.po.Actorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 * updateActOrder 没有实现
 */

@Service("actOrderService")
public class ActOrderServiceImpl implements IActOrderService {

    @Autowired
    private ActorderDao actorderDao;
    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return actorderDao.delete("deleteByPrimaryKey",id);
    }

    @Override
    public int insert(Actorder record) {
        return actorderDao.insert(record);
    }

    @Override
    public int insertSelective(Actorder record) {
        return actorderDao.insert("insertSelective",record);
    }

    @Override
    public Actorder selectByPrimaryKey(BigDecimal id) {
        return actorderDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(Actorder record) {
        return actorderDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public int updateByPrimaryKey(Actorder record) {
        return actorderDao.update("updateByPrimaryKey",record);
    }

    @Override
    public List<Actorder> getPointsDrawList(Map<String, Object> map) {
        return actorderDao.find("getPointsDrawList",map);
    }

    @Override
    public PageInfo getPointsDrawList(Map<String, Object> map,int PageNum,int PageSize) {
        PageHelper.startPage(PageNum,PageSize);
        List<Actorder> list = actorderDao.find("getPointsDrawList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
    @Override
    public ResultMsg updateActOrder(Actorder actorder) throws Exception {
        // TODO: 2016/3/24  需要jdserver。
//        ResultMsg rmsg = new ResultMsg();
//        int ii = updateByPrimaryKeySelective(actorder);//更新活动订单
//        if(ii>0){
//            Actlog actlog = new Actlog();
//            actlog.setId(actorder.getActlogid());
//            actlog.setState(new BigDecimal(2));//设置为已领取
//            int i = iActLogService.updateByPrimaryKeySelective(actlog);//更新日志
//            if (i<=0){
//                logger.error("更新日志领取状态失败");
//                throw new Exception("更新日志失败");
//            }
//            rmsg.setResult(true);
//            rmsg.setMsg("更新成功！");
//
//        }else {
//            logger.error("更新活动订单失败");
//            throw new Exception("更新失败");
//        }
//        return rmsg;
        return null;
    }

    @Override
    public PageInfo getMonthWinnerListPageList(Map<String, Object> map,int PageNum,int PageSize) {
        PageHelper.startPage(PageNum,PageSize);
        List<Actorder> list = actorderDao.find("getMonthWinnerList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
    @Override
    public List<Actorder> getMonthWinnerList(Map<String, Object> map){
        return actorderDao.find("getMonthWinnerList",map);
    }
    @Override
    public List<Actorder> getActOrderDetails(BigDecimal orderId) {
        return actorderDao.find("getActOrderDetails",orderId);
    }

    @Override
    public Integer deleteByActlogId(Integer actlogid) {
        return actorderDao.delete("deleteByActlogId",actlogid);
    }

    @Override
    public List<Actorder> getPointDrawDetailList(Map<String, Object> map) {
        return  actorderDao.find("getPointDrawDetailList",map);
    }
    @Override
    public PageInfo getPointDrawDetailList(Map<String, Object> map,int PageNum,int PageSize) {
        PageHelper.startPage(PageNum,PageSize);
        List<Actorder> list = actorderDao.find("getPointDrawDetailList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
}
