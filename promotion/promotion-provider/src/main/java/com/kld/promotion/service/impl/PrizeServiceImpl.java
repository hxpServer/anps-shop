package com.kld.promotion.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.util.DateUtils;
import com.kld.promotion.api.IPrizeService;
import com.kld.promotion.dao.*;
import com.kld.promotion.po.Prize;
import com.kld.common.framework.global.Global;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Dan on 2015/10/29.
 */
@Service("prizeService")
@Transactional(propagation = Propagation.REQUIRED)
public class PrizeServiceImpl implements IPrizeService {
    @Autowired
    private PrizeDao prizeDao;


    private boolean isDebugMode = Global.IsPaySkipModel;
    private Logger logger = LoggerFactory.getLogger(PrizeServiceImpl.class);

    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return prizeDao.delete("deleteByPrimaryKey",id);
    }

    @Override
    public int insert(Prize record) {
        return prizeDao.insert(record);
    }

    @Override
    public int insertSelective(Prize record) {

        record.setIsdel(BigDecimal.valueOf(0));
        record.setCreatetime(DateUtils.getDate());
        record.setCode(UUID.randomUUID().toString().replaceAll("-",""));
        return prizeDao.insert("insertSelective",record);
    }

    @Override
    public Prize selectByPrimaryKey(BigDecimal id) {
        return prizeDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(Prize record) {
        return prizeDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public int updateByPrimaryKey(Prize record) {
        return prizeDao.update("updateByPrimaryKey",record);
    }

    @Override
    public List<Prize> getPrizeList(Map<String, Object> map) {

        return prizeDao.find("getPrizeList",map);
    }

    @Override
    public PageInfo getPrizeList(Map<String, Object> map, int PageNum, int PageSize) {

        PageHelper.startPage(PageNum,PageSize);
        List<Prize> list = prizeDao.find("getPrizeList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
    /**
     * 批量删除（带事务）
     * @param ids
     * @return
     * @throws Exception
     */
    @Override
    public ResultMsg deleteLogicByIds(String ids) throws Exception{
        ResultMsg rmsg = new ResultMsg();
        String[] idArr = ids.split(",");
        if(idArr.length==0){
            rmsg.setResult(false);
            rmsg.setMsg("未选择活动");
            return rmsg;
        }
        List<Integer> idlist = new ArrayList<Integer>();
        for (String item : idArr) {
            Integer id = Integer.parseInt(item.trim());
            idlist.add(id);
        }
        if(idlist.size()<idArr.length){
            rmsg.setResult(false);
            rmsg.setMsg("未选择活动");
            return rmsg;
        }
        boolean hasUsed = checkPrizeUsed(idlist)>0?true:false;

        if(hasUsed){
            rmsg.setResult(false);
            rmsg.setMsg("选择奖品中有发布中活动正在使用，不允许删除");
            return rmsg;
        }

        int r = prizeDao.delete("deleteLogicByIds",idlist);
        if(r>0 && r==idArr.length){
            rmsg.setResult(true);
            rmsg.setMsg("删除成功");
        }else {
            throw new Exception("删除失败");
        }
        return rmsg;
    }

    @Override
    public List<Prize> queryTop4OrAllJiFenPrize(String flag) {
        return prizeDao.find("queryTop4OrAllJiFenPrize",flag);
    }

    @Override
    public Prize queryjiFenPrizeItemByActID(BigDecimal actID) {
        return prizeDao.get("queryjiFenPrizeItemByActID",actID);
    }


    @Override
    public boolean checkNameIfUsed(String name) {
        return (int)prizeDao.get("checkNameIfUsed",name)>0?true:false;
    }

    @Override
    public Prize selectByActItemID(BigDecimal actItemId) {
        return prizeDao.get("selectByActItemID",actItemId);
    }

    @Override
    public Integer checkPrizeUsed(List<Integer> ids) {
        return prizeDao.get("checkPrizeUsed",ids);
    }

}
