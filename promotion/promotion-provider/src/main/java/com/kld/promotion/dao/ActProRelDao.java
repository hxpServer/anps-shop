package com.kld.promotion.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.promotion.po.ActProRel;
import org.springframework.stereotype.Repository;

@Repository
public class ActProRelDao extends SimpleDaoImpl<ActProRel>{

}
