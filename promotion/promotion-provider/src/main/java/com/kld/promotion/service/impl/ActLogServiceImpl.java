package com.kld.promotion.service.impl;

import com.kld.bi.po.Report;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.util.DateUtils;
import com.kld.promotion.api.IActLogService;
import com.kld.promotion.dao.ActivityDao;
import com.kld.promotion.dao.ActlogDao;
import com.kld.promotion.dao.PrizeDao;
import com.kld.promotion.po.Activity;
import com.kld.promotion.po.Actlog;
import com.kld.sys.api.ISysDictService;
import com.kld.sys.po.SysDict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/28.
 */
@Service("actLogService")
public class ActLogServiceImpl implements IActLogService {


    @Autowired
    ActlogDao actlogDao;
    @Autowired
    private PrizeDao prizeDao;
    @Autowired
    private ISysDictService iSysDictService;
    @Autowired
    private ActivityDao activityDao;


    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return actlogDao.delete(id);
    }

    @Override
    public int insert(Actlog record) {
        return actlogDao.insert(record);
    }

    @Override
    public int insertSelective(Actlog record) {
        record.setCreatetime(DateUtils.getDate());
        return actlogDao.insert("insertSelective",record);
    }

    @Override
    public Actlog selectByPrimaryKey(BigDecimal id) {
        return actlogDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(Actlog record) {
        return actlogDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public int updateByPrimaryKey(Actlog record) {
        return actlogDao.update("updateByPrimaryKey",record);
    }

    @Override
    public List<Actlog> getActlogList(Map<String, Object> map) {
        return actlogDao.find("getActlogList",map);
    }

    @Override
    public List<Actlog> getActlogs(Map<String, Object> map) {
        return actlogDao.find("getActlogs",map);
    }

    @Override
    public List<Actlog> getActlogListByActid(Map<String,Object> map) {
        return actlogDao.find("getActlogListByActid",map);
    }

    @Override
    public List<Actlog> getActlogListbyUserid(Map<String,Object> map) {
        return actlogDao.find("getActlogListbyUserid",map);
    }

    @Override
    public List<String> getMonthWinnerIds(Integer actid) {
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("actid",actid);
        return actlogDao.find("getMonthWinnerIds",map);
    }

    @Override
    public BigDecimal getJFSumPoints(BigDecimal actid,Integer minActLogId) {
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("actid",actid);
        map.put("minActLogId",minActLogId);
        BigDecimal sumPoints = actlogDao.get("getJFSumPoints",map);
        return  sumPoints==null?BigDecimal.valueOf(0):sumPoints;
    }


    @Override
    public Double getJFCost(BigDecimal actid) {
        return actlogDao.get("getJFCost",actid);
    }

    @Override
    public List<Actlog> getPointsActlogListBill(HashMap<String, Object> map) {
        return actlogDao.find("getPointsActlogListBill",map);
    }

    @Override
    public Integer updatePayState(BigDecimal id, int paystate) {
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("id",id);
        map.put("paystate",paystate);
        return actlogDao.update("updatePayState",map);
    }

    @Override
    public Integer updateCheckCard(BigDecimal id, String billDate) {
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("id",id);
        map.put("billDate",billDate);
        return actlogDao.get("updateCheckCard",map);
    }

    @Override
    public Integer updatePayStates(List<Integer> ids, String paystate) {
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("ids",ids);
        map.put("paystate",paystate);
        return actlogDao.update("updatePayStates",map);
    }

    @Override
    public Integer getLastWinLogId(BigDecimal actId) {
        Integer lastWinId = actlogDao.get("getLastWinLogId",actId);
        return lastWinId==null?0:lastWinId;
    }

    @Override
    public int updatepCardPaiedTime(Date cardPaiedTime, Integer id) {
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("id",id);
        map.put("cardPaiedTime",cardPaiedTime);
        return actlogDao.update("updatepCardPaiedTime",map);
    }

    @Override
    public List<Integer> getIdList() {
        return actlogDao.find("getIdList","");
    }

    @Override
    public Map<String, Object> getPointsAndNum(String billdate) {
        return actlogDao.get("getPointsAndNum",billdate);
    }

    @Override
    public List<Report> getActlogDataForReport(String billdate) {
        return actlogDao.find("getActlogDataForReport",billdate);
    }

    @Override
    public Actlog getTop1ActTimesByUserID(String userID) {
        return prizeDao.get("getTop1ActTimes",userID);
    }

    //首次注册登陆送砸金蛋机会
    @Override
    public ResultMsg insertByRegister(String userid, String username) {
        Map<String,Object> map=new HashMap<String, Object>();
        ResultMsg msg=new ResultMsg();
        Actlog actlog = new Actlog();
        SysDict dict = iSysDictService.getSysDictByAlias("GetChanceByFirstLogin");
        Activity act=activityDao.get("selectByPrimaryKey",new BigDecimal(dict.getValue()));
        if(act.getState().intValue()==2){
            map.put("userid", userid);
            map.put("actid", dict.getValue());
            map.put("acttype", 3);

            List<Actlog> actlogList=actlogDao.find("getActlogListbyUserid",map);
            if(actlogList.size()==0) {
                actlog.setActid(new BigDecimal(dict.getValue()));
                actlog.setUserid(userid);
                actlog.setActtype(new BigDecimal(3));
                actlog.setUsername(username);
                actlog.setTimes(new BigDecimal(1));
                actlog.setCreatetime(new Date());

                int i = actlogDao.insert("insertSelective",actlog);
                if (i > 0) {
                    msg.setResult(true);
                    msg.setMsg("您获得了一次砸金蛋的机会，恭喜！");
                } else {
                    msg.setResult(false);
                    msg.setMsg("对不起，由于系统故障，您没有获得砸金蛋的机会！");
                }
            }
        }else {
            msg.setResult(false);
            msg.setMsg("对不起，活动已结束！");
        }
        return msg;
    }
}
