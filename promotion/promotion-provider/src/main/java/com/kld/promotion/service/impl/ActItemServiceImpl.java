package com.kld.promotion.service.impl;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.util.DateUtils;
import com.kld.promotion.api.IActItemService;
import com.kld.promotion.api.IPrizeService;
import com.kld.promotion.dao.ActitemDao;
import com.kld.promotion.po.Actitem;
import com.kld.promotion.po.Prize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
@Service("actItemService")
public class ActItemServiceImpl implements IActItemService {

    @Autowired
    private ActitemDao actitemDao;

    @Autowired
    private IPrizeService prizeService;
    private Logger logger= LoggerFactory.getLogger(ActOrderServiceImpl.class);

    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return actitemDao.delete("deleteByPrimaryKey",id);
    }

    @Override
    public int insert(Actitem record) {
        return actitemDao.insert(record);
    }

    @Override
    public int insertSelective(Actitem record) {

        record.setIsdel(BigDecimal.valueOf(0));
        record.setCreatetime(DateUtils.getDate());
        return actitemDao.insert("insertSelective",record);
    }

    @Override
    public Actitem selectByPrimaryKey(BigDecimal id) {
        return actitemDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(Actitem record) {
        return actitemDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public int updateByPrimaryKey(Actitem record) {
        return actitemDao.update("updateByPrimaryKey",record);
    }

    @Override
    public List<Actitem> getActItemList(Map<String, Object> map) {
        return actitemDao.find("getActItemList",map);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = {Exception.class})
    public ResultMsg deleteLogic(Integer id) throws Exception{
        ResultMsg rmsg = new ResultMsg();
        Actitem actitem = selectByPrimaryKey(BigDecimal.valueOf(id));
        actitem.setId(BigDecimal.valueOf(id));
        actitem.setIsdel(BigDecimal.valueOf(1));
        int upd = actitemDao.update("updateByPrimaryKeySelective",actitem);
        if(upd>0){
            rmsg.setResult(true);
            rmsg.setMsg("删除成功");
        }
        else {
            throw new Exception("删除失败");
        }
        return rmsg;
    }

    @Override
    public int openActItem(List<Integer> ids) {
        return actitemDao.get("openActItem",ids);
    }

    @Override
    public List<Actitem> getActItemByActId(BigDecimal actid) {
        return actitemDao.find("getActItemListByActId",actid);
    }

    @Override
    public ResultMsg AddActitem(Actitem actitem) throws Exception {
        ResultMsg rmsg = new ResultMsg();
        Prize prize = prizeService.selectByPrimaryKey(actitem.getPrizeid());
        if(prize==null || prize.getIsdel().intValue()==1){
            rmsg.setResult(false);
            rmsg.setMsg("添加奖品失败，奖品不存在");
            return rmsg;
        }
        int r = insertSelective(actitem);
        if(r>0){
            rmsg.setResult(true);
            rmsg.setMsg("添加成功");
        }else {
            throw new Exception("添加失败");
        }
        return rmsg;
    }
}
