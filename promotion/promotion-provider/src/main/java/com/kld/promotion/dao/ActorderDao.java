package com.kld.promotion.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.promotion.po.Actorder;
import org.springframework.stereotype.Repository;

/**
 * Created by 曹不正 on 2016/3/24.
 */
@Repository
public class ActorderDao extends SimpleDaoImpl<Actorder> {

}
