package com.kld.promotion.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.promotion.po.Prize;
import org.springframework.stereotype.Repository;

/**
 * Created by 曹不正 on 2016/3/28.
 */
@Repository
public class PrizeDao extends SimpleDaoImpl<Prize> {
}
