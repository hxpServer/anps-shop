package com.kld.promotion.service.impl;

import com.kld.promotion.api.IActItemService;
import com.kld.promotion.api.IActivityService;
import com.kld.promotion.po.Actitem;
import com.kld.promotion.po.Activity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by 曹不正 on 2016/3/30.
 */

public class ActItemServiceImplTest {
    @Test
    public void testInsert() throws Exception {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
        IActivityService activityService = (IActivityService) context.getBean("activityService");
        Activity activity = new Activity();
        activity.setTitle("asd");
        activity.setType(new BigDecimal(4));
        activityService.insert(activity);
        activityService.insertSelective(activity);
        activityService.selectByPrimaryKey(new BigDecimal(1));
        activityService.updateByPrimaryKeySelective(activity);
        activityService.updateByPrimaryKeySelective(activity);
        activityService.getActList(null);
        activityService.deleteLogicByID(15);

    }

}