package com.kld.promotion.po;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by 曹不正 on 2016/3/24.
 * 用于封装前台查询奖品的信息
 */
public class PrizeInfo implements Serializable {
    private static final long serialVersionUID = -2704050629200463990L;
    private String grade;//中奖奖项,即月抽奖获奖等级

    private BigDecimal acttype;//中奖方式

    private String drawTime;//中奖时间

    private String remark;//活动备注

    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getDrawTime() {
        return drawTime;
    }

    public void setDrawTime(String drawTime) {
        this.drawTime = drawTime;
    }

    public BigDecimal getActtype() {
        return acttype;
    }

    public void setActtype(BigDecimal acttype) {
        this.acttype = acttype;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

}
