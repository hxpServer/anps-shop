package com.kld.promotion.po;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by anwentong on 2015/11/10.
 * 绑定油卡送砸金蛋机会，实体
 */
public class BindCard implements Serializable{

    private static final long serialVersionUID = -1911610492249066403L;
    private String userid;
    private String username;
    private String phone;
    private String cardno;
    private Date createtime;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }


    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}
