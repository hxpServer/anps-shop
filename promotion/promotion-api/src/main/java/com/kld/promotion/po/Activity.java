package com.kld.promotion.po;

import com.kld.product.po.Product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Activity implements Serializable {
    private static final long serialVersionUID = -1060847908919787483L;
    private BigDecimal id;

    private String title;

    private BigDecimal type;

    private BigDecimal points;

    private BigDecimal times;

    private BigDecimal winrate;

    private BigDecimal expireday;

    private Date starttime;

    private Date endtime;

    private String remark;

    private String creator;

    private Date createtime;

    private BigDecimal state;

    private BigDecimal pid;

    private String oucode;

    private BigDecimal isdel;

    private BigDecimal isshow;   //是否显示到前台

    private BigDecimal minpoints;  //月抽奖最低积分限制

    private BigDecimal mincount;  //月抽奖最低次数限制

    private BigDecimal filterflag;  //筛选条件逻辑标志（0-且；1或）

    private BigDecimal openstate;

    private Date publishtime; //月抽奖公布名单时间

    private List<Actitem> actitems;  //奖项内容

    private String itemsname; //奖品内容

    private  List<Product> actPro;//活动商品列表

    private String webTitleimg;   //web首页主题图

    private String webBackgroundimg;   //web首页背景图
    private String wxTitleimg;   //wx首页主题图

    private String wxBackgroundimg;   //wx首页背景图

    private String webItemBannerimg;   //web 落地页 banner

    private String webItemBackgroundimg;  //新加的web落地页列表banner

    private String webItemBackgroundcolor; // web落地页背景色图片

    private String webItemListBackgroundcolor; // web落地页商品列表背景色图片


    private String wxItemBannerimg;   //wx 落地页 banner

    private String wxItemBackgroundimg;  //新加的wx落地页banner

    private String wxItemBackgroundcolor; //wx落地页背景色图片

    private String wxItemListBackgroundcolor; // wx落地页商品列表背景色图片




    public String getWebItemListBackgroundcolor() {
        return webItemListBackgroundcolor;
    }

    public void setWebItemListBackgroundcolor(String webItemListBackgroundcolor) {
        this.webItemListBackgroundcolor = webItemListBackgroundcolor;
    }

    public String getWxItemListBackgroundcolor() {
        return wxItemListBackgroundcolor;
    }

    public void setWxItemListBackgroundcolor(String wxItemListBackgroundcolor) {
        this.wxItemListBackgroundcolor = wxItemListBackgroundcolor;
    }

    public String getWebItemBannerimg() {
        return webItemBannerimg;
    }

    public void setWebItemBannerimg(String webItemBannerimg) {
        this.webItemBannerimg = webItemBannerimg;
    }

    public String getWebItemBackgroundimg() {
        return webItemBackgroundimg;
    }

    public void setWebItemBackgroundimg(String webItemBackgroundimg) {
        this.webItemBackgroundimg = webItemBackgroundimg;
    }

    public String getWebItemBackgroundcolor() {
        return webItemBackgroundcolor;
    }

    public void setWebItemBackgroundcolor(String webItemBackgroundcolor) {
        this.webItemBackgroundcolor = webItemBackgroundcolor;
    }

    public String getWxItemBannerimg() {
        return wxItemBannerimg;
    }

    public void setWxItemBannerimg(String wxItemBannerimg) {
        this.wxItemBannerimg = wxItemBannerimg;
    }

    public String getWxItemBackgroundimg() {
        return wxItemBackgroundimg;
    }

    public void setWxItemBackgroundimg(String wxItemBackgroundimg) {
        this.wxItemBackgroundimg = wxItemBackgroundimg;
    }

    public String getWxItemBackgroundcolor() {
        return wxItemBackgroundcolor;
    }

    public void setWxItemBackgroundcolor(String wxItemBackgroundcolor) {
        this.wxItemBackgroundcolor = wxItemBackgroundcolor;
    }

    public String getWebTitleimg() {
        return webTitleimg;
    }

    public void setWebTitleimg(String webTitleimg) {
        this.webTitleimg = webTitleimg;
    }

    public String getWebBackgroundimg() {
        return webBackgroundimg;
    }

    public void setWebBackgroundimg(String webBackgroundimg) {
        this.webBackgroundimg = webBackgroundimg;
    }

    public String getWxTitleimg() {
        return wxTitleimg;
    }

    public void setWxTitleimg(String wxTitleimg) {
        this.wxTitleimg = wxTitleimg;
    }

    public String getWxBackgroundimg() {
        return wxBackgroundimg;
    }

    public void setWxBackgroundimg(String wxBackgroundimg) {
        this.wxBackgroundimg = wxBackgroundimg;
    }

    public List<Product> getActPro() {
        return actPro;
    }

    public void setActPro(List<Product> actPro) {
        this.actPro = actPro;
    }

    public String getItemsname() {
        itemsname = "";
        if(actitems!=null && actitems.size()>0){
            for(Actitem item:actitems){
                itemsname+=",";
                itemsname+=item.getPrizename();
            }
        }
        return itemsname.length()>0?itemsname.substring(1):"";
    }

    public List<Actitem> getActitems() {
        return actitems;
    }

    public void setActitems(List<Actitem> actitems) {
        this.actitems = actitems;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public BigDecimal getType() {
        return type;
    }

    public void setType(BigDecimal type) {
        this.type = type;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getTimes() {
        return times;
    }

    public void setTimes(BigDecimal times) {
        this.times = times;
    }

    public BigDecimal getWinrate() {
        return winrate;
    }

    public void setWinrate(BigDecimal winrate) {
        this.winrate = winrate;
    }

    public BigDecimal getExpireday() {
        return expireday;
    }

    public void setExpireday(BigDecimal expireday) {
        this.expireday = expireday;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public BigDecimal getPid() {
        return pid;
    }

    public void setPid(BigDecimal pid) {
        this.pid = pid;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public BigDecimal getIsdel() {
        return isdel;
    }

    public void setIsdel(BigDecimal isdel) {
        this.isdel = isdel;
    }

    public BigDecimal getMinpoints() {
        return minpoints;
    }

    public void setMinpoints(BigDecimal minpoints) {
        this.minpoints = minpoints;
    }

    public BigDecimal getMincount() {
        return mincount;
    }

    public void setMincount(BigDecimal mincount) {
        this.mincount = mincount;
    }

    public BigDecimal getFilterflag() {
        return filterflag;
    }

    public void setFilterflag(BigDecimal filterflag) {
        this.filterflag = filterflag;
    }

    public BigDecimal getOpenstate() {
        return openstate;
    }

    public void setOpenstate(BigDecimal openstate) {
        this.openstate = openstate;
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }

    public BigDecimal getIsshow() {
        return isshow;
    }

    public void setIsshow(BigDecimal isshow) {
        this.isshow = isshow;
    }

}
