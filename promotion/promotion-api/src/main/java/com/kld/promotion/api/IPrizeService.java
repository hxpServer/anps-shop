package com.kld.promotion.api;

import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.promotion.po.Prize;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface IPrizeService {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(Prize record);

    int insertSelective(Prize record);

    Prize selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Prize record);

    int updateByPrimaryKey(Prize record);

    List<Prize> getPrizeList(Map<String, Object> map);
    PageInfo getPrizeList(Map<String, Object> map, int PageNum, int PageSize);

    /**
     * 批量删除（带事务）
     * @param ids
     * @return
     * @throws Exception
     */
    ResultMsg deleteLogicByIds(String ids) throws Exception;


    List<Prize> queryTop4OrAllJiFenPrize(String flag);

    Prize queryjiFenPrizeItemByActID(BigDecimal actID);

    boolean checkNameIfUsed(String name);

    Prize selectByActItemID(BigDecimal actItemId);

    Integer checkPrizeUsed(List<Integer> ids);

}
