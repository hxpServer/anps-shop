package com.kld.promotion.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Actlog implements Serializable {
    private static final long serialVersionUID = 7230906882368419612L;
    private BigDecimal id;

    private BigDecimal actid;

    private BigDecimal actitemid;

    private String userid;

    private String cardno;

    private String username;

    private String phone;

    private BigDecimal acttype;

    private BigDecimal state;   //状态是否领取（1未领取，2已领取）

    private BigDecimal points;

    private BigDecimal iswon;

    private BigDecimal channel;

    private String creator;

    private Date createtime;

    private String cardtradeno;

    private BigDecimal orderid;  //参与月抽奖中奖（商品兑换订单号），注：来源于orders表

    private BigDecimal paystate;  //支付状态（-1未支付; 1支付中; 2已支付;）

    private String ischeckcard;//卡核心是否对账

//    private BigDecimal provinceid;
//
//    private BigDecimal cityid;
//
//    private BigDecimal countyid;
//
//    private BigDecimal townid;
//
//    private String address;

    private BigDecimal times;
    private Date billdate;   //卡核心对账日期


    private String belongareaid;  //消费卡归属地ID

    private String belongareaname; //消费卡归属地名称

    //    private String zip;

    private BigDecimal prizeid;   //奖品ID

    private String prizename;  //奖品名称

    private String oucode;//组织机构

    private BigDecimal actitemtype;  //奖项类型

    private String acttitle; //活动主题

    private BigDecimal prizetype; //奖品类型

    private String prizeimg; //奖品图片

    private String typename;//奖项名称

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getPrizeimg() {
        return prizeimg;
    }

    public void setPrizeimg(String prizeimg) {
        this.prizeimg = prizeimg;
    }

    public String getActtitle() {
        return acttitle;
    }

    public void setActtitle(String acttitle) {
        this.acttitle = acttitle;
    }

    public BigDecimal getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(BigDecimal prizetype) {
        this.prizetype = prizetype;
    }


    public BigDecimal getPrizeid() {
        return prizeid;
    }

    public void setPrizeid(BigDecimal prizeid) {
        this.prizeid = prizeid;
    }

    public String getPrizename() {
        return prizename;
    }

    public void setPrizename(String prizename) {
        this.prizename = prizename;
    }

    public BigDecimal getActitemtype() {
        return actitemtype;
    }

    public void setActitemtype(BigDecimal actitemtype) {
        this.actitemtype = actitemtype;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getActid() {
        return actid;
    }

    public void setActid(BigDecimal actid) {
        this.actid = actid;
    }

    public BigDecimal getActitemid() {
        return actitemid;
    }

    public void setActitemid(BigDecimal actitemid) {
        this.actitemid = actitemid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno == null ? null : cardno.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public BigDecimal getActtype() {
        return acttype;
    }

    public void setActtype(BigDecimal acttype) {
        this.acttype = acttype;
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getIswon() {
        return iswon;
    }

    public void setIswon(BigDecimal iswon) {
        this.iswon = iswon;
    }

    public BigDecimal getChannel() {
        return channel;
    }

    public void setChannel(BigDecimal channel) {
        this.channel = channel;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getOrderid() {
        return orderid;
    }

    public void setOrderid(BigDecimal orderid) {
        this.orderid = orderid;
    }

    public BigDecimal getTimes() {
        return times;
    }

    public void setTimes(BigDecimal times) {
        this.times = times;
    }

    public String getCardtradeno() {
        return cardtradeno;
    }

    public void setCardtradeno(String cardtradeno) {
        this.cardtradeno = cardtradeno;
    }

    public String getBelongareaid() {
        return belongareaid;
    }

    public void setBelongareaid(String belongareaid) {
        this.belongareaid = belongareaid;
    }

    public String getBelongareaname() {
        return belongareaname;
    }

    public void setBelongareaname(String belongareaname) {
        this.belongareaname = belongareaname;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public BigDecimal getPaystate() {
        return paystate;
    }

    public void setPaystate(BigDecimal paystate) {
        this.paystate = paystate;
    }

    public String getIscheckcard() {
        return ischeckcard;
    }

    public void setIscheckcard(String ischeckcard) {
        this.ischeckcard = ischeckcard;
    }

    public Date getBilldate() {
        return billdate;
    }

    public void setBilldate(Date billdate) {
        this.billdate = billdate;
    }

}
