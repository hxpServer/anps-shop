package com.kld.promotion.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Prize implements Serializable {
    private static final long serialVersionUID = -1250557339321201722L;
    private BigDecimal id;

    private String name;

    private BigDecimal type;

    private BigDecimal mctprice;

    private BigDecimal selfprice;

    private BigDecimal num;

    private String intro;

    private String remark;

    private String picurl;

    private String creator;

    private Date createtime;

    private BigDecimal salenum;

    private BigDecimal isdel;

    private String code;

    private String source;

    private String oucode;

    private BigDecimal tax;  //税率


    private BigDecimal points;//积分换大奖消耗积分

    private String cardNo;//交易卡号

    private BigDecimal prizetype;//奖品类型

    private String picurlstr; //图片完整链接

    public String getPicurlstr() {
        return this.picurl;
        // TODO: 2016/3/24 图片的路径。图片上传的方式不一样了吧。 
//        return PropertiesUtil.props.getProperty("imgUrl")+picurl;
    }

    public BigDecimal getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(BigDecimal prizetype) {
        this.prizetype = prizetype;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public BigDecimal getType() {
        return type;
    }

    public void setType(BigDecimal type) {
        this.type = type;
    }

    public BigDecimal getMctprice() {
        return mctprice;
    }

    public void setMctprice(BigDecimal mctprice) {
        this.mctprice = mctprice;
    }

    public BigDecimal getSelfprice() {
        return selfprice;
    }

    public void setSelfprice(BigDecimal selfprice) {
        this.selfprice = selfprice;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl == null ? null : picurl.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getSalenum() {
        return salenum;
    }

    public void setSalenum(BigDecimal salenum) {
        this.salenum = salenum;
    }

    public BigDecimal getIsdel() {
        return isdel;
    }

    public void setIsdel(BigDecimal isdel) {
        this.isdel = isdel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

}
