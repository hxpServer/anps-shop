package com.kld.promotion.api;

import com.github.pagehelper.PageInfo;
import com.kld.promotion.po.Actorder;
import com.kld.common.framework.dto.ResultMsg;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface IActOrderService {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(Actorder record);

    int insertSelective(Actorder record);

    Actorder selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Actorder record);

    int updateByPrimaryKey(Actorder record);

    List<Actorder> getPointsDrawList(Map<String, Object> map);
    PageInfo getPointsDrawList(Map<String, Object> map,int PageNum,int PageSize);

    ResultMsg updateActOrder(Actorder actorder) throws Exception;

    PageInfo getMonthWinnerListPageList(Map<String, Object> map,int PageNum,int PageSize);

    List<Actorder> getMonthWinnerList(Map<String, Object> map);

    List<Actorder> getActOrderDetails(BigDecimal orderId);

    Integer deleteByActlogId(Integer actlogid);

    List<Actorder> getPointDrawDetailList(Map<String,Object> map);
    PageInfo getPointDrawDetailList(Map<String,Object> map,int PageNum,int PageSize);
}
