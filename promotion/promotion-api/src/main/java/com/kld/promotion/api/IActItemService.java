package com.kld.promotion.api;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.promotion.po.Actitem;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface IActItemService {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(Actitem record);

    int insertSelective(Actitem record);

    Actitem selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Actitem record);

    int updateByPrimaryKey(Actitem record);

    List<Actitem> getActItemList(Map<String, Object> map);

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    ResultMsg deleteLogic(Integer id) throws Exception;

    int openActItem(@Param("ids") List<Integer> ids);

    List<Actitem> getActItemByActId(BigDecimal actid);

    ResultMsg AddActitem(Actitem actitem) throws Exception;

}
