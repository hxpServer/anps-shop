package com.kld.promotion.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Actitem implements Serializable {
    private static final long serialVersionUID = -1060847908919787483L;
    private BigDecimal id;

    private BigDecimal actid;

    private BigDecimal prizeid;

    private BigDecimal num;

    private BigDecimal saleNum;//已抽中的奖品数量

    private BigDecimal winrate;

    private BigDecimal type;

    private String creator;

    private Date createtime;

    private BigDecimal isdel;

    private String typename; //活动奖项名称

    private String prizename; //奖品名称

    private BigDecimal isopen;  //是否开奖

    private Date opentime; //开将时间

    private String cardTrandeNo;//扣除积分流水号

    private List<Actlog> actlogs; //抽奖记录

    public List<Actlog> getActlogs() {
        return actlogs;
    }

    public void setActlogs(List<Actlog> actlogs) {
        this.actlogs = actlogs;
    }

    public String getCardTrandeNo() {
        return cardTrandeNo;
    }

    public void setCardTrandeNo(String cardTrandeNo) {
        this.cardTrandeNo = cardTrandeNo;
    }

    public BigDecimal getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(BigDecimal saleNum) {
        this.saleNum = saleNum;
    }

    private String prizeimg;

    public String getPrizeimg() {
        return prizeimg;
    }

    public void setPrizeimg(String prizeimg) {
        this.prizeimg = prizeimg;
    }

    public String getPrizename() {
        return prizename;
    }

    public void setPrizename(String prizename) {
        this.prizename = prizename;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getActid() {
        return actid;
    }

    public void setActid(BigDecimal actid) {
        this.actid = actid;
    }

    public BigDecimal getPrizeid() {
        return prizeid;
    }

    public void setPrizeid(BigDecimal prizeid) {
        this.prizeid = prizeid;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getWinrate() {
        return winrate;
    }

    public void setWinrate(BigDecimal winrate) {
        this.winrate = winrate;
    }

    public BigDecimal getType() {
        return type;
    }

    public void setType(BigDecimal type) {
        this.type = type;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getIsdel() {
        return isdel;
    }

    public void setIsdel(BigDecimal isdel) {
        this.isdel = isdel;
    }

    public BigDecimal getIsopen() {
        return isopen;
    }

    public void setIsopen(BigDecimal isopen) {
        this.isopen = isopen;
    }

    public Date getOpentime() {
        return opentime;
    }

    public void setOpentime(Date opentime) {
        this.opentime = opentime;
    }

}
