package com.kld.third.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.kld.third.api.jingdong.IJdBaseService;
import com.kld.third.api.jingdong.IJdProductService;
import com.kld.third.dto.jingdong.JDSku;
import com.kld.third.dto.jingdong.JDSkuStockState;
import com.kld.third.dto.jingdong.constant.JDConstant;
import com.kld.third.dto.jingdong.constant.JDEnums;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dara on 2016/3/28.
 */
@Service
public class JdProductServiceImpl implements IJdProductService {

    @Autowired
    private IJdBaseService jdBaseService;

    /**
     *  {"result":
     [
     {"name":"池子名称", "page_num":1},
     {"name":"池子名称2", "page_num":2}
     ]
     }
     * @return
     */
    @Override
    public List<String> getProPoolNums() {
        List<String> pageNums = new ArrayList<String>();
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_GETPOOLNUM,""));
        if(jso.getBoolean("success")){
            JSONArray jArr = jso.getJSONArray("result");
            for(Object obj:jArr) {
                net.sf.json.JSONObject pagenumObj = net.sf.json.JSONObject.fromObject(obj);
                pageNums.add(pagenumObj.getString("page_num"));
            }
        }
        return pageNums;
    }

    @Override
    public String getSkuIds(String proPoolNum) {
        String dataParams = "&pageNum="+proPoolNum;
        net.sf.json.JSONObject jso =jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_GETSKUIDS, dataParams));
        return jso.getBoolean("success")?jso.getString("result"):"";
    }

    @Override
    public JDSku getProDetail(String skuId) {
        String dataParams="&sku="+skuId;
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_DETAIL,dataParams));
        if(jso.getBoolean("success")){
            return JSON.parseObject(jso.getString("result"),JDSku.class);
        }
        return null;
    }

    /**
     * {"result”:
     [
     {"skuId”:111111,”state”:1},
     {"skuId”:111111,”state”:0}
     ]
     }

     * @param skuIds
     * @return
     */
    @Override
    public List<JDSku> getProState(String skuIds) {
        String dataParams="&sku="+skuIds;
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_STATE,dataParams));
        if(jso.getBoolean("success")){
            String jsonArray = jso.getString("reslut");
            return JSON.parseArray(jsonArray,JDSku.class);
        }
        return null;
    }

    @Override
    public List<net.sf.json.JSONObject> getProImageList(String skuIds) {
        String dataParams="&sku="+skuIds;
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_IMAGES,dataParams));
        if(jso.getBoolean("success")){
            String jsoString = jso.getString("reslut");
            return JSON.parseArray(jsoString, net.sf.json.JSONObject.class);
        }
        return null;
    }

    @Override
    public List<JDSku> getProAreaLimit(String skuIds, Integer provinceid, Integer cityid, Integer countyid, Integer townid) {
        String dataParams="&skuIds="+skuIds
                +"&province="+provinceid
                +"&city="+cityid
                +"&county="+countyid
                +"&town="+townid;
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRODUCT_CHECKAREALIMIT,dataParams));
        if(jso.getBoolean("success")){
            String jsoString = jso.getString("reslut");
            return JSON.parseArray(jsoString, JDSku.class);
        }
        return null;
    }

    @Override
    public List<JDSkuStockState> getStockStateWithNum(String skuNums,Integer provinceid,Integer cityid,Integer countyid) {
        String area = provinceid+"_"+cityid+"_"+countyid;
        String dataParams = "&skuNums="+skuNums
                +"&area="+area;

        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_STOCK_CHECKSTATE_WITHNUM,dataParams));
        if(jso.getBoolean("success")){
            String jsoString = jso.getString("reslut");
            return JSON.parseArray(jsoString, JDSkuStockState.class);
        }
        return null;
    }

    @Override
    public List<JDSkuStockState> getStockState(String skuIds,Integer provinceid,Integer cityid,Integer countyid) {
        String area = provinceid+"_"+cityid+"_"+countyid;
        String dataParams = "&skuNums="+skuIds
                +"&area="+area;

        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_STOCK_CHECKSTATE,dataParams));
        if(jso.getBoolean("success")){
            String jsoString = jso.getString("reslut");
            return JSON.parseArray(jsoString, JDSkuStockState.class);
        }
        return null;
    }

    @Override
    public boolean hasStock(String skuId, Integer num, Integer provinceid,Integer cityid,Integer countyid) {
        String skuNums = "";
        List<JDSkuStockState> stockStates = getStockStateWithNum(skuNums,provinceid,cityid,countyid);
        if(stockStates!=null && stockStates.size()>0){
            return JDEnums.JDStockState.hasStock(stockStates.get(0).getStockStateId());
        }
        return false;
    }

    @Override
    public List<JDSku> getPrice(String skuIds) {
        String dataParams = "&skuNums="+skuIds;
        net.sf.json.JSONObject jso = jdBaseService.returnMsg(jdBaseService.returnMsg(JDConstant.URL_PRICE_,dataParams));
        if(jso.getBoolean("success")){
            String jsoString = jso.getString("reslut");
            return JSON.parseArray(jsoString, JDSku.class);
        }
        return null;
    }
}
