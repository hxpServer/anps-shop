package com.kld.third.dto.jingdong;

import java.io.Serializable;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDOrderItem implements Serializable{
    private static final long serialVersionUID = -7630165423881273239L;
    private String skuId; //商品编号
    private Integer num; //购买数量
    private String category; //类别
    private Double price;  //京东协议价
    private Double tax;  //京东进项税（税率，百分比前数值）
    private Double taxPrice;  //京东税价
    private Double nakedPrice;  //京东裸价

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(Double taxPrice) {
        this.taxPrice = taxPrice;
    }

    public Double getNakedPrice() {
        return nakedPrice;
    }

    public void setNakedPrice(Double nakedPrice) {
        this.nakedPrice = nakedPrice;
    }
}
