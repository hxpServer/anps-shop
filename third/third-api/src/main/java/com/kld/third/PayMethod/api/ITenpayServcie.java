package com.kld.third.PayMethod.api;

import com.kld.third.PayMethod.dto.TenPayDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by xiaohe on 2016/3/31.
 */
public interface ITenpayServcie {
    /**
     *财付通支付接口
     * @param tenPayDto
     * @return
     */
    public String  payRequest(TenPayDto tenPayDto,HttpServletRequest request, HttpServletResponse response)throws UnsupportedEncodingException;

    /**
     * 通知查询接口
     */
    public void payNotifyUrl(HttpServletRequest request, HttpServletResponse response)throws Exception;

    /**
     * 通过商户订单号查询订单信息
     * @return
     */
    public Map<String,String> normalOrderQuery(Map<String,String> map)throws Exception;
}
