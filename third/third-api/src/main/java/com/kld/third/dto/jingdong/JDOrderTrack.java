package com.kld.third.dto.jingdong;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDOrderTrack implements Serializable{
    private static final long serialVersionUID = 5121766716092238236L;
    private String msgTime; //配送信息操作时间（ 2013-09-25 09:03:53）
    private String  content; //配送信息操作内容

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
