package com.kld.third.dto.jingdong;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dara on 2016/3/28.
 */
public class JDArea implements Serializable {

    private static final long serialVersionUID = -3733627264462368097L;

    private Integer id;
    private String name;
    private List<JDArea> cArea;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JDArea> getcArea() {
        return cArea;
    }

    public void setcArea(List<JDArea> cArea) {
        this.cArea = cArea;
    }
}
