package com.kld.third.api.jingdong;

import net.sf.json.JSONObject;

/**
 * Created by kwg on 2016/3/29.
 */
public interface IJdBaseService {

    public String getAccessToken();

    public JSONObject returnMsg(String result);

    /**
     * 处理京东返回结果
     *
     * @param url
     * @param params
     * @return
     */
    public String returnMsg(String url, String params)  ;

}
