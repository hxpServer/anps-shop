package com.kld.third.dto.jingdong;

import java.io.Serializable;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDSku implements Serializable{
    private static final long serialVersionUID = -6728676009634341146L;
    private String sku;
    private String skuId;
    private String name;  //商品名称
    private String imagePath;  //主图地址
    private String weight;  //重量
    private String state;  //上下架状态（1为上架，0为下架）
    private String brandName;  //品牌名称
    private String productArea; //产地
    private String upc;   //条形码
    private String skuType;  //商品类别
    private String category;  //分类
    private String introduction; //详细介绍
    private String param;   //规格参数html
    private String wareQD;  //包装
    private Double jdPrice; //京东价格
    private Double price; //协议价格

    private Boolean isAreaRestrict;//地区是否受限

    public Boolean getIsAreaRestrict() {
        return isAreaRestrict;
    }

    public void setIsAreaRestrict(Boolean isAreaRestrict) {
        this.isAreaRestrict = isAreaRestrict;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductArea() {
        return productArea;
    }

    public void setProductArea(String productArea) {
        this.productArea = productArea;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSkuType() {
        return skuType;
    }

    public void setSkuType(String skuType) {
        this.skuType = skuType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getWareQD() {
        return wareQD;
    }

    public void setWareQD(String wareQD) {
        this.wareQD = wareQD;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Double getJdPrice() {
        return jdPrice;
    }

    public void setJdPrice(Double jdPrice) {
        this.jdPrice = jdPrice;
    }
}
