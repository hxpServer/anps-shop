package com.kld.third.dto.jingdong.constant;


import com.kld.third.api.conf.JDConfig;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDConstant {
    public  static  final String JD_TOKEN_Url= JDConfig.config.getProperty("jd_token_url").toString();
    public  static  final String JD_API_Url=JDConfig.config.getProperty("jd_api_url").toString();


    // 获取tokenURL
    public static final String URL_ACCESSTOKEN = JD_TOKEN_Url+"oauth2/access_token";
    public static final String URL_REFRESHTOKEN =JD_TOKEN_Url+"oauth2/refresh_token";

    //订单相关
    public static final String URL_ORDER_SUBMIT = JD_API_Url + "order/jdPriceSubmit"; //提交订单，锁库存接口
    public static final String URL_ORDER_COMFIRM = JD_API_Url + "order/confirmOrder"; //确认预占库存订单接口
    public static final String URL_ORDER_CANCEL = JD_API_Url + "order/cancel"; //取消未确认订单接口
    public static final String URL_ORDER_SELECTJDORDER = JD_API_Url + "order/selectJdOrder"; //查询京东订单信息接口
    public static final String URL_ORDER_SELECTJDORDERID_BYTHIRDORDER = JD_API_Url + "order/selectJdOrderIdByThirdOrder"; //订单反查接口
    public static final String URL_ORDER_TRACK = JD_API_Url + "order/orderTrack"; //查询订单配送信息接口

    //推送消息相关
    public static final String URL_MESSAGE_GET = JD_API_Url+"message/get"; //查询京东推送消息（每次最多99条记录）
    public static final String URL_MESSAGE_DEL = JD_API_Url+"message/del"; //删除推送消息

    //商品相关
    public static final String URL_PRODUCT_GETPOOLNUM=JD_API_Url+"product/getPageNum"; //批量获取商品池编码
    public static final String URL_PRODUCT_GETSKUIDS=JD_API_Url+"product/getSku"; //根据商品池批量获取商品SKUID
    public static final String URL_PRODUCT_DETAIL=JD_API_Url+"product/getDetail"; //根据SKUID查询商品详情
    public static final String URL_PRODUCT_STATE=JD_API_Url+"product/skuState"; //查询商品状态
    public static final String URL_PRODUCT_IMAGES=JD_API_Url+"product/skuImage"; //查询商品图片列表
    public static final String URL_PRODUCT_CHECKAREALIMIT=JD_API_Url+"product/checkAreaLimit"; //批量查询商品区域购买限制

    //商品库存相关
    public static final String URL_STOCK_CHECKSTATE_WITHNUM = JD_API_Url +"stock/getNewStockById"; //批量查询商品库存状态（带库存剩余数量，适用于商品详情、下单） remainNum 剩余数量 -1未知；当库存小于5时展示真实库存数量
    public static final String URL_STOCK_CHECKSTATE = JD_API_Url +"stock/getStockById"; //批量查询商品库存状态（不带库存剩余数量，适用于列表）

    //商品价格相关
    public static final String URL_PRICE_ = JD_API_Url+"price/getJdPrice"; //批量获取京东价格

    //京东地址相关
    public static final String URL_AREA_PROVINCE = JD_API_Url +"area/getCity"; //获取省份列表
    public static final String URL_AREA_CITY = JD_API_Url +"area/getCity"; //获取城市列表
    public static final String URL_AREA_COUNTY = JD_API_Url +"area/getCounty"; //获取县/区列表
    public static final String URL_AREA_TOWN = JD_API_Url +"area/getTown"; //获取镇/街道列表


}
