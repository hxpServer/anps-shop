package com.kld.third.dto.jingdong;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDPOrder implements Serializable{
    private static final long serialVersionUID = -6719215384298902782L;
    private String  jdOrderId; //京东订单号
    private String thirdOrder; //第三方的订单单号
    private String sku; //[{"skuId":"skuId": "skuId": "skuId":"skuId":商品编号 商品编号 , "num":, "num": , "num": , "num":, "num":, "num":商品数量 商品数量 }](最高支持50种商品)
    private String name; //收货人
    private Integer province; //一级地址
    private Integer city; //二级地址
    private Integer county;//三级地址
    private Integer town;//四级地址 (如果该地区有四级地址，则必须传递四级地址)
    private String address; //详细地址
    private String zip;  //邮编
    private String phone; //座机号 (与mobile其中一个有值即可)
    private String mobile; //手机号 （与phone其中一个有值即可）
    private String email; //邮箱
    private String remark; //备注（少于100字）
    private Integer invoiceState;  //开票方式(1为随货开票，0为订单预借，2为集中开票 )
    private Integer invoiceType;  //1普通发票2增值税发票
    private Integer selectedInvoiceTitle; //发票类型：4个人，5单位
    private String companyName;  //发票抬头 (如果selectedInvoiceTitle=5则此字段必须)
    private Integer invoiceContent; //1:明细，3：电脑配件，19:耗材，22：办公用品 备注:若增值发票则只能选1 明细
    private Integer paymentType;  //支付类型， 101金融支付
    private Integer state; // 物流状态 0 是新建 1是妥投 2是拒收
    private Integer submitState;  //(0为未确认下单订单 1为确认下单订单) 是否预占库存，0是预占库存（需要调用确认订单接口），1是不预占库存 金融支付必须预占库存传0
    private Integer orderState; //订单状态 0为取消订单 1为有效
    private String invoiceName; //增值票收票人姓名  备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private String invoicePhone;  //增值票收票人电话 备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private String invoiceProvice; //增值票收票人所在省 备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private String invoiceCity; //增值票收票人所在市 备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private String invoiceCounty; //增值票收票人所在区/县  备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private String invoiceAddress; //增值票收票人所在地址 备注：当invoiceType=2 且invoiceState=1时则此字段必填
    private Double orderPrice; //订单价格
    private Double orderNakedPrice; //订单裸价
    private Double orderTaxPrice;//京东税价
    private Integer Type; //订单类型 1是父订单 2是子订单
    private List<JDCOrder> cOrders; //子订单列表
//    private List<JDOrderTrack> orderTrack; //配送信息
    private List<JDSku> skus; //商品列表

    public String getJdOrderId() {
        return jdOrderId;
    }

    public void setJdOrderId(String jdOrderId) {
        this.jdOrderId = jdOrderId;
    }

    public String getThirdOrder() {
        return thirdOrder;
    }

    public void setThirdOrder(String thirdOrder) {
        this.thirdOrder = thirdOrder;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getCounty() {
        return county;
    }

    public void setCounty(Integer county) {
        this.county = county;
    }

    public Integer getTown() {
        return town;
    }

    public void setTown(Integer town) {
        this.town = town;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getInvoiceState() {
        return invoiceState;
    }

    public void setInvoiceState(Integer invoiceState) {
        this.invoiceState = invoiceState;
    }

    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Integer getSelectedInvoiceTitle() {
        return selectedInvoiceTitle;
    }

    public void setSelectedInvoiceTitle(Integer selectedInvoiceTitle) {
        this.selectedInvoiceTitle = selectedInvoiceTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getInvoiceContent() {
        return invoiceContent;
    }

    public void setInvoiceContent(Integer invoiceContent) {
        this.invoiceContent = invoiceContent;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getSubmitState() {
        return submitState;
    }

    public void setSubmitState(Integer submitState) {
        this.submitState = submitState;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoicePhone() {
        return invoicePhone;
    }

    public void setInvoicePhone(String invoicePhone) {
        this.invoicePhone = invoicePhone;
    }

    public String getInvoiceProvice() {
        return invoiceProvice;
    }

    public void setInvoiceProvice(String invoiceProvice) {
        this.invoiceProvice = invoiceProvice;
    }

    public String getInvoiceCity() {
        return invoiceCity;
    }

    public void setInvoiceCity(String invoiceCity) {
        this.invoiceCity = invoiceCity;
    }

    public String getInvoiceCounty() {
        return invoiceCounty;
    }

    public void setInvoiceCounty(String invoiceCounty) {
        this.invoiceCounty = invoiceCounty;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public List<JDSku> getSkus() {
        return skus;
    }

    public void setSkus(List<JDSku> skus) {
        this.skus = skus;
    }

    public Double getOrderNakedPrice() {
        return orderNakedPrice;
    }

    public void setOrderNakedPrice(Double orderNakedPrice) {
        this.orderNakedPrice = orderNakedPrice;
    }

    public Double getOrderTaxPrice() {
        return orderTaxPrice;
    }

    public void setOrderTaxPrice(Double orderTaxPrice) {
        this.orderTaxPrice = orderTaxPrice;
    }

    public List<JDCOrder> getcOrders() {
        return cOrders;
    }

    public void setcOrders(List<JDCOrder> cOrders) {
        this.cOrders = cOrders;
    }
}
