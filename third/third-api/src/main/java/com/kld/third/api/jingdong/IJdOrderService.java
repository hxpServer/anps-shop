package com.kld.third.api.jingdong;

import com.kld.third.dto.jingdong.JDOrderTrack;
import com.kld.third.dto.jingdong.JDPOrder;

import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
public interface IJdOrderService {

    //订单相关
    public String createOrder(JDPOrder jdpOrder); //提交订单，锁库存接口
    public boolean confirmOrder(String jDOrderId); //确认预占库存订单接口
    public boolean cancelOrder(String jDOrderId);  //取消未确认订单接口
    public JDPOrder getOrderDetail(String jDOrderId); //查询京东订单信息接口
    public String getJdOrderIdByOrderId(String orderid);//反查京东订单号接口
    public JDPOrder getJdOrderByOrderId(String orderid);//反查京东订单号接口
    public List<JDOrderTrack> getJdOrderTrackByJdOrderId(String jDOrderId);//查询订单配送信息接口

}
