package com.kld.third.PayMethod.dto;

import java.io.Serializable;

/**
 * Created by xiaohe on 2016/3/31.
 */
public class TenPayDto implements Serializable {
    private static final long serialVersionUID = -8884462267065353127L;
    private String signType;//签名类型，取值：MD5、RSA，默认：MD5
    private String serviceVersion;//版本号，默认为1.0
    private String inputCharset;//字符编码,取值：GBK、UTF-8，默认：GBK
    private String sign;//签名
    private String signKeyIndex;//多密钥支持的密钥序号，默认1
    private String bankType;//银行类型，默认为“DEFAULT”－财付通支付中心。银行直连编码及额度请与技术支持联系
    private String body;//商品描述
    private String attach;//附加数据，原样返回
    private String returnUrl;//交易完成后跳转的URL，需给绝对路径
    private String notifyUrl;//接收财付通通知的URL，需给绝对路径
    private String buyerId;//买方的财付通账户(QQ 或EMAIL
    private String partner;//买方的财付通账户(QQ 或EMAIL
    private String outTradeNo;//商户系统内部的订单号,32个字符内、可包含字母,确保在商户系统唯一
    private Integer totalFee;//订单总金额，单位为分
    private Integer feeType;//现金支付币种,取值：1（人民币）,默认值是1，暂只支持1
    private String spbillCreateIp;//订单生成的机器IP，指用户浏览器端IP，不是商户服务器IP
    private String timeStart;//订单生成时间，格式为yyyyMMddHHmmss，
    private String timeExpire;//订单失效时间，格式为yyyyMMddHHmmss
    private Integer transportFee;//物流费用，单位为分。
    private Integer productFee;//商品费用，单位为分
    private String goodsTag;//商品费用，单位为分

    public String getSignType() {
        return signType;
    }

    public String getServiceVersion() {
        return serviceVersion;
    }

    public String getInputCharset() {
        return inputCharset;
    }

    public String getSign() {
        return sign;
    }

    public String getSignKeyIndex() {
        return signKeyIndex;
    }

    public String getBankType() {
        return bankType;
    }

    public String getBody() {
        return body;
    }

    public String getAttach() {
        return attach;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public String getPartner() {
        return partner;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public String getSpbillCreateIp() {
        return spbillCreateIp;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public String getTimeExpire() {
        return timeExpire;
    }

    public Integer getTransportFee() {
        return transportFee;
    }

    public Integer getProductFee() {
        return productFee;
    }

    public String getGoodsTag() {
        return goodsTag;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    public void setInputCharset(String inputCharset) {
        this.inputCharset = inputCharset;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void setSignKeyIndex(String signKeyIndex) {
        this.signKeyIndex = signKeyIndex;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public void setSpbillCreateIp(String spbillCreateIp) {
        this.spbillCreateIp = spbillCreateIp;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
    }

    public void setTransportFee(Integer transportFee) {
        this.transportFee = transportFee;
    }

    public void setProductFee(Integer productFee) {
        this.productFee = productFee;
    }

    public void setGoodsTag(String goodsTag) {
        this.goodsTag = goodsTag;
    }
}
