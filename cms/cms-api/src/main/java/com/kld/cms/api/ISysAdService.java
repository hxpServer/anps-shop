package com.kld.cms.api;

import com.kld.cms.po.SysAd;

import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
public interface ISysAdService {

    int insertSelective(SysAd record);

    int updateByPrimaryKeySelective(SysAd record);

    int deleteAdsByIds(List<Integer> list);

    List<SysAd> getAdList();

    SysAd getAdByCode(String code);

    List<SysAd> getAdWebList(Map<String, Object> map);
}
