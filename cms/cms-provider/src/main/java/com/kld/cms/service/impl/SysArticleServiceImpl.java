package com.kld.cms.service.impl;

import com.kld.cms.api.ISysArticleService;
import com.kld.cms.dao.SysArticleDao;
import com.kld.cms.po.SysArticle;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.global.Global;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.common.redis.JedisManager;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysArticleServiceImpl implements ISysArticleService {

    @Resource
    private SysArticleDao articleDao;

    @Override
    public int insertSelective(SysArticle record) {
        int r=articleDao.insert("insertSelective",record);
//        if(r>0&&record.getState().intValue()==1){
//            //清除前台缓存信息
//            RedisUtil ru = new RedisUtil();
//            if(ru.getJedis()!=null){
//                if(ru.isExists(Global.INDEX_ARTICLE)){
//                    ru.delete(Global.INDEX_ARTICLE);
//                }
//            }
//        }
        return r;
    }

    @Override
    public int updateByPrimaryKeySelective(SysArticle record) {
        int r=articleDao.update("updateByPrimaryKeySelective",record);
//        if(r>0&&record.getState().intValue()==1){
//            //清除前台缓存信息
//                RedisUtil ru = new RedisUtil();
//                if(ru.getJedis()!=null){
//                    if(ru.isExists(Global.INDEX_ARTICLE)){
//                        ru.delete(Global.INDEX_ARTICLE);
//                    }
//                }
//        }
        return r;
    }

    @Override
    public int deleteArticlesByIds(List<Integer> list) {
        int r=articleDao.delete("deleteArticlesByIds",list);
//        if(r>0){
//            //清除前台缓存信息
//            RedisUtil ru = new RedisUtil();
//            if(ru.getJedis()!=null){
//                if(ru.isExists(Global.INDEX_ARTICLE)){
//                    ru.delete(Global.INDEX_ARTICLE);
//                }
//            }
//        }
        if(r>0) {
            JedisManager.delObject(Global.INDEX_ARTICLE);
        }
        return r;
    }

    @Override
    public ResultMsg getArticleList(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo pageInfo=new PageInfo(articleDao.find("getArticleList",null));
        return  PageInfoResult.PageInfoMsg(pageInfo);
    }

    @Override
    public List<SysArticle> getArtWebList(Integer num) {
        Map<String,Object> map1=new HashMap<String,Object>();
        map1.put("num",num);
        return articleDao.find("getArtWebList",map1);
    }

    @Override
    public ResultMsg getArtWebList(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo pageInfo=new PageInfo(articleDao.find("getArtWebList",null));
        return  PageInfoResult.PageInfoMsg(pageInfo);
    }
    @Override
    public SysArticle selectByPrimaryKey(Integer id) {
        return articleDao.get("selectByPrimaryKey",id);
    }

}
