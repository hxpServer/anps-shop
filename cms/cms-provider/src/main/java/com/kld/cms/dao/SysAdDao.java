package com.kld.cms.dao;

import com.kld.cms.po.SysAd;
import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * hejinping 2016.3.24
 */
@Repository
public class SysAdDao extends SimpleDaoImpl<SysAd> {

}