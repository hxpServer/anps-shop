package com.kld.cms.dao;


import com.kld.cms.po.SysArticle;
import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * hejinping 2016.3.24
 */
@Repository
public class SysArticleDao extends SimpleDaoImpl<SysArticle> {

}