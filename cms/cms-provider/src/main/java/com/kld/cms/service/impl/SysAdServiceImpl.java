package com.kld.cms.service.impl;

import com.kld.cms.api.ISysAdService;
import com.kld.cms.dao.SysAdDao;
import com.kld.cms.po.SysAd;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysAdServiceImpl implements ISysAdService {

    @Resource
    private SysAdDao adDao;

    @Override
    public int insertSelective(SysAd record) {
        int r=adDao.insert("insertSelective",record);
//        if(r>0){
//            RedisUtil ru = new RedisUtil();
//            if(ru.getJedis()!=null){
//                if(ru.isExists(Global.INDEX_AD)){ru.delete(Global.INDEX_AD);}
//                if(ru.isExists(Global.WXAPP_INDEX_AD)){ru.delete(Global.WXAPP_INDEX_AD);}
//            }
//        }
        return r;
    }

    @Override
    public int updateByPrimaryKeySelective(SysAd record) {
        int r=adDao.update("updateByPrimaryKeySelective",record);
//        if(r>0){
//            RedisUtil ru = new RedisUtil();
//            if(ru.getJedis()!=null){
//                if(ru.isExists(Global.INDEX_AD)){ru.delete(Global.INDEX_AD);}
//                if(ru.isExists(Global.WXAPP_INDEX_AD)){ru.delete(Global.WXAPP_INDEX_AD);}
//            }
//        }
        return r;
    }

    @Override
    public int deleteAdsByIds(List<Integer> list) {
        return adDao.delete("deleteAdsByIds",list);
    }

    @Override
    public List<SysAd> getAdList() {
        return adDao.find("getAdList",null);
    }

    @Override
    public SysAd getAdByCode(String code) {
        return adDao.get("getAdByCode",code);
    }

    @Override
    public List<SysAd> getAdWebList(Map<String,Object> map) {
        return adDao.find("getAdWebList",map);
    }
}
