package com.kld.order.po;



import com.kld.order.dto.jingdong.JDAfterSaleDto;
import com.kld.order.dto.jingdong.JDCompatibleServiceDetailDTO;
import com.kld.order.dto.jingdong.JDComponentExport;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ReturnOrder implements Serializable {
    private static final long serialVersionUID = 5133614305804304395L;
    private BigDecimal id;

    private BigDecimal orderid;

    private String productcode;

    private Date createtime;

    private BigDecimal suborderid;

    private String thirdorderid;

    private BigDecimal state;

    private String recieveaddr;

    private String addr;

    private String cardtradeno;

    private String remark;

    private String catecode;

    private BigDecimal num;

    private BigDecimal thirdrtnid;

    private String creator;

    private int proviceid;

    private int cityid;

    private int countyid;

    private int towinid;

    private String adress;

    private String description;

    private List<JDComponentExport> jdCompSList;//服务类型

    private List<JDComponentExport> jdCompRList;//返回方式

    private String thirdcode;//第三方商品code

    private JDCompatibleServiceDetailDTO jdCompatibleServiceDetailDTO;//服务单明细信息

    private JDAfterSaleDto jdAfterSaleDto;

    private String realname;

    private String phone;

    private String name;   //商品名称

    private String picurl;  //退单商品图片

    private String picurlstr; //图片完整路径

    private String picimgurl;//问题图片url（多张图片，逗号分隔）

    private String wareQD;//包装清单

    private int issplist;//是否拆单0否1是

    public String getThirdcode() {
        return thirdcode;
    }

    public void setThirdcode(String thirdcode) {
        this.thirdcode = thirdcode;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getOrderid() {
        return orderid;
    }

    public void setOrderid(BigDecimal orderid) {
        this.orderid = orderid;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode == null ? null : productcode.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getSuborderid() {
        return suborderid;
    }

    public void setSuborderid(BigDecimal suborderid) {
        this.suborderid = suborderid;
    }

    public String getThirdorderid() {
        return thirdorderid;
    }

    public void setThirdorderid(String thirdorderid) {
        this.thirdorderid = thirdorderid == null ? null : thirdorderid.trim();
    }

    public String getPicurlstr() {
        return picurl==null?"": "http://img13.360buyimg.com/n2/"+picurl;
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }



    public String getCardtradeno() {
        return cardtradeno;
    }

    public void setCardtradeno(String cardtradeno) {
        this.cardtradeno = cardtradeno == null ? null : cardtradeno.trim();
    }

    public String getRecieveaddr() {
        return recieveaddr;
    }

    public void setRecieveaddr(String recieveaddr) {
        this.recieveaddr = recieveaddr;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCatecode() {
        return catecode;
    }

    public void setCatecode(String catecode) {
        this.catecode = catecode == null ? null : catecode.trim();
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getThirdrtnid() {
        return thirdrtnid;
    }

    public void setThirdrtnid(BigDecimal thirdrtnid) {
        this.thirdrtnid = thirdrtnid;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }


    public int getProviceid() {
        return proviceid;
    }

    public void setProviceid(int proviceid) {
        this.proviceid = proviceid;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public int getCountyid() {
        return countyid;
    }

    public void setCountyid(int countyid) {
        this.countyid = countyid;
    }

    public int getTowinid() {
        return towinid;
    }

    public void setTowinid(int towinid) {
        this.towinid = towinid;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public List<JDComponentExport> getJdCompSList() {
        return jdCompSList;
    }

    public void setJdCompSList(List<JDComponentExport> jdCompSList) {
        this.jdCompSList = jdCompSList;
    }

    public List<JDComponentExport> getJdCompRList() {
        return jdCompRList;
    }

    public void setJdCompRList(List<JDComponentExport> jdCompRList) {
        this.jdCompRList = jdCompRList;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWareQD() {
        return wareQD;
    }

    public void setWareQD(String wareQD) {
        this.wareQD = wareQD;
    }

    public int getIssplist() {
        return issplist;
    }

    public void setIssplist(int issplist) {
        this.issplist = issplist;
    }

    public JDCompatibleServiceDetailDTO getJdCompatibleServiceDetailDTO() {
        return jdCompatibleServiceDetailDTO;
    }

    public void setJdCompatibleServiceDetailDTO(JDCompatibleServiceDetailDTO jdCompatibleServiceDetailDTO) {
        this.jdCompatibleServiceDetailDTO = jdCompatibleServiceDetailDTO;
    }

    public JDAfterSaleDto getJdAfterSaleDto() {
        return jdAfterSaleDto;
    }

    public void setJdAfterSaleDto(JDAfterSaleDto jdAfterSaleDto) {
        this.jdAfterSaleDto = jdAfterSaleDto;
    }

    public String getPicimgurl() {
        return picimgurl;
    }

    public void setPicimgurl(String picimgurl) {
        this.picimgurl = picimgurl;
    }

    @Override
    public String toString() {
        return "ReturnOrder{" +
                "id=" + id +
                ", orderid=" + orderid +
                ", productcode='" + productcode + '\'' +
                ", createtime=" + createtime +
                ", suborderid=" + suborderid +
                ", thirdorderid='" + thirdorderid + '\'' +
                ", state=" + state +
                ", recieveaddr='" + recieveaddr + '\'' +
                ", addr='" + addr + '\'' +
                ", cardtradeno='" + cardtradeno + '\'' +
                ", remark='" + remark + '\'' +
                ", catecode='" + catecode + '\'' +
                ", num=" + num +
                ", thirdrtnid=" + thirdrtnid +
                ", creator='" + creator + '\'' +
                ", proviceid=" + proviceid +
                ", cityid=" + cityid +
                ", countyid=" + countyid +
                ", towinid=" + towinid +
                ", adress='" + adress + '\'' +
                ", description='" + description + '\'' +
                ", thirdcode='" + thirdcode + '\'' +
                ", realname='" + realname + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", picurl='" + picurl + '\'' +
                '}';
    }
}