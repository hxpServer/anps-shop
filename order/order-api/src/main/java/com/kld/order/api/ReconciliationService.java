package com.kld.order.api;

import com.kld.order.dto.cardcore.CardDTO;
import com.kld.order.dto.cardcore.CardExpensePointDTO;
import com.kld.order.po.Reconciliation;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/16.
 */
public interface ReconciliationService {

    /**
     * 保存对账记录
     * @param reconciliation
     * @return
     */
    int saveReconciliation(Reconciliation reconciliation);

    /**
     * 根据日期更新对账记录为已对账
     * @param date
     * @param checkType
     * @return
     */
    int updateReconciliation(String date, String checkType);

    /**
     * 根据日期查询对账记录
     * @param date
     * @param checkType
     * @return
     */
    Reconciliation selectReconciliationByTypeAndDate(String date, String checkType);

    /**
     * 获取对账记录
     * @param map
     * @return
     */
    List<Reconciliation> getReconciliationList(Map<String, Object> map);

    /**
     * 获取卡核心的账单记录
     * @return
     */
    HashMap<String,Object> getRecordsFromCard(String billdate);

    /**
     * 下单扣减卡积分
     * @param cardExpensePointDTO
     * @return
     */
    JSONObject expensePoint(CardExpensePointDTO cardExpensePointDTO);

    /**
     * 获取扣减积分信息
     * @param uniqueId
     * @return
     */
    JSONObject checkExpensePoint(String uniqueId);

    Integer insertCardBills(CardDTO cardDTO);

    List<Map<String,Object>> getPointsAndNum(String billdate);

    List<String> getBilldateList();
}
