package com.kld.order.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SOrderProcess implements Serializable {
    private static final long serialVersionUID = 6094450088658943775L;
    private BigDecimal id;

    private Integer soptype;

    private String sopmemo;

    private String soper;

    private Date soptime;

    private BigDecimal suborderid;

    private Integer subordertype;

    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Integer getSoptype() {
        return soptype;
    }

    public void setSoptype(Integer soptype) {
        this.soptype = soptype;
    }

    public String getSopmemo() {
        return sopmemo;
    }

    public void setSopmemo(String sopmemo) {
        this.sopmemo = sopmemo == null ? null : sopmemo.trim();
    }

    public String getSoper() {
        return soper;
    }

    public void setSoper(String soper) {
        this.soper = soper == null ? null : soper.trim();
    }

    public Date getSoptime() {
        return soptime;
    }

    public void setSoptime(Date soptime) {
        this.soptime = soptime;
    }

    public BigDecimal getSuborderid() {
        return suborderid;
    }

    public void setSuborderid(BigDecimal suborderid) {
        this.suborderid = suborderid;
    }

    public Integer getSubordertype() {
        return subordertype;
    }

    public void setSubordertype(Integer subordertype) {
        this.subordertype = subordertype;
    }
}