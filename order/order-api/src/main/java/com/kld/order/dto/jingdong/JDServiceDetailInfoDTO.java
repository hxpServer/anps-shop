package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDServiceDetailInfoDTO {

    public Integer wareId;//商品编号
    public String wareName;//商品名称
    public String wareBrand;//商品品牌
    public Integer afsDetailType;//明细类型 主商品(10),  赠品(20), 附件(30)，拍拍取主商品就可以
    public String wareDescribe;//附件描述

    public Integer getWareId() {
        return wareId;
    }

    public void setWareId(Integer wareId) {
        this.wareId = wareId;
    }

    public String getWareName() {
        return wareName;
    }

    public void setWareName(String wareName) {
        this.wareName = wareName;
    }

    public String getWareBrand() {
        return wareBrand;
    }

    public void setWareBrand(String wareBrand) {
        this.wareBrand = wareBrand;
    }

    public Integer getAfsDetailType() {
        return afsDetailType;
    }

    public void setAfsDetailType(Integer afsDetailType) {
        this.afsDetailType = afsDetailType;
    }

    public String getWareDescribe() {
        return wareDescribe;
    }

    public void setWareDescribe(String wareDescribe) {
        this.wareDescribe = wareDescribe;
    }
}
