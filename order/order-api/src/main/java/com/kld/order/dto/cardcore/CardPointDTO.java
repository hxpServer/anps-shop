package com.kld.order.dto.cardcore;

/**
 * Created by Administrator on 2015/8/16.
 */
public class CardPointDTO {

    public long cardAsn;
    public String MAC;

    public long getCardAsn() {
        return cardAsn;
    }

    public void setCardAsn(long cardAsn) {
        this.cardAsn = cardAsn;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }
}
