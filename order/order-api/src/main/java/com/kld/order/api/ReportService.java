package com.kld.order.api;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.order.dto.DailyDetailRptVO;
import com.kld.order.po.Report;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2016/1/15.
 */
public interface ReportService {

    int insertSelective(Report record);

    Report selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Report record);

    ResultMsg insertReport(String billdate);

//    List<PointsActDailyRptVO> getPointsActDailyRptList(Map<String, Object> map);
//
//    List<TradeDailyRptVO> getTradeDailyRptList(Map<String, Object> map);
//
//    List<OtherActDailyRptVO> getActDailyRptList(Map<String, Object> map);
//
//    List<DgColumn> getPActTradeNames(Map<String, Object> map);
//
//    List<Report> getReportList(Map<String, Object> map);
//
//    List<DailyDetailRptVO> getDailyDetailRptList(Map<String, Object> map);
//
//    List<PromotionActRptVO> getPromotionActRptList(Map<String, Object> map);
//
//    List<HotCategoryRptVO> getHotLV1CategoryRptList(Map<String, Object> map);
//
//    List<HotAreaOrderRptVO> getHotAreaOrderRptList(Map<String, Object> map);
//
//    List<HotPointsOrderRpt> getHotPointsOrderRptList(Map<String, Object> map);
//
//    List<HotTimeOrderRptVo> getHotTimeOrderRptList(Map<String, Object> map);
//
//    List<NonHotProductRptVO> getNonhotProductRptList(Map<String, Object> map);
}
