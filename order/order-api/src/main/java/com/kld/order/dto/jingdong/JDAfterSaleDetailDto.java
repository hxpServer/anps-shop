package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDAfterSaleDetailDto {

    public Long skuId; // 商品编号 必填
    public Integer skuNum;// 商品申请数量  必填

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }
}
