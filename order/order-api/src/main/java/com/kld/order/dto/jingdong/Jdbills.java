package com.kld.order.dto.jingdong;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2015/12/29.
 */
public class Jdbills {

    private String jdorderid;
    private BigDecimal state;
    private BigDecimal hangupstate;
    private BigDecimal  invoicestate;
    private BigDecimal  orderprice;
    private Date createtime;
    private BigDecimal checktype;
    private Date checkttime;

    public Date getCheckttime() {
        return checkttime;
    }

    public void setCheckttime(Date checkttime) {
        this.checkttime = checkttime;
    }

    public BigDecimal getChecktype() {
        return checktype;
    }

    public void setChecktype(BigDecimal checktype) {
        this.checktype = checktype;
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public BigDecimal getHangupstate() {
        return hangupstate;
    }

    public void setHangupstate(BigDecimal hangupstate) {
        this.hangupstate = hangupstate;
    }

    public BigDecimal getInvoicestate() {
        return invoicestate;
    }

    public void setInvoicestate(BigDecimal invoicestate) {
        this.invoicestate = invoicestate;
    }

    public BigDecimal getOrderprice() {
        return orderprice;
    }

    public void setOrderprice(BigDecimal orderprice) {
        this.orderprice = orderprice;
    }


    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getJdorderid() {
        return jdorderid;
    }

    public void setJdorderid(String jdorderid) {
        this.jdorderid = jdorderid;
    }

    @Override
    public String toString() {
        return "Jdbills{" +
                "jdorderid='" + jdorderid + '\'' +
                ", state=" + state +
                ", hangupstate=" + hangupstate +
                ", invoicestate=" + invoicestate +
                ", orderprice=" + orderprice +
                ", createtime=" + createtime +
                '}';
    }
}
