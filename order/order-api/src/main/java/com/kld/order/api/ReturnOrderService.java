package com.kld.order.api;


import com.kld.common.framework.dto.ResultMsg;
import com.kld.order.po.ReturnOrder;
import net.sf.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2015/8/7.
 */
public interface ReturnOrderService {

    ReturnOrder selectReturnOrderDetailByOrderid(Map<String, Object> map);

    ResultMsg checkReturnOrderDetailByOrderid(Map<String, Object> map);

    int insertReturnorder(ReturnOrder record);

    int updateByPrimaryKeySelective(ReturnOrder record);


    ResultMsg saveReturnOrderWithTrans(JSONObject jsonReturnOrder, JSONObject asSafeDto,
                                       JSONObject jsonasSafeDto,
                                       JSONObject jsonasPickwareDto,
                                       JSONObject jsonasReturnwareDto,
                                       JSONObject jsonasDetailDto) throws Exception;

    List<ReturnOrder> getReturnOrdersThirdRtnIdIsNull();
    
}

