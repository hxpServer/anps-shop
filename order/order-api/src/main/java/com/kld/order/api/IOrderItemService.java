package com.kld.order.api;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.order.po.OrderItem;
import com.kld.order.po.POrder;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/6.
 */
public interface IOrderItemService {

    List<OrderItem> selectOrderItemByTime(Map<String, Object> map);

    List<OrderItem> selectOrderParentItemByTime(Map<String, Object> map);

    int insertSelective(OrderItem orderitem);

    int updateByPrimaryKeySelective(OrderItem orderitem);

    List<OrderItem> getOrderItemList(Map<String, Object> map);

    List<OrderItem> getOrderItemListByOrderid(Integer orderid);

    int deleteOrderItemsByOredrid(Integer orderid);

    POrder getReturnOrderByOrderid(POrder o);

    List<OrderItem> getOrderItemListBySubId(Integer suborderid);

    int bathinsertOrderItem(@Param("list") List<OrderItem> list);

    int updateOrderItemByIDs(List<Integer> ids, int suborderid);

    int updateOrderItemByOrderIdAndProid(Integer orderid, Integer subOrderid, BigDecimal proid, Double tax, Double mctprice, Double mctnakedprice, Double mcttaxprice);

    List<OrderItem> getReturnItemList(Map<String, Object> map);

    List<OrderItem> getReturnItems(Map<String, Object> map);

    OrderItem findOrderitemByItemid(BigDecimal itemid);

    ResultMsg updateOrderItemActInfoByActid(Integer actid);

    public ResultMsg updateActOrderItemOriInfoByMctPrice(Integer actid);

    int updateSelfPriceByPoints(Map<String, Object> map);
}
