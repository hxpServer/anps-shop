package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 *
 * 返件信息实体，即商品如何返回客户手中
 */
public class JDAfterSaleReturnwareDto {

    public Integer returnwareType;//返件方式  自营配送(10),第三方配送(20);换、修这两种情况必填（默认值）
    public Integer returnwareProvince;//返件省    换、修这两种情况必填
    public Integer returnwareCity;//返件市
    public Integer returnwareCounty;//返件县
    public Integer returnwareVillage;//返件乡镇
    public String returnwareAddress;//返件街道地址  最多500字符，换、修这两种情况必填

    public Integer getReturnwareType() {
        return returnwareType;
    }

    public void setReturnwareType(Integer returnwareType) {
        this.returnwareType = returnwareType;
    }

    public Integer getReturnwareProvince() {
        return returnwareProvince;
    }

    public void setReturnwareProvince(Integer returnwareProvince) {
        this.returnwareProvince = returnwareProvince;
    }

    public Integer getReturnwareCity() {
        return returnwareCity;
    }

    public void setReturnwareCity(Integer returnwareCity) {
        this.returnwareCity = returnwareCity;
    }

    public Integer getReturnwareCounty() {
        return returnwareCounty;
    }

    public void setReturnwareCounty(Integer returnwareCounty) {
        this.returnwareCounty = returnwareCounty;
    }

    public Integer getReturnwareVillage() {
        return returnwareVillage;
    }

    public void setReturnwareVillage(Integer returnwareVillage) {
        this.returnwareVillage = returnwareVillage;
    }

    public String getReturnwareAddress() {
        return returnwareAddress;
    }

    public void setReturnwareAddress(String returnwareAddress) {
        this.returnwareAddress = returnwareAddress;
    }
}
