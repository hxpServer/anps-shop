package com.kld.order.po;

import net.sf.json.JSONArray;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SubOrder implements Serializable {
    private static final long serialVersionUID = -3326076763333989158L;
    private Integer id;

    private Integer orderid;
    private Integer suborderid; //等同ID，后台页面使用

    private String merchantcode;

    private String thirdid;

    private String cardtradeno;

    private Integer state;

    private Date subtime;

    private Double merchantorderprice;
    private Double merchantnakedprice;
    private Double merchanttaxprice;

    private String oilcardno;
    private String realname;
    private String phone;
    private String mobile;
    private Integer issplit;
    private Integer soptype;
    private String sopmemo;
    private Double freight;

    private List<OrderItem> suborderitems;
    private JSONArray jdOrderTracks; //[{"msgTime":操作时间, "content":配送内容, "operator":操作人}]
    private String statestr; //状态文本信息

    private int neworderid;  //补单单号

    private int refuseprocessstate;  //拒收订单状态

    private String merchantName; //合作商名称
    private String subtimeStr;

    public Double getFreight() {
        return freight;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }
    
    public String getSubtimeStr() {
        return subtimeStr;
    }

    public void setSubtimeStr(String subtimeStr) {
        this.subtimeStr = subtimeStr;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public int getRefuseprocessstate() {
        return refuseprocessstate;
    }

    public void setRefuseprocessstate(int refuseprocessstate) {
        this.refuseprocessstate = refuseprocessstate;
    }

    public int getNeworderid() {
        return neworderid;
    }

    public void setNeworderid(int neworderid) {
        this.neworderid = neworderid;
    }

    public String getStatestr() {
        if(state!=null){
            switch (state.intValue()){
                case 1: statestr="未支付"; break;
                case 2: statestr="支付中";break;
                case 3: statestr="已支付"; break;
                case 4: statestr="待收货"; break;
                case 5: statestr="已妥投"; break;
                case 6: statestr="已拒收"; break;
                case 7: statestr="已废弃"; break;
                case 8: statestr="已取消"; break;
                case 9: statestr="已完成"; break;
                default: statestr=""; break;
            }
        }else{
            return null;
        }

        return statestr;
    }

    public JSONArray getJdOrderTracks() {
        return jdOrderTracks;
    }

    public void setJdOrderTracks(JSONArray jdOrderTracks) {
        this.jdOrderTracks = jdOrderTracks;
    }

    public Integer getId() {
        return id;
    }

    public List<OrderItem> getSuborderitems() {
        return suborderitems;
    }

    public void setSuborderitems(List<OrderItem> suborderitems) {
        this.suborderitems = suborderitems;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getMerchantcode() {
        return merchantcode;
    }

    public void setMerchantcode(String merchantcode) {
        this.merchantcode = merchantcode == null ? null : merchantcode.trim();
    }

    public String getThirdid() {
        return thirdid;
    }

    public void setThirdid(String thirdid) {
        this.thirdid = thirdid == null ? null : thirdid.trim();
    }

    public String getCardtradeno() {
        return cardtradeno;
    }

    public void setCardtradeno(String cardtradeno) {
        this.cardtradeno = cardtradeno == null ? null : cardtradeno.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getSubtime() {
        return subtime;
    }

    public void setSubtime(Date subtime) {
        this.subtime = subtime;
    }

    public String getOilcardno() {
        return oilcardno;
    }

    public void setOilcardno(String oilcardno) {
        this.oilcardno = oilcardno;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getSuborderid() {
        return suborderid;
    }

    public void setSuborderid(Integer suborderid) {
        this.suborderid = suborderid;
    }

    public Integer getIssplit() {
        return issplit;
    }

    public void setIssplit(Integer issplit) {
        this.issplit = issplit;
    }

    public Integer getSoptype() {
        return soptype;
    }

    public void setSoptype(Integer soptype) {
        this.soptype = soptype;
    }

    public String getSopmemo() {
        return sopmemo;
    }

    public void setSopmemo(String sopmemo) {
        this.sopmemo = sopmemo;
    }

    public Double getMerchantorderprice() {
        return merchantorderprice;
    }

    public void setMerchantorderprice(Double merchantorderprice) {
        this.merchantorderprice = merchantorderprice;
    }

    public Double getMerchantnakedprice() {
        return merchantnakedprice;
    }

    public void setMerchantnakedprice(Double merchantnakedprice) {
        this.merchantnakedprice = merchantnakedprice;
    }

    public Double getMerchanttaxprice() {
        return merchanttaxprice;
    }

    public void setMerchanttaxprice(Double merchanttaxprice) {
        this.merchanttaxprice = merchanttaxprice;
    }
}