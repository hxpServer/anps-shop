package com.kld.order.api;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.order.po.OrderItem;
import com.kld.order.po.POrder;
import com.kld.order.po.Report;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2016/3/16.
 */
public interface IPOrderService {

//    public List<POrder> getOrderList(Map<String, Object> map);
//
//    public POrder getOrderDetail(int id);
//
//    public POrder getOrderDetail(int id, boolean withTracks);
//
//    /**
//     * 新建订单（带事务）
//     * @param orders
//     * @return
//     * @throws Exception
//     */
//    public ResultMsg insertWithTrans(POrder orders) throws Exception;
//
//    /**
//     * 支付订单（带事务）
//     * @param orderid
//     * @return
//     */
//    public ResultMsg payOrder(int orderid, String oilcardno, Boolean iswxBindCard) throws Exception;
//
//    int deleteByPrimaryKey(Integer id);
//    ResultMsg ConfirmJDOrder(POrder order);
//    int update(POrder orders);
//
//    List<POrder> getReturnOrderList(Map<String, Object> map);
//
//    public POrder getOrderWithItem(int id);
//
//    POrder getOrderWithItems(POrder o);
//
//    /**
//     * 根据订单ID修改订单状态，处理拒收订单
//     * @param id
//     * @return
//     */
//    int updateOrderState(Integer id, Integer state, Date datet);
//
//    /**
//     * 根据订单ID修改为卡核心已完成对账
//     * @param id
//     * @return
//     */
//    int updateOrderCheckCard(Integer id, String billDate);
//
//    /**
//     * 根据订单ID修改为第三方已完成对账
//     * @param id
//     * @return
//     */
//    int updateOrderCheckMerchant(Integer id);
//
//    /**
//     * 根据订单ID修改是否拆单标记
//     * @param id
//     * @return
//     */
//    int updateOrderSplit(Integer id);
//
//
//    /**
//     * 根据父订单ID获取父订单信息
//     * @param oid
//     * @return
//     */
//    POrder getOrdersInfoByOrderId(Integer oid);
//
//    /**
//     * 根据第三方信息获取本系统订单信息
//     * @param map
//     * @return
//     */
//    POrder getOrdersInfoByThirdId(Map<String, Object> map);
//
//    /**
//     * 根据来源子订单ID获取父订单信息
//     * @param fromsubid
//     * @return
//     */
//    POrder getOrderInfoByFromSubId(Integer fromsubid);
//
//    /**
//     * 根据ID，更新第3方订单信息
//     * @param id
//     * @param jdOrderId
//     * @return
//     */
//    int updateThirdOrderOfOrder(Integer id, String jdOrderId);
//
//    /**
//     * 批量修改订单状态
//     * @param merchantcode
//     * @param list
//     * @param state
//     * @return
//     */
//    int updateOrderStateByThirdId(String merchantcode, List<String> list, Integer state);
//
//    /**
//     * 补单重新下单
//     * @param orders
//     * @return
//     */
//    int saveOrderByReOrder(POrder orders);
//
//    ResultMsg getOrdersByOrderidAndItemid(Map<String, Object> map);
//
//    /**
//     * 根据时间，查询全部订单，对账，某天的全部订单
//     * @param map
//     * @return
//     */
//    List<POrder> getOrdersListBill(Map<String, Object> map);
//
//    ResultMsg isApplyFWD(Map<String, Object> map);
//
//    String getAreaName(int areaid_1, int areaid_2, int areaid_3, int areaid_4);
//
//    POrder getLastOrderIDByUserID(String userid);
//
//    Integer getTodayOrderNum();
//
//    POrder findOrdersById(Integer orderid);
//
//    int updateOrdersStateById(Integer orderid, Integer state);
//
//    ResultMsg createOrder(POrder order) throws Exception;
//
//    Map<String,Object> orderlist(String userid);
//
//    List<OrderItem> buildOrderItems(String cartids, String productsinfo) ;
//
//    List<POrder> getPayStateOrders(Map<String, Object> map);
//
//    ResultMsg UpdateOrderCardPayState(POrder order);
//    ResultMsg createJdOrder(POrder order);
//
//    List<POrder> getListByBelongareaname();
//
//    List<POrder> getListByCardtradeno();
//
//    Integer getSumPoint(Map<String, Object> map);
//
//    Date getBillDateByOrderId(Integer orderId);
//
//    Integer updateOrderBillDate(Map<String, Object> map);
//
//    Map<String,Object> getPointsAndNum(String billdate);
//
//    ResultMsg cancelOrder(String orderid);
//
//    ResultMsg updateOrderItem(Map<String, Object> map);
//
//    List<Report> getOrdersDataForReport(String billdate);
}
