package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDServiceAftersalesAddressInfoDTO {

    public String address;//售后地址
    public String tel;//售后电话
    public String linkMan;//售后联系人
    public String postCode;//售后邮编

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
