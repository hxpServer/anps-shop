package com.kld.order.po;

/**
 * Created by 曹不正 on 2016/3/24.
 */
import com.kld.order.dto.jingdong.JDAfterSaleDto;
import com.kld.order.dto.jingdong.JDCompatibleServiceDetailDTO;
import com.kld.order.dto.jingdong.JDComponentExport;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class POrder implements Serializable {
    private static final long serialVersionUID = 5133614305804304395L;
    private Integer id;

    private String oilcardno;

    private BigDecimal points;
    private BigDecimal originpoints;

    private Integer state;

    private Integer channel;

    private String username;

    private String wxopenid;

    private String thirdorderid;

    private String cardtradeno;

    private Date createtime;

    private String creator;

    private Integer provinceid;

    private Integer cityid;

    private Integer countyid;

    private Integer townid;

    private String address;

    private String zip;

    private String phone;

    private String mobile;

    private String email;

    private String remark;

    private String merchantcode;
    private String merchantname;

    private Double suggestpoints;

    private Double amount;   //成本总价
    private Double originamount;

    private String realname;

    private String name;

    private List<OrderItem> orderitems;  //单身项

    private String cartids;      //购物车Id(逗号分隔)
    private String productsinfo;  //商品积分兑换信息（不同商品间逗号分隔，每组商品和数量用冒号分隔，例：11：1，22：2）

    private String cityname;
    private String provincename;
    private String countyname;
    private String townname;

    private String oucode;
    private String belongareaid; //支付油卡的归属地ID
    private String belongareaname; //支付油卡的归属地名称
    private BigDecimal freight;  //运费

    private int issplit;//是否拆单 1是0否
    public int ischeckcard; // 卡核心是否对账
    public int ischeckmerchant; //合作商是否对账

    private JSONObject trackJsonObject;

    private BigDecimal suborderid;

    private Date cardpaiedtime;

    private Date thirdendtime;

    private Integer fromsubid;

    private String wareQD;//包装清单

    private String thirdcode;//第三方商品code

    private String productcode;//商品编码

    private String picurl;

    private int num;

    private List<SubOrder> suborders;   //子订单信息; 详情展示使用

    private String areaname;  //区域全名（省、市、县、镇）；详情展示使用

    private String acqouname;  //收单机构

    private String userid; //用户id

    private String statestr; //订单状态文本

    private Integer fromorderid;  //来源主订单

    private Date billdate; //账单日期

    public BigDecimal getFreight() {
        return freight==null?BigDecimal.valueOf(0):freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public Integer getFromorderid() {
        return fromorderid;
    }

    public void setFromorderid(Integer fromorderid) {
        this.fromorderid = fromorderid;
    }

    public String getStatestr() {
        switch (state.intValue()){
            case 1: statestr="未支付"; break;
            case 2: statestr="支付中";break;
            case 3: statestr="已支付"; break;
            case 4: statestr="待收货"; break;
            case 5: statestr="已妥投"; break;
            case 6: statestr="已拒收"; break;
            case 7: statestr="已废弃"; break;
            case 8: statestr="已取消"; break;
            case 9: statestr="已完成"; break;
            default: statestr=""; break;
        }
        return statestr;
    }

    public Date getBilldate() {
        return billdate;
    }

    public void setBilldate(Date billdate) {
        this.billdate = billdate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAcqouname() {
        return acqouname;
    }

    public void setAcqouname(String acqouname) {
        this.acqouname = acqouname;
    }

    public String getAreaname() {
        areaname="";
        areaname+=StringUtils.isNotBlank(provincename)?(provincename+" "):"";
        areaname+=StringUtils.isNotBlank(cityname)?(cityname+" "):"";
        areaname+=StringUtils.isNotBlank(countyname)?(countyname+" "):"";
        areaname+=StringUtils.isNotBlank(townname)?townname:"";
        return areaname;
    }

//    public void setAreaname(String areaname) {
//        this.areaname = areaname;
//    }

    public List<SubOrder> getSuborders() {
        return suborders;
    }

    public void setSuborders(List<SubOrder> suborders) {
        this.suborders = suborders;
    }

    private List<JDComponentExport> jdCompSList;//服务类型

    private List<JDComponentExport> jdCompRList;//返回方式

    private JDCompatibleServiceDetailDTO jdCompatibleServiceDetailDTO;//服务单明细信息

    private JDAfterSaleDto jdAfterSaleDto;

    public String getProductsinfo() {
        return productsinfo;
    }

    public void setProductsinfo(String productsinfo) {
        this.productsinfo = productsinfo;
    }

    public Integer getProvinceid() {
        return provinceid==null?0:provinceid;
    }

    public void setProvinceid(Integer provinceid) {
        this.provinceid = provinceid;
    }

    public Integer getCityid() {
        return cityid==null?0:cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public Integer getCountyid() {
        return countyid==null?0:countyid;
    }

    public void setCountyid(Integer countyid) {
        this.countyid = countyid;
    }

    public Integer getTownid() {
        return townid==null?0:townid;
    }

    public void setTownid(Integer townid) {
        this.townid = townid;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename;
    }

    public String getCountyname() {
        return countyname;
    }

    public void setCountyname(String countyname) {
        this.countyname = countyname;
    }

    public String getTownname() {
        return townname;
    }

    public void setTownname(String townname) {
        this.townname = townname;
    }

    public String getCartids() {
        return cartids;
    }

    public void setCartids(String cartids) {
        this.cartids = cartids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSuggestpoints() {
        return suggestpoints;
    }

    public void setSuggestpoints(Double suggestpoints) {
        this.suggestpoints = suggestpoints;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<OrderItem> getOrderitems() {
        return orderitems;
    }

    public void setOrderitems(List<OrderItem> orderitems) {
        this.orderitems = orderitems;
    }

    public String getMerchantcode() {
        return merchantcode;
    }

    public void setMerchantcode(String merchantcode) {
        this.merchantcode = merchantcode;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOilcardno() {
        return oilcardno==null?"":oilcardno.trim();
    }

    public void setOilcardno(String oilcardno) {
        this.oilcardno = oilcardno == null ? null : oilcardno.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getOriginpoints() {
        return originpoints;
    }

    public void setOriginpoints(BigDecimal originpoints) {
        this.originpoints = originpoints;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUsername() {
        return username==null?"":username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getWxopenid() {
        return wxopenid;
    }

    public void setWxopenid(String wxopenid) {
        this.wxopenid = wxopenid == null ? null : wxopenid.trim();
    }

    public String getThirdorderid() {
        return thirdorderid;
    }

    public void setThirdorderid(String thirdorderid) {
        this.thirdorderid = thirdorderid == null ? null : thirdorderid.trim();
    }

    public String getCardtradeno() {
        return cardtradeno;
    }

    public void setCardtradeno(String cardtradeno) {
        this.cardtradeno = cardtradeno == null ? null : cardtradeno.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getAddress() {
        return address==null?"":address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip == null ? null : zip.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public JSONObject getTrackJsonObject() {
        return trackJsonObject;
    }

    public int getIssplit() {
        return issplit;
    }

    public void setIssplit(int issplit) {
        this.issplit = issplit;
    }

    public Date getCardpaiedtime() {
        return cardpaiedtime;
    }

    public void setCardpaiedtime(Date cardpaiedtime) {
        this.cardpaiedtime = cardpaiedtime;
    }

    public Date getThirdendtime() {
        return thirdendtime;
    }

    public void setThirdendtime(Date thirdendtime) {
        this.thirdendtime = thirdendtime;
    }

    public void setTrackJsonObject(JSONObject trackJsonObject) {
        this.trackJsonObject = trackJsonObject;
    }

    public Integer getFromsubid() {
        return fromsubid;
    }

    public void setFromsubid(Integer fromsubid) {
        this.fromsubid = fromsubid;
    }

    public String getWareQD() {
        return wareQD;
    }

    public void setWareQD(String wareQD) {
        this.wareQD = wareQD;
    }

    public List<JDComponentExport> getJdCompSList() {
        return jdCompSList;
    }

    public void setJdCompSList(List<JDComponentExport> jdCompSList) {
        this.jdCompSList = jdCompSList;
    }

    public List<JDComponentExport> getJdCompRList() {
        return jdCompRList;
    }

    public void setJdCompRList(List<JDComponentExport> jdCompRList) {
        this.jdCompRList = jdCompRList;
    }

    public JDCompatibleServiceDetailDTO getJdCompatibleServiceDetailDTO() {
        return jdCompatibleServiceDetailDTO;
    }

    public void setJdCompatibleServiceDetailDTO(JDCompatibleServiceDetailDTO jdCompatibleServiceDetailDTO) {
        this.jdCompatibleServiceDetailDTO = jdCompatibleServiceDetailDTO;
    }

    public JDAfterSaleDto getJdAfterSaleDto() {
        return jdAfterSaleDto;
    }

    public void setJdAfterSaleDto(JDAfterSaleDto jdAfterSaleDto) {
        this.jdAfterSaleDto = jdAfterSaleDto;
    }

    public String getThirdcode() {
        return thirdcode;
    }

    public void setThirdcode(String thirdcode) {
        this.thirdcode = thirdcode;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public BigDecimal getSuborderid() {
        return suborderid;
    }

    public void setSuborderid(BigDecimal suborderid) {
        this.suborderid = suborderid;
    }

    public String getPicurl() {
        return "http://img13.360buyimg.com/n1/"+picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public int getIscheckcard() {
        return ischeckcard;
    }

    public void setIscheckcard(int ischeckcard) {
        this.ischeckcard = ischeckcard;
    }

    public int getIscheckmerchant() {
        return ischeckmerchant;
    }

    public void setIscheckmerchant(int ischeckmerchant) {
        this.ischeckmerchant = ischeckmerchant;
    }

    public String getBelongareaid() {
        return belongareaid;
    }

    public void setBelongareaid(String belongareaid) {
        this.belongareaid = belongareaid;
    }

    public String getBelongareaname() {
        return belongareaname==null?"":belongareaname;
    }

    public void setBelongareaname(String belongareaname) {
        this.belongareaname = belongareaname;
    }

    @Override
    public String toString() {
        return "POrder{" +
                "id=" + id +
                ", oilcardno='" + oilcardno + '\'' +
                ", points=" + points +
                ", originpoints=" + originpoints +
                ", state=" + state +
                ", channel=" + channel +
                ", username='" + username + '\'' +
                ", wxopenid='" + wxopenid + '\'' +
                ", thirdorderid='" + thirdorderid + '\'' +
                ", cardtradeno='" + cardtradeno + '\'' +
                ", createtime=" + createtime +
                ", creator='" + creator + '\'' +
                ", provinceid=" + provinceid +
                ", cityid=" + cityid +
                ", countyid=" + countyid +
                ", townid=" + townid +
                ", address='" + address + '\'' +
                ", zip='" + zip + '\'' +
                ", phone='" + phone + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", remark='" + remark + '\'' +
                ", merchantcode='" + merchantcode + '\'' +
                ", merchantname='" + merchantname + '\'' +
                ", suggestpoints=" + suggestpoints +
                ", amount=" + amount +
                ", originamount=" + originamount +
                ", realname='" + realname + '\'' +
                ", name='" + name + '\'' +
                ", orderitems=" + orderitems +
                ", cartids='" + cartids + '\'' +
                ", productsinfo='" + productsinfo + '\'' +
                ", cityname='" + cityname + '\'' +
                ", provincename='" + provincename + '\'' +
                ", countyname='" + countyname + '\'' +
                ", townname='" + townname + '\'' +
                ", oucode='" + oucode + '\'' +
                ", belongareaid='" + belongareaid + '\'' +
                ", belongareaname='" + belongareaname + '\'' +
                ", freight=" + freight +
                ", issplit=" + issplit +
                ", ischeckcard=" + ischeckcard +
                ", ischeckmerchant=" + ischeckmerchant +
                ", trackJsonObject=" + trackJsonObject +
                ", suborderid=" + suborderid +
                ", cardpaiedtime=" + cardpaiedtime +
                ", thirdendtime=" + thirdendtime +
                ", fromsubid=" + fromsubid +
                ", wareQD='" + wareQD + '\'' +
                ", thirdcode='" + thirdcode + '\'' +
                ", productcode='" + productcode + '\'' +
                ", picurl='" + picurl + '\'' +
                ", num=" + num +
                ", suborders=" + suborders +
                ", areaname='" + areaname + '\'' +
                ", acqouname='" + acqouname + '\'' +
                ", userid='" + userid + '\'' +
                ", statestr='" + statestr + '\'' +
                ", fromorderid=" + fromorderid +
                ", billdate=" + billdate +
                ", jdCompSList=" + jdCompSList +
                ", jdCompRList=" + jdCompRList +
                ", jdCompatibleServiceDetailDTO=" + jdCompatibleServiceDetailDTO +
                ", jdAfterSaleDto=" + jdAfterSaleDto +
                '}';
    }
}
