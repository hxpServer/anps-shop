package com.kld.order.dto.cardcore;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2015/8/16.
 */
public class CardDTO {

    public String orderId;
    public BigDecimal cardno;
    public BigDecimal tradeMoney;
    public String tradeType;
    public String tradeId;
    public String tradeTime;
    public String mac;


    public String billDate;
    public String orderType;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getCardno() {
        return cardno;
    }

    public void setCardno(BigDecimal cardno) {
        this.cardno = cardno;
    }

    public BigDecimal getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return "CardDTO{" +
                "orderId='" + orderId + '\'' +
                ", cardno=" + cardno +
                ", tradeMoney=" + tradeMoney +
                ", tradeType='" + tradeType + '\'' +
                ", tradeId='" + tradeId + '\'' +
                ", tradeTime='" + tradeTime + '\'' +
                ", mac='" + mac + '\'' +
                '}';
    }
}
