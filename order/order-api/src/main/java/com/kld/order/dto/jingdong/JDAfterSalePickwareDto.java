package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 *
 * 取件信息实体，即原商品如何返回京东或者卖家，如果不为取件方式，默认设置订单中省市县镇信息
 */
public class JDAfterSalePickwareDto {

    public Integer pickwareType;//取件方式 必填 4 上门取件 7 客户送货  40客户发货
    public Integer pickwareProvince;//取件省    必填
    public Integer pickwareCity; //取件市    必填
    public Integer pickwareCounty;//取件县   必填
    public Integer pickwareVillage;// 取件乡镇   必填
    public String pickwareAddress;//取件街道地址  最多500字符,  必填

    public Integer getPickwareType() {
        return pickwareType;
    }

    public void setPickwareType(Integer pickwareType) {
        this.pickwareType = pickwareType;
    }

    public Integer getPickwareProvince() {
        return pickwareProvince;
    }

    public void setPickwareProvince(Integer pickwareProvince) {
        this.pickwareProvince = pickwareProvince;
    }

    public Integer getPickwareCity() {
        return pickwareCity;
    }

    public void setPickwareCity(Integer pickwareCity) {
        this.pickwareCity = pickwareCity;
    }

    public Integer getPickwareCounty() {
        return pickwareCounty;
    }

    public void setPickwareCounty(Integer pickwareCounty) {
        this.pickwareCounty = pickwareCounty;
    }

    public Integer getPickwareVillage() {
        return pickwareVillage;
    }

    public void setPickwareVillage(Integer pickwareVillage) {
        this.pickwareVillage = pickwareVillage;
    }

    public String getPickwareAddress() {
        return pickwareAddress;
    }

    public void setPickwareAddress(String pickwareAddress) {
        this.pickwareAddress = pickwareAddress;
    }
}
