package com.kld.order.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.order.api.DzExceptionService;
import com.kld.order.dao.DzExceptionDao;
import com.kld.order.po.DzException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/13.
 */
@Service
public class DzExceptionServiceImpl implements DzExceptionService {

    @Resource
    private DzExceptionDao dzExceptionDao;

    @Override
    public  int saveDzException(DzException dzException){return dzExceptionDao.insert(dzException);}

    @Override
    public DzException getDzExceptionByID(Integer id) {
        return dzExceptionDao.get("getDzExceptionByID",id);
    }

    @Override
    public List<DzException> getAllDzExceptioinList(Map<String,Object> map){
        return dzExceptionDao.find("getAllDzExceptioinList",map);
    }

    @Override
    public PageInfo getAllDzExceptioinList(Map<String,Object> map,int PageNum,int PageSize){
        PageHelper.startPage(PageNum,PageSize);
        List<DzException> list = dzExceptionDao.find("getAllDzExceptioinList",map);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @Override
    public int updateDzExceptionById(BigDecimal id,Integer pt,String pm,Integer ps){
        HashMap<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("pt",pt);
        map.put("pm",pm);
        map.put("ps",ps);

        return dzExceptionDao.update("updateDzExceptionById",map);
    }

    @Override
    public  List<DzException> getAllDzApprovalExceptioinList(Map<String,Object> map){return dzExceptionDao.find("getAllDzApprovalExceptioinList",map);}

    @Override
    public int updateDzExceptionByIds(Integer processstate,List<Integer> list){
        HashMap<String, Object> map = new HashMap<>();
        map.put("processstate",processstate);
        map.put("list",list);
        return dzExceptionDao.update("updateDzExceptionByIds",map);}

    @Override
    public  DzException getProcessTypeById(Integer id){return dzExceptionDao.get("getProcessTypeById",id);}

    @Override
    public DzException getDzExceptionByOrderId(String orderId,Integer ordertype){
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderId",orderId);
        map.put("ordertype",ordertype);
        return dzExceptionDao.get("getDzExceptionByOrderId",map);}
}
