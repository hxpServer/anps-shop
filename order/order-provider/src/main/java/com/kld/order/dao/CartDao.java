/**
 * 58.com Inc.
 * Copyright (c) 2005-2015 All Rights Reserved.
 */
package com.kld.order.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.order.po.Cart;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 
  */
@Repository
public class CartDao extends SimpleDaoImpl<Cart>{

//    public int insertSelective(Cart cart);
//
//    public int deleteCartByIds(@Param("list") List<Integer> list);
//
//    public Integer deleteCart(Cart cart);
//
//    public List<Cart> getCartList(Cart cart);
//
//    public List<Cart> getCartListByMap(Map<String, Object> map);
//
//
//    public Integer update(Cart cart);
//
//    public Integer insertCartList(@Param("list") List<Cart> list);
//
//    Integer getTotalQuantity(@Param("userID") String userID, @Param("cartids") List<Integer> cartids);
}
