package com.kld.sys.api;

import com.kld.sys.po.SysApproval;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.23
 */
public interface ISysApprovalService {
    int insert(SysApproval record);

    int insertSelective(SysApproval record);
    /**
     * 审批
     * @param state
     * @param list
     * @return
     */
    int updateApprovalByIds(Integer state, List<Integer> list);

    int  updateApprovalByBsnsiid(Integer state, String bsnsid, Integer approvetype);

    SysApproval selectApprovalByBsnsid(Map<String, Object> map);

    int  updateApprovalByBsnsiidandType(Integer state, String bsnsid, Integer approvetype, String opinion);

    int approveProductsByIds(Integer state, List<Integer> list, String opinion);

    SysApproval selectApprovalByBsnsid2(Map<String, Object> map);

    int  updateApprovalPointsByBsnsiid(Integer state, Integer points, String username, String bsnsid, Integer approvetype, double discountrate);

    int updateApprovalUser(HashMap<String, Object> map);
}
