package com.kld.sys.po;

import java.io.Serializable;
import java.util.Date;

/**
 * hejinping 2016.3.23
 */
public class SysApproval implements Serializable {
    private static final long serialVersionUID = -504699169399014491L;
    private Integer id;

    private Integer state;

    private String bsnsid;

    private String approver;

    private Date approvetime;

    private String oucode;

    private Integer approvetype;

    private String remark;

    private Integer points;

    private Double discountrate;

    private String opinion;

    private Date createtime;

    public Double getDiscountrate() {
        return discountrate;
    }

    public void setDiscountrate(Double discountrate) {
        this.discountrate = discountrate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getBsnsid() {
        return bsnsid;
    }

    public void setBsnsid(String bsnsid) {
        this.bsnsid = bsnsid == null ? null : bsnsid.trim();
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver == null ? null : approver.trim();
    }

    public Date getApprovetime() {
        return approvetime;
    }

    public void setApprovetime(Date approvetime) {
        this.approvetime = approvetime;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode == null ? null : oucode.trim();
    }

    public Integer getApprovetype() {
        return approvetype;
    }

    public void setApprovetype(Integer approvetype) {
        this.approvetype = approvetype;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getPoints() {
        return points;
    }
    public void setPoints(Integer points) {
        this.points = points;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }
    public String getOpinion() {
        return opinion;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
