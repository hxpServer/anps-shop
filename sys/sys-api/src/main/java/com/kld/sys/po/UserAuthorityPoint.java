package com.kld.sys.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/29.
 */
public class UserAuthorityPoint implements Serializable {
    private static final long serialVersionUID = -6276712984062876228L;
    private String funccode;
    private String parentcode;
    private String name;
    private String picurl;
    private String targeturl;
    private BigDecimal isopenwindow;
    private BigDecimal orderno;

    public String getFunccode() {
        return funccode;
    }

    public void setFunccode(String funccode) {
        this.funccode = funccode;
    }

    public String getParentcode() {
        return parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getTargeturl() {
        return targeturl;
    }

    public void setTargeturl(String targeturl) {
        this.targeturl = targeturl;
    }

    public BigDecimal getIsopenwindow() {
        return isopenwindow;
    }

    public void setIsopenwindow(BigDecimal isopenwindow) {
        this.isopenwindow = isopenwindow;
    }

    public BigDecimal getOrderno() {
        return orderno;
    }

    public void setOrderno(BigDecimal orderno) {
        this.orderno = orderno;
    }
}
