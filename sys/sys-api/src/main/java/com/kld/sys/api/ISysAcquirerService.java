package com.kld.sys.api;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.sys.po.SysAcquirer;

import java.util.List;
import java.util.Map;

/**
 * Created by xiaohe on 2016/3/28.
 */
public interface ISysAcquirerService {
    /**
     * 查询收单方管理
     * @param map
     * @return
     */
    public ResultMsg getAcquirerList(Map<String,Object> map,int pageNum,int pageSize);

    /**
     * 插入数据 收单方管理
     * @param sysAcquirer
     * @return
     */
    public Integer insertAcquirer(SysAcquirer sysAcquirer);
    public Integer updateAcquirer(SysAcquirer sysAcquirer);
}
