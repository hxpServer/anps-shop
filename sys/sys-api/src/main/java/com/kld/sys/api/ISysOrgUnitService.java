package com.kld.sys.api;

import com.kld.common.framework.dto.TreeNode;
import com.kld.sys.po.SysOrgUnit;

import java.util.List;
import java.util.Map;
/**
 * hejinping 2016.3.23
 */
public interface ISysOrgUnitService {

    int updateByPrimaryKeySelective(SysOrgUnit record);

    List<SysOrgUnit> getSysOrgunitList(Map<String, Object> map);

    SysOrgUnit getSysOrgunitByOuCode(String oucode);

    List<TreeNode> getTreeNodesByPCode(String pcode);

}
