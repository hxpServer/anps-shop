package com.kld.sys.api;


import com.kld.common.framework.dto.ResultMsg;
import com.kld.sys.po.SysRole;

import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
public interface ISysRoleService {


    List<SysRole> getRoleList(Map<String, Object> map);

    ResultMsg getSysRoleFunList(Map<String, Object> map,Integer pageNum,Integer pageSize);

    ResultMsg delete(String rolecode);

    ResultMsg updateSysRoleByCode(SysRole sys_role);

    ResultMsg insert(SysRole sys_role);

    /**
     * 通过rolecode查询是否有重复数据
     * @param sys_role
     * @return
     */
    SysRole selectSysRoleByRolecode(SysRole sys_role);
}
