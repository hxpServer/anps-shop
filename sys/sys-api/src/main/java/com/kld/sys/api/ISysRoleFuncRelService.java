package com.kld.sys.api;
import com.kld.sys.po.SysRoleFuncRel;

/**
 * hejinping 2016.3.24
 */
public interface ISysRoleFuncRelService {


    int deleteRoleFuncByRolecode(String rolecode);

    int insert(SysRoleFuncRel record);
}
