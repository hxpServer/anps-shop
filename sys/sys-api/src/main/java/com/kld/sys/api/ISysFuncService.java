package com.kld.sys.api;

import com.kld.sys.po.SysFunc;

import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
public interface ISysFuncService {


    List<SysFunc> getFuncList(Map<String, Object> map);
}
