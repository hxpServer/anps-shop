package com.kld.sys.api;


import com.kld.common.framework.dto.ResultMsg;
import com.kld.sys.po.SysUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * hejinping 2016.3.23
 */
public interface ISysUserService {

    ResultMsg findSysUserByUserName(String userName) ;

    SysUser querySysUserByUserName(String userName);

    SysUser selectUserMoreInfo(String username);

    public ResultMsg<SysUser> getSysUserList(Map<String,Object> map,Integer pageNum,Integer pageSize);

    int insert(SysUser user);

    ResultMsg insertWithTrans(SysUser user) throws Exception;
    int update(SysUser user);

    List<SysUser> getSysUserListByRolecode(String roleCode);

    ResultMsg getApprovalUserList(int pageNum,int pageSize);

    int updateByPrimaryKeySelective(SysUser sys_user);

    SysUser getUserByUserName(String username);
    int updateAppBatch(SysUser sysUser,HashMap<String,Object> map);
}
