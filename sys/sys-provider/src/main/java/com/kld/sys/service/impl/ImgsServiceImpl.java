package com.kld.sys.service.impl;

import com.kld.sys.api.ImgsService;
import com.kld.sys.dao.ImgsDao;
import com.kld.sys.po.Imgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/28.
 */
@Service("imgsService")
public class ImgsServiceImpl implements ImgsService{
    Logger logger  = LoggerFactory.getLogger(ImgsServiceImpl.class);
    @Autowired
    private ImgsDao imgsDao;

    @Override
    public int insertSelective(Imgs record) {
        return imgsDao.insert("insertSelective",record);
    }

    @Override
    public Imgs selectByPrimaryKey(BigDecimal id) {
        return imgsDao.get("selectByPrimaryKey",id);
    }

    @Override
    public int updateByPrimaryKeySelective(Imgs record) {
        return imgsDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public List<Imgs> getImgListByActid(Integer actid) {
        return imgsDao.find("getImgListByActid",actid);
    }

    @Override
    public int  updatePrimaryByBsnsid(BigDecimal bsnsid,boolean isprimary) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("bsnsid",bsnsid);
        map.put("isprimary",isprimary?1:0);
        return imgsDao.update("updatePrimaryByBsnsid",map);
    }

    @Override
    public Imgs getPrimaryImg(BigDecimal bsnsid, int clsid) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("bsnsid",bsnsid);
        map.put("clsid",clsid);
        return imgsDao.get("getPrimaryImg",map);
    }

}
