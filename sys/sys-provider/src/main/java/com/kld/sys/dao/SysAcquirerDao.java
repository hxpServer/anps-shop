package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.SysAcquirer;
import org.springframework.stereotype.Repository;

/**
 * hejinping 2016.3.24
 */
@Repository
public class SysAcquirerDao extends SimpleDaoImpl<SysAcquirer> {

}