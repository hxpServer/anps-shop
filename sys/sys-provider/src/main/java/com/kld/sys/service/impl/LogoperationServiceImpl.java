package com.kld.sys.service.impl;

import com.kld.sys.dao.LogoperationDao;

import com.kld.sys.api.ILogoperationService;
import com.kld.sys.po.Logoperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by jw on 2015/7/30.
 */
@Service
public class LogoperationServiceImpl implements ILogoperationService {
    @Autowired
    private LogoperationDao logoperationDao;
    @Override
    public int insert(Logoperation record){
        return  logoperationDao.insert(record);
    }
    @Override
    public Logoperation createLogperation(Map<String,String> logmap){
        String statetext = "";
        String opinion = logmap.get("opinion");
        String ids = logmap.get("ids");
        String table = logmap.get("table");
        String operationType = logmap.get("operationType");
        String username = logmap.get("username");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowDate = sdf.format(new Date());
        String objectName = logmap.get("objectName");
        Logoperation logoperation = new Logoperation();
        logoperation.setCreatetime(new Date());
        logoperation.setCreateuser(username);
        logoperation.setOperationobject(objectName);

        if("1".equals(operationType)){
            statetext = "添加";
        }else if("2".equals(operationType)){
            statetext = "删除";
        }else if("3".equals(operationType)){
            statetext = "修改";
        }else if("5".equals(operationType)){
            statetext = "审核通过";
        }else if("6".equals(operationType)){
            statetext = "审核拒绝";
        }
        logoperation.setOperationtype(new BigDecimal(operationType));
        String content ="模块:"+objectName+",表名:"+table+",对象ID:"+ids+",操作:"+statetext+",原因:"+opinion+",操作人:"+username+",时间:"+nowDate;
        logoperation.setContent(content);
        return logoperation;
    }



}
