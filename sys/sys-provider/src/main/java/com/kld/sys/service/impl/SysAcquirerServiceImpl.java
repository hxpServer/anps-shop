package com.kld.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.sys.api.ISysAcquirerService;
import com.kld.sys.api.ISysUserService;
import com.kld.sys.dao.SysAcquirerDao;
import com.kld.sys.dao.SysApprovalDao;
import com.kld.sys.dao.SysUserDao;
import com.kld.sys.po.SysAcquirer;
import com.kld.sys.po.SysApproval;
import com.kld.sys.po.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.23
 */
@Service
class SysAcquirerServiceImpl implements ISysAcquirerService {


  @Autowired
  private SysAcquirerDao sysAcquirerDao;


  @Override
  public ResultMsg getAcquirerList(Map<String,Object> map,int pageNum,int pageSize) {

    PageHelper.startPage(pageNum, pageSize);
    PageInfo pageInfo=new PageInfo(sysAcquirerDao.find("getAcquirerList", map));
    return  PageInfoResult.PageInfoMsg(pageInfo);
  }

  @Override
  public Integer insertAcquirer(SysAcquirer sysAcquirer) {
    return sysAcquirerDao.insert("insertAcquirer", sysAcquirer);
  }

  @Override
  public Integer updateAcquirer(SysAcquirer sysAcquirer) {
    return sysAcquirerDao.update("updateAcquirer", sysAcquirer);
  }
}