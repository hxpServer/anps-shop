package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.SysUser;
import org.springframework.stereotype.Repository;
/**
 * hejinping 2016.3.23
 */
@Repository
public class SysUserDao extends SimpleDaoImpl<SysUser> {

}