package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.UserAuthorityPoint;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
 * Created by 曹不正 on 2016/3/29.
 */
@Repository
public class UserAuthorityPointDao extends SimpleDaoImpl<UserAuthorityPoint>{

}
