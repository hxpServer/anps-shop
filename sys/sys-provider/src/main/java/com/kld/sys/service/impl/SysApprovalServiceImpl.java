package com.kld.sys.service.impl;
import com.kld.sys.api.ISysApprovalService;
import com.kld.sys.dao.SysApprovalDao;
import com.kld.sys.po.SysApproval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.23
 */
@Service
public class SysApprovalServiceImpl implements ISysApprovalService {
    @Autowired
    private SysApprovalDao sysApprovalDao;

    @Override
    public int insert(SysApproval record){
        return sysApprovalDao.insert(record);
    }

    @Override
    public int insertSelective(SysApproval record){
        return sysApprovalDao.insert("insertSelective",record);
    }

    /**
     * 审批
     * @param state
     * @param list
     * @return
     */
    @Override
    public int updateApprovalByIds(Integer state,List<Integer> list){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("state",state);
        map.put("list",list);
        return  sysApprovalDao.update("updateApprovalByIds",map);
    }
    @Override
    public int  updateApprovalByBsnsiid(Integer state,String bsnsid,Integer approvetype){
        SysApproval sysApproval =new SysApproval();
        sysApproval.setBsnsid(bsnsid);
        sysApproval.setApprovetype(approvetype);
        return sysApprovalDao.update("updateApprovalByBsnsiid", sysApproval);
    }

    public SysApproval selectApprovalByBsnsid(Map<String, Object> map){
        return sysApprovalDao.get("selectApprovalByBsnsid",map);
    }

    public int  updateApprovalByBsnsiidandType(Integer state,String bsnsid,Integer approvetype,String opinion){
        SysApproval sysApproval =new SysApproval();
        sysApproval.setBsnsid(bsnsid);
        sysApproval.setApprovetype(approvetype);
        sysApproval.setOpinion(opinion);
        return sysApprovalDao.update("updateApprovalByBsnsiidandType", sysApproval);
    }
    @Override
    public int approveProductsByIds(Integer state,List<Integer> list,String opinion){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("state",state);
        map.put("list",list);
        map.put("opinion",opinion);
        return sysApprovalDao.update("approveProductsByIds",map);
    }
    @Override
    public SysApproval selectApprovalByBsnsid2(Map<String, Object> map){
        return sysApprovalDao.get("selectApprovalByBsnsid2",map);
    }
    @Override
    public int  updateApprovalPointsByBsnsiid(Integer state,Integer points,String username,String bsnsid,Integer approvetype,double discountrate){
        SysApproval sysApproval =new SysApproval();
        sysApproval.setState(state);
        sysApproval.setPoints(points);
        sysApproval.setApprover(username);
        sysApproval.setBsnsid(bsnsid);
        sysApproval.setApprovetype(approvetype);
        sysApproval.setDiscountrate(discountrate);
        return sysApprovalDao.update("updateApprovalPointsByBsnsiid", sysApproval);
    }

    @Override
    public int updateApprovalUser(HashMap<String, Object> map) {
        return sysApprovalDao.update("updateApprovalUser",map);
    }

}
