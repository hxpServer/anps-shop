package com.kld.sys.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.sys.api.ISysRoleFuncRelService;
import com.kld.sys.api.ISysRoleService;
import com.kld.sys.api.ISysUserService;
import com.kld.sys.dao.SysRoleDao;
import com.kld.sys.dao.SysRoleFuncRelDao;
import com.kld.sys.po.SysRole;
import com.kld.sys.po.SysRoleFuncRel;
import com.kld.sys.po.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysRoleServiceImpl implements ISysRoleService {
    @Autowired
    private SysRoleDao sysRoleDao;
    @Autowired
    private SysRoleFuncRelDao sysRoleFuncRelDao;
    @Autowired
    private ISysRoleFuncRelService sysRoleFuncRelService;
    @Autowired
    private ISysUserService sysUserService;

    @Override
    public List<SysRole> getRoleList(Map<String,Object> map){
        return sysRoleDao.find("getSysRoleList",map);
    }
    @Override
    public ResultMsg getSysRoleFunList(Map<String,Object> map,Integer pageNum,Integer pageSize){

        PageHelper.startPage(pageNum, pageSize);
        List<SysRole> roleList = sysRoleDao.find("getSysRoleFunList", map);
        PageInfo pageInfo = new PageInfo(roleList);
        return PageInfoResult.PageInfoMsg(pageInfo);
    }
    @Override
    public ResultMsg delete(String rolecode){
        ResultMsg rmsg = new ResultMsg();
        List<SysUser> list = sysUserService.getSysUserListByRolecode(rolecode);
        if(list.size()>0){
            rmsg.setResult(false);
            rmsg.setMsg("删除操作失败！该角色使用中");
        }else{
            int del = sysRoleDao.deleteLogic(rolecode);
            if(del >0){
                rmsg.setResult(true);
                rmsg.setMsg("删除操作成功！");
            }else{
                rmsg.setResult(false);
                rmsg.setMsg("删除操作失败！");
            }
        }
        return rmsg;
    }
    @Override
    public ResultMsg updateSysRoleByCode(SysRole sys_role){
        ResultMsg rmsg = new ResultMsg();
        String[] funcIds = sys_role.getRolefunccodes().split(",");
        sys_role.setModitime(new Date());
        int upd = sysRoleDao.update("updateSysRoleByCode",sys_role);
        //先删除原角色菜单关系
        int flag1 = sysRoleFuncRelService.deleteRoleFuncByRolecode(sys_role.getRolecode());
        boolean isSuccess = true;
        for(int i=0;i<funcIds.length;i++){//保存角色菜单关系表
            SysRoleFuncRel sys_rolefuncrel = new SysRoleFuncRel();
            String funcId = funcIds[i];
            sys_rolefuncrel.setRolecode(sys_role.getRolecode());
            sys_rolefuncrel.setFunccode(funcId);
            sys_rolefuncrel.setModitime(new Date());
            int flag2 = sysRoleFuncRelService.insert(sys_rolefuncrel);
            if(flag2>0){

            }else{
                isSuccess = false;

            }
        }
        if(upd>0 && isSuccess){
            rmsg.setResult(true);
            rmsg.setMsg("更新成功！");
        }else{
            rmsg.setResult(false);
            rmsg.setMsg("更新失败！");
        }
        return rmsg;
    }
    @Override
    public ResultMsg insert(SysRole sys_role){
        ResultMsg rmsg = new ResultMsg();
        SysRole isRole = selectSysRoleByRolecode(sys_role);
        String[] funcIds = sys_role.getRolefunccodes().split(",");
        if(isRole == null){//角色编码是否重复
            sys_role.setCreatetime(new Date());
            sys_role.setState(1);
            int flag = sysRoleDao.insert(sys_role);
            boolean isSuccess = true;
            for(int i=0;i<funcIds.length;i++){//保存角色菜单关系表
                SysRoleFuncRel sys_rolefuncrel = new SysRoleFuncRel();
                String funcId = funcIds[i];
                sys_rolefuncrel.setRolecode(sys_role.getRolecode());
                sys_rolefuncrel.setFunccode(funcId);
                sys_rolefuncrel.setCreatetime(new Date());
                int flag2 = sysRoleFuncRelDao.insert(sys_rolefuncrel);
                if(flag2>0){

                }else{
                    isSuccess = false;
                }
            }
            if(flag>0  && isSuccess){
                rmsg.setResult(true);
                rmsg.setMsg("添加成功！");
            }else{
                rmsg.setResult(false);
                rmsg.setMsg("添加失败！");
            }
        }else{
            rmsg.setResult(false);
            rmsg.setMsg("添加失败,角色编码已存在！");
        }
        return rmsg;
    }
    @Override
    public SysRole selectSysRoleByRolecode(SysRole sys_role){
        return sysRoleDao.get("selectSysRoleByRolecode",sys_role);
    }

}
