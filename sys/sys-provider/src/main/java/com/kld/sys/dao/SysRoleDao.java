package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.SysRole;
import org.springframework.stereotype.Repository;
/**
 * hejinping 2016.3.24
 */
@Repository
public class SysRoleDao extends SimpleDaoImpl<SysRole> {

    public int deleteLogic(String rolecode) {
        SysRole sysRole = new SysRole();
        sysRole.setRolecode(rolecode);
        return update("deleteLogic",sysRole);
    }
}