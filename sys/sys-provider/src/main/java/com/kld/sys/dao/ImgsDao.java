package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.Imgs;
import org.springframework.stereotype.Repository;

/**
 * Created by 曹不正 on 2016/3/28.
 */
@Repository
public class ImgsDao extends SimpleDaoImpl<Imgs> {
}
