package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.Logoperation;
import org.springframework.stereotype.Repository;


@Repository
public class LogoperationDao extends SimpleDaoImpl<Logoperation>{

}
