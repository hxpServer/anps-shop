package com.kld.shop.controller.webController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.cms.api.ISysArticleService;
import com.kld.cms.po.SysArticle;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.product.api.IProProductService;
import com.kld.shop.controller.base.BaseWebController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Modify by caoz 2016/3/30.
 * problems...iProProductService.getCategoryTree());
 */
@Controller
@RequestMapping("article")
public class ArticleWebController extends BaseWebController {

    @Resource
    private IProProductService iProProductService;

    @Resource
    private ISysArticleService iSysArticleService;

    /**
     * 查询前端最新文章列表或所有文章列表
     * @param num
     * @return
     */
    @ResponseBody
    @RequestMapping("/getArtWebList")
    public ResultMsg getArtWebList(Integer num){
        ResultMsg msg = new ResultMsg();
        List<SysArticle> list;
        if (null!=num){//最新动态
            list = iSysArticleService.getArtWebList(num);
            msg.setResult(true);
            msg.setData(list);
        }else {//所有文章
            return iSysArticleService.getArtWebList(pageNum,pageSize);
        }
        return msg;
    }

    /**
     * 根据ID查询文章详情
     * @param id
     * @return
     */
    @RequestMapping("getArtWebByPrimaryKey")
    public ModelAndView getArtWebByPrimaryKey(BigDecimal id){
        Map<String,Object> map= new HashMap<String,Object>();
        SysArticle sysArticle = iSysArticleService.selectByPrimaryKey(id.intValue());
        map.put("article",sysArticle);
        // TODO: 2016/3/30
//        map.put("category", iProProductService.getCategoryTree());
        return new ModelAndView("/draw/dynamicDetails",map);
    }
}
