package com.kld.shop.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.cms.api.ISysArticleService;
import com.kld.cms.po.SysArticle;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.shop.controller.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by njh on 2015/10/29 0029.
 */
@Controller
@RequestMapping("/admin/article")
public class SysArticleController extends BaseController {


    @Resource
    private ISysArticleService sysArticleService;

    @RequestMapping("/articleList")
    public String ArticleList(){
        return "/admin/article/articleList";
    }

    /**
     * 得到动态列表
     * @return
     */
    @ResponseBody()
    @RequestMapping("/getArticleList")
    public ResultMsg GetArticleList(){
        ResultMsg resultMsg=null;
        resultMsg =sysArticleService.getArticleList(getPageNum(), getPageSize()) ;
        return resultMsg;
    }

    /**
     * 编辑或添加动态
     * @param article
     * @return
     */
    @ResponseBody()
    @RequestMapping("/editArticle")
    public ResultMsg EditArticle(SysArticle article){
        ResultMsg  result=new ResultMsg();
        if(article.getId() == null){
            //添加动态
            article.setOucode(getUserOuCode());
            article.setCreater(getUserName());
            article.setCreatetime(new Date());
            article.setIsdel(0);
            int i=sysArticleService.insertSelective(article);
            if(i>0){
                result.setResult(true);
                result.setMsg("添加成功");
            }else{
                result.setResult(false);
                result.setMsg("添加失败");
            }
        }else {
            //编辑动态
            if(article.getContent()==null){
                article.setContent("");
            }
           int i=sysArticleService.updateByPrimaryKeySelective(article);
            if(i>0){
                result.setResult(true);
                result.setMsg("更新成功");
            }else {
                result.setResult(false);
                result.setMsg("更新失败");
            }
        }
        return  result;
    }

    /**
     * 批量删除动态
     * @param ids
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/deleteArticles",method = RequestMethod.POST)
    public ResultMsg DeleteArticles(String ids){
        ResultMsg rmsg = new ResultMsg();
        try {
            String[] idList = ids.split(",");
            List<Integer> list = new ArrayList<Integer>();
            for(int i=0;i<idList.length;i++){
                list.add(Integer.valueOf(idList[i]));
            }
            int flag =sysArticleService.deleteArticlesByIds(list);
            if(flag>0){
                rmsg.setResult(true);
                rmsg.setMsg("操作成功");
            }else{
                rmsg.setResult(false);
                rmsg.setMsg("操作失败");
            }
        }catch (Exception e) {
            logger.info("批量删除文章失败:"+e.getMessage(),e);
            rmsg.setResult(false);
            rmsg.setMsg("操作失败" + e.getMessage());
        }
        return rmsg;
    }

}
