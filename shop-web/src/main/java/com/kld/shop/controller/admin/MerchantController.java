package com.kld.shop.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.kld.shop.controller.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jw on 2015/7/30.
 */
@Controller
@RequestMapping("/admin/merchant")
public class MerchantController extends BaseController {
//    @Resource
//    private IMerchantService merchantService;
//    @Resource
//    private IProductService productService;
//
//    @RequestMapping("/merchantList")
//    public ModelAndView list (){
//        ArrayList<String> list = GetUserFuncList();
//        return new ModelAndView("admin/merchant/merchantList","list",list);
//        //return  "admin/merchant/merchantList";
//    }
//
//    /**
//     * 获取商家列表
//     * @return
//     */
//    @ResponseBody()
//    @RequestMapping("/getMerchantList")
//    public ResultMsg getMerchantList (){
//        PageHelper pageHelper = new PageHelper();
//        pageHelper.startPage(getPageNum(), getPageSize());
//        Map<String,Object> map = new HashMap<String, Object>();
//        List<Merchant> merchantList = merchantService.getMerchantList(map);
//        PageInfo pageInfo = new PageInfo(merchantList);
//
//
//        return PageInfoResult.PageInfoMsg(pageInfo);
//    }
//
//    @ResponseBody()
//    @RequestMapping("getMerchants")
//    public List<Merchant> getMerchants(){
//        Map<String,Object> map = new HashMap<String, Object>();
//        //oucode
//        map.put("oucode",getUserOuCode());
//        map.put("oulevel",getUserOuLevel());
//        List<Merchant> merchantList = merchantService.getMerchantList(map);
//        return merchantList;
//    }
//
//    /**
//     * 新增
//     * @param merchant
//     * @return
//     * @throws Exception
//     */
//    @ResponseBody()
//    @RequestMapping(value = "save" ,method = RequestMethod.POST)
//    public ResultMsg save(Merchant merchant)throws Exception{
//        ResultMsg rmsg = new ResultMsg();
//        try {
//            String code = merchantService.selectMaxMcode();
//            if(Integer.valueOf(code)<1000){
//                merchant.setCode("1000");
//            }else{
//                merchant.setCode(String.valueOf(Integer.valueOf(code) + 1));
//            }
//            merchant.setCreator(getUserName());
//                int flag = merchantService.insert(merchant);
//                if(flag>0){
//                    rmsg.setResult(true);
//                    rmsg.setMsg("添加成功！" );
//                }else{
//                    rmsg.setResult(false);
//                    rmsg.setMsg("添加失败！" );
//                }
//
//        }catch (Exception e) {
//            logger.info("删除供应商失败:" + e.getMessage(), e);
//            rmsg.setResult(false);
//            rmsg.setMsg("操作失败！" + e.getMessage());
//        }
//        return rmsg;
//    }
//
//    /**
//     * 修改
//     * @param merchant
//     * @return
//     * @throws Exception
//     */
//    @ResponseBody()
//    @RequestMapping(value = "update" ,method = RequestMethod.POST)
//    public ResultMsg update(Merchant merchant)throws Exception{
//        ResultMsg rmsg = new ResultMsg();
//        try {
//                String merchantcode = request.getParameter("merchantcode");
//                int flag =  merchantService.updateMerchantById(merchant);
//                if(flag>0){
//                    rmsg.setResult(true);
//                    rmsg.setMsg("更新成功！" );
//                }else{
//                    rmsg.setResult(false);
//                    rmsg.setMsg("更新失败！" );
//                }
//
//
//
//        }catch (Exception e) {
//            logger.info("更新供应商失败:"+e.getMessage(),e);
//            rmsg.setResult(false);
//            rmsg.setMsg("操作失败！" + e.getMessage());
//        }
//        return rmsg;
//    }
//
//    /**
//     * 删除
//     * @return
//     * @throws Exception
//     */
//    @ResponseBody()
//    @RequestMapping(value = "delete" ,method = RequestMethod.POST)
//    public ResultMsg delete()throws Exception{
//        ResultMsg rmsg = new ResultMsg();
//        try {
//            String code = request.getParameter("code");
//            List<Product> list = productService.getTopProductListByMercode(code);
//            if(list.size()>0){
//                rmsg.setResult(false);
//                rmsg.setMsg("删除失败！该供应商使用中" );
//            }else{
//                int id = Integer.valueOf(request.getParameter("id"));
//                int flag = merchantService.delete(id);
//                if(flag>0){
//                    rmsg.setResult(true);
//                    rmsg.setMsg("删除成功！" );
//                }else{
//                    rmsg.setResult(false);
//                    rmsg.setMsg("删除失败！" );
//                }
//            }
//        }catch (Exception e) {
//            logger.info("删除供应商失败:"+e.getMessage(),e);
//            rmsg.setResult(false);
//            rmsg.setMsg("操作失败！" + e.getMessage());
//        }
//        return rmsg;
//    }

}
