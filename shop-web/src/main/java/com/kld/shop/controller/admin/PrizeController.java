package com.kld.shop.controller.admin;

import com.kld.shop.controller.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Dan on 2015/10/29.
 */
@Controller
@RequestMapping("/admin/prize")
public class PrizeController extends BaseController {

    /*@Resource
    private IPrizeService prizeService;

    @Resource
    private ICacheService cacheService;

    @RequestMapping(value = "prizeList")
    public String prizeList(){
        return "/admin/prize/prizeList";
    }*/

    /**
     * 获取奖品列表
     * @return
     *//*
    @ResponseBody()
    @RequestMapping("getPrizeList")
    public ResultMsg getPrizeList(){
        String sFromdate = request.getParameter("sFromDate");
        String sType = request.getParameter("sType");
        String sTodate = request.getParameter("sToDate");
        String q = request.getParameter("q");

        Map<String,Object> map = new HashMap<String,Object>();
        if(StringUtils.isNotBlank(sFromdate)){ map.put("fromdate", DateUtils.parseDate(sFromdate));}
        if(StringUtils.isNotBlank(sTodate)){ map.put("todate",DateUtils.plusOneDay(sTodate));}
        if(StringUtils.isNotBlank(sType)&& !("0".equals(sType))){ map.put("type",sType);}
        if("1".equals(getUserOuLevel())){

        }else {
            map.put("oucode",getUserOuCode());
        }
        PageInfo pageInfo = null;
        try {
            pageInfo = prizeService.getPrizeList(map,getPageNum(),getPageSize());
        }catch (Exception e){
            logger.error("获取商品列表列表错误"+e.getMessage(),e);
            e.printStackTrace();
        }
        return PageInfoResult.PageInfoMsg(pageInfo);

    }
    *//**
     * 获取奖品列表
     * @return
     *//*
    @ResponseBody()
    @RequestMapping(value = "getPrizeList2",method = RequestMethod.GET)
    public List<Prize> getPrizeList2(){
        String q = request.getParameter("q");
        String type = request.getParameter("prizetype");
        Map<String,Object> map = new HashMap<String,Object>();
        if(StringUtils.isNotBlank(q)){ map.put("q",q);}
        if(StringUtils.isNotBlank(type)){ map.put("type", Integer.parseInt(type));}
        if("1".equals(getUserOuLevel())){

        }else {
            map.put("oucode",getUserOuCode());
        }

        List<Prize> list = prizeService.getPrizeList(map);
        return list;
    }

    *//**
     * 编辑奖品
     * @param prize
     * @return
     *//*
    @ResponseBody()
    @RequestMapping(value = "editPrize",method = RequestMethod.POST)
    public ResultMsg editPrize(Prize prize){
        ResultMsg rmsg = new ResultMsg();
        prize.setOucode(getUserOuCode());
        if(prize.getId()!=null && prize.getId().intValue()>0){
            //region 编辑奖品
            int upd = prizeService.updateByPrimaryKeySelective(prize);
            if(upd>0){
                rmsg.setResult(true);
                rmsg.setMsg("编辑成功");
                cacheService.delFrontCache(Global.INDEX_JIFENPRIZE);
            }else {
                rmsg.setResult(false);
                rmsg.setMsg("编辑失败");
            }
            //endregion
        }else{

            prize.setCreator(getUserDetails().getUsername());
            int i = prizeService.insertSelective(prize);
            if(i>0){
                rmsg.setResult(true);
                rmsg.setMsg("添加成功");
            }else {
                rmsg.setResult(false);
                rmsg.setMsg("添加失败");
            }
            //endregion
        }
        //清除前台缓存信息
        JedisManager.delObject(Global.INDEX_JIFENPRIZE);
        return rmsg;
    }

    *//**
     * 删除奖品
     * @param ids
     * @return
     *//*
    @ResponseBody()
    @RequestMapping(value = "deletePrize",method = RequestMethod.GET)
    public ResultMsg deletePrize(String ids){
        ResultMsg rmsg = new ResultMsg();
        try{
           rmsg= prizeService.deleteLogicByIds(ids);
        }catch (Exception ex){
            rmsg.setResult(true);
            rmsg.setMsg(ex.getMessage());
        }
        return rmsg;
    }

    @ResponseBody()
    @RequestMapping(value = "uploadPrizePic",method = RequestMethod.POST)
    public ResultMsg uploadPrizePic(){
        ResultMsg rmsg = new ResultMsg();
        String[] paths = {"prize"};
        List<String> list = FileUpload.Upload(request, paths);
        rmsg.setResult(true);
        rmsg.setMsg(list.get(list.size() - 1));
        cacheService.delFrontCache(Global.INDEX_JIFENPRIZE);
        return rmsg;
    }
*/
}
