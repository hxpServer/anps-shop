package com.kld.shop.controller.admin;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.shop.controller.base.BaseController;
import com.kld.sys.api.ISysAcquirerService;
import com.kld.sys.po.SysAcquirer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xiaohe on 2016/3/28.
 */
@Controller
@RequestMapping("/admin/sysAcquirer")
public class SysAcquirerController extends BaseController {

    @Autowired
    private ISysAcquirerService sysAcquirerService;

    @RequestMapping("/acquirerList")
    public String acquirer(){
        return "admin/acquirer/acquirerList";
    }


    @ResponseBody()
    @RequestMapping("/getAcquirerList")
    public ResultMsg getAcquirerList(){
        Map<String,Object> map=new HashMap<String,Object>();
        String ouCodeName  =request.getParameter("ouCodeName");
        if(!"".equals(ouCodeName)&&ouCodeName!=null){
            map.put("ouCodeName",ouCodeName);
        }

        return  sysAcquirerService.getAcquirerList(map,getPageNum(),getPageSize());



    }

    /**
     * 添加修改收单方管理通过id是否存在判断
     * @param sysAcquirer
     * @return
     */
    @ResponseBody()
    @RequestMapping("/editAcquirerURL")
    public ResultMsg editAcquirerList(SysAcquirer sysAcquirer){
        PageInfo pageInfo = null;
        ResultMsg  result=new ResultMsg();
        try{
            if(sysAcquirer.getId()!=null){
                int j=sysAcquirerService.updateAcquirer(sysAcquirer);
                if (j > 0) {
                    result.setResult(true);
                    result.setMsg("修改收单方成功");
                } else {
                    result.setResult(false);
                    result.setMsg("修改收单方失败");
                }
            }else {
                int i = sysAcquirerService.insertAcquirer(sysAcquirer);
                if (i > 0) {
                    result.setResult(true);
                    result.setMsg("添加收单方成功");
                } else {
                    result.setResult(false);
                    result.setMsg("添加收单方失败");
                }
            }
        }catch (Exception e){
            logger.error("收单方管理"+e.getMessage(),e);
            e.printStackTrace();
        }
        return result;
    }

}
