package com.kld.shop.controller.product;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.product.api.IProPropService;
import com.kld.product.api.IProPropValueService;
import com.kld.product.dto.PropertyDto;
import com.kld.product.po.ProProp;
import com.kld.shop.allocation.Constant;
import com.kld.shop.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by anpushang on 2016/3/28.
 * 属性维护
 */
@Controller
@RequestMapping("/admin/prop")
public class ProPropertyerController extends BaseController {

    @Autowired
    private IProPropService proPropService;
    @Autowired
    private IProPropValueService proPropValueService;


    /***
     * 进入列表页面
     * @return
     */
    @RequestMapping(value = "/toList")
    public String toList(){
        return "/admin/prop/prop_list";
    }


    /***
     * 属性查询 集合
     * @param proProp
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/list")
    public ResultMsg list(ProProp proProp){
        PageInfo pageInfo = null;
        try{
            //关联查询属性和属性值的关系
            pageInfo = proPropService.findProProductListPage(getPageNum(),getPageSize(),proProp);
        }catch (Exception e){
            logger.error("属性查询"+e.getMessage(),e);
            e.printStackTrace();
        }
        return PageInfoResult.PageInfoMsg(pageInfo);
    }

    @RequestMapping(value = "/toAddPropPage")
    public String toAddPropPage(){
        return "/admin/prop/prop_add";
    }

    /***
     * 根据属性ID查询信息
     * @param id
     * @return
     */

    @RequestMapping(value = "/getBrandById")
    public ModelAndView getBrandById(Integer id){
        ResultMsg resultMsg = new ResultMsg();
        ModelAndView modelAndView = new ModelAndView();
        try{
            ProProp proProp = proPropService.selectByPrimaryKey(id);
            modelAndView.addObject("proProp",proProp);
            modelAndView.setViewName("/admin/prop/prop_edit");
        }catch (Exception e){
            logger.error("属性查询根据ID"+e.getMessage(),e);
            e.printStackTrace();
        }
        return modelAndView;
    }


    /***
     * 属性删除 物理删除
     * @param id
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/delPropById")
    public ResultMsg delPropById(Integer id){
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proPropService.deleteByPrimaryKey(id);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,Constant.DELETE_SUCCESS) : errorMsg(resultMsg,Constant.DELETE_ERROR);
        }catch (Exception e){
            logger.error("属性删除"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 判断是否可以删除
     * @param id
     * @return
     */
    @RequestMapping(value="/verPassed",method= RequestMethod.POST)
    @ResponseBody
    public ResultMsg verificationPassed(Integer id){
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proPropService.getVerificationPassed(id);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"不能删除") : errorMsg(resultMsg,"可以删除");
        }catch (Exception e){
            logger.error("品牌删除"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }


    /***
     * 属性新增
     * @param proProp
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/saveProp")
    public ResultMsg saveProp(ProProp proProp){
        ResultMsg resultMsg = new ResultMsg();
        try{
            if(null != proProp.getIsSpec() && proProp.getIsSpec() == 1){
                proProp.setIsNeed(1);
                proProp.setPropType(2);
            }
            int resultCount = proPropService.insert(proProp);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"新增成功") : errorMsg(resultMsg,"新增失败");
        }catch (Exception e){
            logger.error("属性新增"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 查询是否存在
     * @param propName
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/checkPropName")
    public ResultMsg checkPropName(String propName){
        ProProp proProp = new ProProp();
        proProp.setPropName(propName);
        ResultMsg resultMsg = new ResultMsg();
        try{
            List<ProProp> proProductList = proPropService.findProProductList(proProp);
            resultMsg = proProductList.size() > 0 ? successMsg(resultMsg,"已经存在") : errorMsg(resultMsg,"可以新增");
        } catch (Exception e) {
            logger.error("查询属性"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg ;
    }

    /***
     * 属性更新
     * @param proProp
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/upPropById")
    public ResultMsg upPropById(ProProp proProp){
        ResultMsg resultMsg = new ResultMsg();
        try{
            if(null != proProp.getIsSpec() && 1 == proProp.getIsSpec()){
                proProp.setIsNeed(1);
                proProp.setPropType(2);
            }
            //查询属性下的属性值
            int resultCount = proPropService.updateByPrimaryKey(proProp);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"更新成功") : errorMsg(resultMsg,"更新失败");
        }catch (Exception e){
            logger.error("品牌更新"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 根据分类ID查询属性信息
     * @param cateId
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/selectProp")
    public List<PropertyDto> selectProp(Integer cateId,Integer isSpec){
        List<PropertyDto> propertyDtoList = null;
        try{
            propertyDtoList = proPropService.selectProp(cateId,isSpec);
        }catch (Exception e){
            logger.error("根据分类ID查询属性信息"+e.getMessage(),e);
            e.printStackTrace();
        }
        return propertyDtoList;
    }


}
