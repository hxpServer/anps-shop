package com.kld.shop.controller.admin;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.dto.TreeNode;
import com.kld.common.util.BuildTreeUtil;
import com.kld.shop.controller.base.BaseController;
import com.kld.sys.api.ISysOrgUnitService;
import com.kld.sys.po.SysOrgUnit;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Dan on 2015/8/3.
 */
@Controller
@RequestMapping("admin/sysOrgunit")
public class SysOrgUnitController extends BaseController {

    @Autowired
    private ISysOrgUnitService sysOrgUnitService;

    @RequestMapping("/orgunitList")
    public ModelAndView orgunitList(){
        ArrayList<String> list =   GetUserFuncList();
        return new ModelAndView("admin/orgunit/orgunitList","list",list);
    }

    /**
     * 获取组织机构
     * @return
     */
    @ResponseBody()
    @RequestMapping("getOrgunitTreeList")
    public List<TreeNode> getOrgunitTreeList(){
        Map<String,Object> map = new HashMap<String,Object>();
        SysOrgUnit sys_orgunit  = sysOrgUnitService.getSysOrgunitByOuCode(getUserOuCode());
      /*  if(!sys_orgunit.getOulevel().equals(1)){
            map.put("oucode",getUserOuCode());
        }*/
        List<TreeNode> treeRootNodes = new ArrayList<TreeNode>();
        if(sys_orgunit.getOulevel()==1) {
            List<SysOrgUnit> list = sysOrgUnitService.getSysOrgunitList(map);
            if (list.size() > 0) {
                Iterator<SysOrgUnit> iter_orgunit = list.iterator();
                while (iter_orgunit.hasNext()) {
                    SysOrgUnit obj = iter_orgunit.next();
                    if (obj.getParentoucode().equals("0")) {
                        TreeNode tree = new TreeNode();
                        tree.setId(obj.getOucode());
                        tree.setPid(obj.getParentoucode());
                        tree.setText(obj.getOuname());
                        tree.setChildren(new ArrayList<TreeNode>());
                        treeRootNodes.add(tree);
                        iter_orgunit.remove();
                    }
                }
                if (treeRootNodes.size() > 0) {
                    Iterator<TreeNode> iter_treeroot = treeRootNodes.iterator();
                    while (iter_treeroot.hasNext()) {
                        TreeNode treeNode = iter_treeroot.next();
                        iter_orgunit = list.iterator();
                        while (iter_orgunit.hasNext()) {
                            SysOrgUnit obj = iter_orgunit.next();
                            if (obj.getParentoucode().equals(treeNode.getId())) {
                                TreeNode newTreeNode = new TreeNode();
                                newTreeNode.setId(obj.getOucode());
                                newTreeNode.setPid(obj.getParentoucode());
                                newTreeNode.setText(obj.getOuname());
                                treeNode.getChildren().add(newTreeNode);
                            }
                        }
                    }

                }
            }
        }else {
            TreeNode newTreeNode = new TreeNode();
            newTreeNode.setId(sys_orgunit.getOucode());
            newTreeNode.setPid(sys_orgunit.getParentoucode());
            newTreeNode.setText(sys_orgunit.getOuname());
            treeRootNodes.add((newTreeNode));
        }
        return treeRootNodes;
    }


    @ResponseBody()
    @RequestMapping("getOrgTreeList")
    public List<TreeNode> getOrgTreeList(){

        String oucode = getUserOuCode();
        TreeNode treeNode = new TreeNode();
        List<TreeNode> tNodeList = new ArrayList<TreeNode>();
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        SysOrgUnit sys_orgunit = sysOrgUnitService.getSysOrgunitByOuCode(oucode);
        if(sys_orgunit != null ){
            treeNode.setId(sys_orgunit.getOucode());
            treeNode.setPid(sys_orgunit.getParentoucode());
            treeNode.setText(sys_orgunit.getOuname());
            treeNode.setLevel(Integer.valueOf(sys_orgunit.getOulevel().toString()));
            tNodeList = getTreeNodeList(tNodeList,oucode);
            tNodeList.add(treeNode);

            treeNodes = BuildTreeUtil.buildtree(tNodeList, sys_orgunit.getParentoucode(), "");
        }
        return treeNodes;
    }

    @ResponseBody()
    @RequestMapping("getOrgunitByOucode")
    public SysOrgUnit getOrgunitByOucode(){
        String oucode = request.getParameter("oucode");
        if(StringUtils.isBlank(oucode)){
            oucode=getUserOuCode();
        }
        return sysOrgUnitService.getSysOrgunitByOuCode(oucode);
    }

    public  List<TreeNode> getTreeNodeList(List<TreeNode> treeNodeList,String pcode){
        List<TreeNode> tNodeList = new ArrayList<TreeNode>();
        if("0".equals(pcode)){
            return null;
        }else{
            tNodeList = sysOrgUnitService.getTreeNodesByPCode(pcode);
            for(TreeNode tNode:tNodeList){
                getTreeNodeList(treeNodeList,tNode.getId());
                treeNodeList.add(tNode);
            }
            return treeNodeList;
        }


    }

    /**
     * 设置商品价格消息同步方式
     * @param synctype
     * @param fromtime
     * @param totime
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "setAutoSync",method = RequestMethod.POST)
    public ResultMsg setAutoSync(Integer synctype,Double fromtime,Double totime){

        ResultMsg rmsg = new ResultMsg();
        SysOrgUnit sys_orgunit =  new SysOrgUnit();
        sys_orgunit.setOucode(getUserOuCode());
        sys_orgunit.setSyncpricestate(synctype);
        if(synctype.equals(1)){
            sys_orgunit.setSyncfromtime(fromtime);
            sys_orgunit.setSynctotime(totime);
        }
        int upd = sysOrgUnitService.updateByPrimaryKeySelective(sys_orgunit);
        if(upd>0){
            rmsg.setResult(true);
            rmsg.setMsg("设置成功");
        }else {
            rmsg.setResult(false);
            rmsg.setMsg("设置失败");
        }
        return rmsg;
    }
}
