package com.kld.shop.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.Global;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.dto.TreeNode;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.common.util.BuildTreeUtil;
import com.kld.shop.controller.base.BaseController;
import com.kld.sys.api.ISysFuncService;
import com.kld.sys.api.ISysRoleFuncRelService;
import com.kld.sys.api.ISysRoleService;
import com.kld.sys.api.ISysUserService;
import com.kld.sys.po.SysFunc;
import com.kld.sys.po.SysRole;
import com.kld.sys.po.SysRoleFuncRel;
import com.kld.sys.po.SysUser;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by jw on 2015/7/30.
 */
@Controller
@RequestMapping("/admin/sysRole")
public class SysRoleController extends BaseController {
    @Resource
    private ISysRoleService sysRoleService;
    @Resource
    private ISysFuncService sysFuncService;
    @Resource
    private ISysRoleFuncRelService sysRoleFuncRelService;
    @Resource
    private ISysUserService sysUserService;

    @RequestMapping("roleList")
    public ModelAndView list (){
        ArrayList<String> list = GetUserFuncList();
        return new ModelAndView("admin/role/roleList","list",list);
    }

    /**
     * 角色列表
     * @return
     */
    @ResponseBody()
    @RequestMapping("/getRoleList")
    public ResultMsg getRoleList (){
        Map<String,Object> map = new HashMap<String, Object>();
        return  sysRoleService.getSysRoleFunList(map,getPageNum(),getPageSize());

    }

    @ResponseBody()
    @RequestMapping("getRolesByOUCode")
    public List<SysRole> getRolesByOUCode(){
        Map<String,Object> map = new HashMap<String, Object>();
        List<SysRole> roleList = sysRoleService.getSysRoleFunList(map,1,Integer.MAX_VALUE).getRows();
        return roleList;
    }

    /**
     * 新增角色
     * @param funcids 菜单ids
     * @param sys_role 角色
     * @return ResultMsg
     * @throws Exception
     */
    @ResponseBody()
    @RequestMapping(value = "save/{funcids}" ,method = RequestMethod.POST)
    public ResultMsg save(@PathVariable("funcids") String funcids,SysRole sys_role)throws Exception {
        ResultMsg rmsg = new ResultMsg();
        sys_role.setRolefunccodes(funcids);
        try {
           rmsg = sysRoleService.insert(sys_role);
        }catch (Exception ex){
            rmsg.setResult(false);
            rmsg.setMsg(ex.getMessage());
        }
        return rmsg;
    }

    /**
     * 修改角色
     * @param funcids 菜单ids
     * @param sys_role 角色
     * @return ResultMsg
     * @throws Exception
     */
    @ResponseBody()
    @RequestMapping(value = "update/{funcids}" ,method = RequestMethod.POST)
    public ResultMsg update(@PathVariable("funcids") String funcids,SysRole sys_role)throws Exception {
        ResultMsg rmsg = new ResultMsg();
        try {
            rmsg = sysRoleService.updateSysRoleByCode(sys_role);
        }catch (Exception e) {
            logger.info("更新角色失败",e);
            rmsg.setResult(false);
            rmsg.setMsg("操作失败！" + e.getMessage());
    }
        return rmsg;
    }

    /**
     * 删除角色
     * @return ResultMsg
     */
    @ResponseBody()
    @RequestMapping(value = "delete" ,method = RequestMethod.POST)
    public ResultMsg delete(){
        ResultMsg rmsg = new ResultMsg();
        String rolecode = request.getParameter("rolecode");
        try {
           rmsg = sysRoleService.delete(rolecode);
        }catch (Exception e) {
            logger.info("删除系统角色失败", e);
            rmsg.setResult(false);
            rmsg.setMsg("操作失败！" + e.getMessage());
        }
        return rmsg;
    }

    @ResponseBody()
    @RequestMapping(value = "getFuncTree" ,method = RequestMethod.GET)
    public List<TreeNode> getFuncTree()throws Exception {
        try {
            List<TreeNode> nodes=new ArrayList<TreeNode>();
            Map<String,Object> map = new HashMap<String, Object>();
            List<SysFunc> funclist = sysFuncService.getFuncList(map);
            JSONArray array = new JSONArray();
            for(SysFunc sys_func:funclist){
                TreeNode    treeNode = new TreeNode();
                treeNode.setId(sys_func.getFunccode());
                treeNode.setPid(sys_func.getParentcode());
                treeNode.setText((String) sys_func.getName());
                nodes.add(treeNode);
            }
            List<TreeNode> treeNodes = BuildTreeUtil.buildtree(nodes, Global.FUN_TOP_PID, "");
            return treeNodes;
        }catch (Exception e) {
            logger.info("获取功能树",e);
            throw e;
        }
    }

}
