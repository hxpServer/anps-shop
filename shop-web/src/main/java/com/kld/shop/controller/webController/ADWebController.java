package com.kld.shop.controller.webController;

import com.kld.cms.api.ISysAdService;
import com.kld.cms.po.SysAd;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.shop.controller.base.BaseWebController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created by An Wentong on 2015/11/5.
 */
@Controller
@RequestMapping("ad")
public class ADWebController extends BaseWebController {

    @Resource
    private ISysAdService iSysAdService;

    /**
     * 获取广告
     * @return
     */
    @ResponseBody
    @RequestMapping("/getAdWebList")
    public ResultMsg getAdWebList(){
        ResultMsg msg = new ResultMsg();
        List<SysAd> list = iSysAdService.getAdWebList(new HashMap<String, Object>());
        msg.setResult(true);
        msg.setData(list);
        return msg;
    }

}
