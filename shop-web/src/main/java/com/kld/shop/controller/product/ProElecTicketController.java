package com.kld.shop.controller.product;


import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.product.api.IProElecTicketService;
import com.kld.product.api.IProMctService;
import com.kld.product.po.ProBrand;
import com.kld.product.po.ProElecTicket;
import com.kld.product.po.ProMct;
import com.kld.shop.allocation.Constant;
import com.kld.shop.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by anpushang on 2016/3/28.
 */
@Controller
@RequestMapping("/admin/ticket")
public class ProElecTicketController extends BaseController {

    @Autowired
    private IProElecTicketService proElecTicketService;
    @Autowired
    private IProMctService proMctService;


    /***
     * 进入列表页面
     * @return
     */
    @RequestMapping(value = "/toList")
    public String toList(){
        return "/admin/elecTicket/ticket_list";
    }


    /***
     * 电子券列表查询
     * @param proElecTicket
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/list")
    public ResultMsg list(ProElecTicket proElecTicket){
        PageInfo pageInfo = null;
        try{
            pageInfo = proElecTicketService.findProElecTicketPageList(getPageNum(), getPageSize(), proElecTicket);
        }catch (Exception e){
            logger.error("电子券查询"+e.getMessage(),e);
            e.printStackTrace();
        }
        return PageInfoResult.PageInfoMsg(pageInfo);
    }


    /***
     * 电子券新增
     * @param proElecTicket
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/saveTicket")
    public ResultMsg saveBrand(ProElecTicket proElecTicket){
        ResultMsg resultMsg = new ResultMsg();
        try{
            //TODO 临时写这样子

            proElecTicket.setOuCode(getUserOuCode());
            proElecTicket.setIsDel(Constant.ENABLE);
            proElecTicket.setCreator(getUserName());

            if (null != proElecTicket.getId()){
                int resultCount = proElecTicketService.updateByPrimaryKey(proElecTicket);
                resultMsg = resultCount > 0 ? successMsg(resultMsg,"修改成功") : errorMsg(resultMsg,"修改失败");
            }else{
                proElecTicket.setCode(String.valueOf(proElecTicket.getMctId()));
                int resultCount = proElecTicketService.insert(proElecTicket);
                resultMsg = resultCount > 0 ? successMsg(resultMsg,"新增成功") : errorMsg(resultMsg,"新增失败");
            }
        }catch (Exception e){
            logger.error("电子券新增"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 电子券修改
     * @param proElecTicket
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/upTicket")
    public ResultMsg upTicket(ProElecTicket proElecTicket){
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proElecTicketService.updateByPrimaryKey(proElecTicket);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"修改成功") : errorMsg(resultMsg,"修改失败");
        }catch (Exception e){
            logger.error("电子券修改"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }


    /***
     * 电子券删除 逻辑删除
     * @param id
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/delTicket")
    public ResultMsg delBrandById(Integer id){
        ProElecTicket proElecTicket = new ProElecTicket();
        proElecTicket.setIsDel(Constant.DISABLE);
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proElecTicketService.updateByPrimaryKey(proElecTicket);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"删除成功") : errorMsg(resultMsg,"删除失败");
        }catch (Exception e){
            logger.error("电子券删除"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }


    /***
     * 供应商查询
     * @param proMct
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/findMctList")
    public ResultMsg findMctList(ProMct proMct){
        ResultMsg resultMsg = new ResultMsg();
        try{
            List<ProMct> proMctList = proMctService.findProMctList(proMct);
            resultMsg.setData(proMctList);
        }catch (Exception e){
            logger.error("电子券查询根据"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }



}
