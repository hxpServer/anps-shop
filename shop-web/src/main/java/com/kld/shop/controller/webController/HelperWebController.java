package com.kld.shop.controller.webController;

import com.kld.shop.controller.base.BaseWebController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hasee on 2015/11/9.
 */
@Controller
@RequestMapping("help")
public class HelperWebController extends BaseWebController {
    /**
     * 兑换流程
     */
    @RequestMapping("/exchangeFlow")
    public String exchangeFlow(){
        return "/help-center/exchange-flow";
    }
    /**
     * 积分计划
     */
    @RequestMapping("/jifenPlan")
    public String jifenPlan(){
        return "/help-center/jifen-plan";
    }
    /**
     * 快递配送
     */
    @RequestMapping("/delivery")
    public String delivery(){
        return "/help-center/delivery";
    }
    /**
     * 售后服务
     */
    @RequestMapping("/afterSale")
    public String afterSale(){
        return "/help-center/after-sale";
    }
}
