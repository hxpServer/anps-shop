package com.kld.shop.allocation;

/**
 * Created by anpushang on 2016/3/15.
 */
public class Constant {


    //权限拦截使用,不被拦截的url配置
    public static final String NO_INTERCEPTOR_PATH = ".*/((login)|(logout)|(code)).*";	//不对匹配该值的访问路径拦截（正则）
    //session key
    public static final String SESSION_USER = "SESSION_USER";
    //登陆地址
    public static final String LOGIN = "/login_toLogin.do";
    //图片服务器地址 1
    public static final String IMAGE_URL01 = "";
    //图片服务器地址 2
    public static final String IMAGE_URL02 = "";
    //图片服务器地址 3
    public static final String IMAGE_URL03 = "";


    public static final Integer ENABLE = 1;

    public static final Integer DISABLE = 0;

    public static final String INSERT_SUCCESS = "添加成功";

    public static final String INSERT_ERROR = "添加失败";

    public static final String UPDATE_SUCCESS = "修改成功";

    public static final String UPDATE_ERROR = "修改失败";

    public static final String DELETE_SUCCESS = "删除成功";

    public static final String DELETE_ERROR = "删除失败";

    public static final String PRODUCT_TYPE = "2125";

    public static final String PRODUCT_MCT = "2130";

    public static final String PRODUCT_STOCK_MANAGER = "2133";

    public static final String PRODUCT_WEIGHT_UNIT = "2142";

    public static final String PRODUCT_WX_INDATE = "2145";

    public static final String PRODUCT_SER = "2136";

    public static final String PRODUCT_PS = "2139";
}
