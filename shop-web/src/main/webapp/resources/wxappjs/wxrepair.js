function applayRepair(num,pickwareType,description,province,city,country,town,address,recieveaddr,sprovince,scity,scountry,stown,saddress,addr,customerContactName,customerTel,customerExpect){
    /*returnOrder*/
    var orderid=$$('#orderid').text();
    var productcode=$$('#productcode').data("productcode")===undefined?"":$$('#productcode').data("productcode");
    var suborderid=$$('#suborderid').data("suborderid")===undefined?"":$$('#suborderid').data("suborderid");
    var thirdorderid=$$('#thirdorderid').data("thirdorderid")===undefined?"":$$('#thirdorderid').data("thirdorderid");
    var thirdcode=$$('#thirdcode').data("thirdcode")===undefined?"":$$('#thirdcode').data("thirdcode");

    var picimgurl="";//图片的上传地址，可返回多个图片的地址
    /*asSafeDto*/
    var jdOrderId=thirdorderid;

    var questionDesc=description;
    var isNeedDetectionReport=$$("#isNeedDetectionReport").data("isNeedDetectionReport")===undefined?"":$$("#isNeedDetectionReport").data("isNeedDetectionReport");//是否需要检测报告
    var questionPic=picimgurl;
    var isHasPackage=$$("#isHasPackage").data("isHasPackage")===undefined?"":$$("#isHasPackage").data("isHasPackage");//是否有包装
    var packageDesc=$$("#packageDesc").data("packageDesc")===undefined?"":$$("#packageDesc").data("packageDesc");//包装描述
    /*asCustomerDto*/

    var customerEmail=$$("#customerEmail").data("customerEmail")===undefined?"":$$("#customerEmail").data("customerEmail");
    var customerPostcode=$$("#customerPostcode").data("customerPostcode")===undefined?"":$$("#customerPostcode").data("customerPostcode");
    var customerContactName=$$("#realname").val();
    var customerMobilePhone=$$("#phone").val();
    /*asPickwareDto*/

    var pickwareProvince=$$("#provinceId").val();
    var pickwareCity=$$("#cityId").val();
    var pickwareCounty=$$("#countyId").val();
    var pickwareVillage=$$("#townId").val();
    var pickwareAddress=$$("#saddress").val();
    /*asReturnwareDto*/
    var returnwareType=$$("#jdCompSList").find(".current-btn").data("code");
    var returnwareProvince=$$("#two-provinceId").val();
    var returnwareCity=$$("#two-cityId").val();
    var returnwareCounty=$$("#two-countyId").val();
    var returnwareVillage=$$("#two-townId").val();
    var returnwareAddress=$$("#address").val();
    /*asDetailDto*/
    var skuId=thirdcode;
    var skuNum=num;


    var returnorder = new Object();
    var asSafeDto = new Object();
    var asCustomerDto = new Object();
    var asPickwareDto = new Object();
    var asReturnwareDto = new Object();
    var asDetailDto = new Object();
    var returnOrderStr='{"returnOrder": {"orderid":"'+orderid+'","productcode":"'+productcode+'","suborderid":"'+suborderid+'","thirdorderid":"'+thirdorderid+'","recieveaddr":"'+recieveaddr+'","addr":"'+addr+'","num":"'+num+'","description":"'+description+'","picimgurl":"'+picimgurl +'"},' +
        '"asSafeDto": {"jdOrderId":"'+jdOrderId+'","questionPic":"'+questionPic+'","customerExpect":"'+customerExpect+'","questionDesc":"'+questionDesc+'"},' +
        '"asCustomerDto":{"customerContactName":"'+customerContactName+'","customerTel":"'+customerTel+'"},' +
        '"asPickwareDto":{"pickwareType":'+pickwareType+',' +  '"pickwareProvince":'+pickwareProvince+',"pickwareCity":'+pickwareCity+',"pickwareCounty":"'+pickwareCounty+'","pickwareVillage":"'+pickwareVillage+'","pickwareAddress":"'+pickwareAddress+'"},' +
        '"asReturnwareDto":{"returnwareType":"'+returnwareType+'","returnwareProvince":"'+returnwareProvince+'",' + '"returnwareCity":"'+returnwareCity+'","returnwareCounty":"'+returnwareCounty+'","returnwareVillage":"'+returnwareVillage+'","returnwareAddress":"'+returnwareAddress+'"},' +
        '"asDetailDto":{"skuId":'+skuId+',"skuNum":"'+skuNum+'"}' +
        '}';

    var robj = new Object();
    robj.returnOrder=returnorder;

    $$("#wait-circle").show();
    $$.ajax({
        url: "/api/order/saveAfsApply",
        type: "post",
        dataType: "json",
        data: {"returnOrderStr": returnOrderStr},
        success: function (res) {
            if (res.result) {
                $$("#wait-circle").hide();
                alert("申请已成功提交。");
                window.location.href="/order/orderlist";
            }else{
                $$("#wait-circle").hide();
                alert(res.msg);
            }
        }
    });
}
