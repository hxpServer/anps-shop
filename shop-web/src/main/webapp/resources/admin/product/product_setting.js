(function () {
    var levelOne = $("#levelOne").combobox({
        url: '/admin/cate/getCateByPid?pId=0',
        valueField: 'id',
        textField: 'text',
        onLoadSuccess: function () { //选择第一个
            var data = $('#levelOne').combobox('getData');
            if (data.length > 0) {
                $('#levelOne').combobox('select', data[0].id);
            }
        }, onSelect: function (node) {
            $("#levelOneName").val(node.text)
            var levelTwo = $("#levelTwo").combobox({
                url: '/admin/cate/getCateByPid?pId=' + node.id,
                valueField: 'id',
                textField: 'text',
                onLoadSuccess: function () { //选择第一个
                    var data = $('#levelTwo').combobox('getData');
                    if (data.length > 0) {
                        $('#levelTwo').combobox('select', data[0].id);
                    }
                }, onSelect: function (node) {
                    $("#levelTwoName").val(node.text)
                    var levelThree = $("#levelThree").combobox({
                        url: '/admin/cate/getCateByPid?pId=' + node.id,
                        valueField: 'id',
                        textField: 'text',
                        onLoadSuccess: function () { //选择第一个
                            var data = $('#levelThree').combobox('getData');
                            if (data.length > 0) {
                                $('#levelThree').combobox('select', data[0].id);
                            }
                        }, onSelect: function (node) {
                            $("#levelThreeName").val(node.text)
                            /*alert("得到的是三级分类"+node.text);//得到三级商品分类*/
                            var cateId = node.id;
                            $("#cateId").val(node.id);

                            var brandId = $("#brandId").combobox({
                                url: '/admin/brand/getBrandByCateId?cateId=' + node.id,
                                valueField: 'id',
                                textField: 'text',
                                onLoadSuccess: function () { //选择第一个
                                    var data = $('#brandId').combobox('getData');
                                    if (data.length > 0) {
                                        $('#brandId').combobox('select', data[0].id);
                                    }
                                }, onSelect: function (node) {
                                    /*alert("得到的是三级分类"+node.text);//得到三级商品分类*/

                                }
                            });

                            selectProp(cateId, 0);
                        }
                    });
                }
            });
        }
    });

    /***
     * 查询分类下的属性
     * @param cateId
     */
    function selectProp(cateId, isSpec) {
        $.ajax({
            type: "post",
            url: "/admin/prop/selectProp",
            data: {"cateId": cateId, "isSpec": isSpec},
            dataType: 'json',
            success: function (data) {
                $(".proClass").remove();
                console.log(data);
                var html = "";
                $.each(data, function (i, item) {
                    var prop = item.proProp;
                    html += '<tr class="proClass"><td>' + prop.propName + ':&nbsp;&nbsp; <input type="hidden" name="attrName" value="' + prop.propName + '"/>';
                    $.each(item.proPropValueList, function (j, proValue) {
                        html += '<input name="'+proValue.propId+'" id="'+proValue.proValueId+'" type="checkbox" value="' + proValue.propValue + '"/><span>' + proValue.propValue + '</span>&nbsp;&nbsp;';
                    });
                    html += '</td></tr>';
                });
                console.log(html);
                $("#productSettingTable").append(html);
            }
        });
    }

    /*// 一层Combo
     var srmCombx = $("#levelOne").combobox({
     loader: function (param, success, error) {
     $.ajax({
     url: '/admin/cate/getCateByPid?pId=0',
     dataType: 'json',
     success: function (data) {
     data.unshift({id: '', text: '全部'});
     success(data);
     },
     error: function () {
     error.apply(this, arguments);
     }
     });
     },
     onSelect: function (record) {  //onSelect 用户点击时触发的事件  在此的意义在于，用户点击一级后自动二级combobox
     piperowCombx.combobox({
     loader: function (param, success, error) {
     $.ajax({
     url: '/admin/cate/getCateByPid?pId=' + $("#levelOne").combobox("getValue") + '',
     dataType: 'json',
     success: function (data) {
     data.unshift({id: '', text: '全部'});
     success(data);
     },
     error: function () {
     error.apply(this, arguments);
     }
     });
     },
     onSelect: function (record) { //这里也使用了onSelect事件，在二级combobox被用户点击时触发三级combobox
     pipeCombx.combobox({
     loader: function (param, success, error) {
     $.ajax({
     url: '/admin/cate/getCateByPid?pId=' + $("#levelTwo").combobox("getValue") + '',
     dataType: 'json',
     success: function (data) {
     data.unshift({id: '', text: '全部'});
     success(data);
     },
     error: function () {
     error.apply(this, arguments);
     }
     });
     },
     valueField: 'id',
     textField: 'text',
     value: '',
     editable: false
     });
     },
     onLoadSuccess: function () {  //清空三级下拉框 就是成功加载完触发的事件 当一级combobox改变时，二级和三级就需要清空
     pipeCombx.combobox("clear");
     pipeCombx.combobox('setValue', '全部'); //给combobox下拉框设置一个值，否则为空不好看
     },
     valueField: 'id',
     textField: 'text',
     value: '',
     editable: false
     }).combobox("clear"); //清空二级下拉框

     },
     valueField: 'id',
     textField: 'text',
     value: '',
     editable: false
     });
     */
    /******************************************************************************************************/
    /*
     //下面的俩个是组件，

     //  二层Combo
     var piperowCombx = $("#levelTwo").combobox({
     loader: function (param, success, error) {
     $.ajax({
     url: '/admin/cate/getCateByPid?pId=' + $("#levelOne").combobox("getValue") + '',
     dataType: 'json',
     success: function (data) {
     data.unshift({id: '', text: '全部'});
     success(data);
     },
     error: function () {
     error.apply(this, arguments);
     }
     });
     },
     valueField: 'id',
     textField: 'text',
     value: '',
     editable: false
     });

     //三层Combo
     var pipeCombx = $("#levelThree").combobox({
     loader: function (param, success, error) {
     $.ajax({
     url: '/admin/cate/getCateByPid?pId=' + $("#levelTwo").combobox("getValue") + '',
     dataType: 'json',
     success: function (data) {
     data.unshift({id: '', text: '全部'});
     success(data);
     },
     error: function () {
     error.apply(this, arguments);
     }
     });
     },
     valueField: 'id',
     textField: 'text',
     value: '',
     editable: false
     });
     */

})();