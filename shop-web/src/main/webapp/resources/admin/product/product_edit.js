(function () {

    $("#proType").combobox({
        url:'/admin/pro/getProductConstent?par=2125',
        valueField:'id',
        textField:'text'
    });

    $("#stockManage").combobox({
        url:'/admin/pro/getProductConstent?par=2133',
        valueField:'id',
        textField:'text'
    });


    $("#unit").combobox({
        url:'/admin/pro/getProductConstent?par=2142',
        valueField:'id',
        textField:'text'
    });

    $("#mainTain").combobox({
        url:'/admin/pro/getProductConstent?par=2145',
        valueField:'id',
        textField:'text'
    });

    var imageUrl = $("#imageArray").val();
    inituploader("", "00", imageUrl);

})();

$("#kdys").on("click",function(){
    $("#processTime").attr("disabled",true);
});

$("#smzi").on("click",function(){
    $("#processTime").attr("disabled",false);
});




var pxDetailEditor;
KindEditor.ready(function(K) {
    pxDetailEditor = K.create('textarea[name="pxDetail"]', {
        resizeType : 1,
        /* allowPreviewEmoticons : false,
         allowImageUpload : false,*/
        allowImageRemote:false,
        imageUploadJson:"/admin/home/uploadFile",
        allowFileManager : true,
        items : [
            'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
            'insertunorderedlist', '|', 'emoticons', 'image', 'link']
    });
});

var wxDetailEditor;
KindEditor.ready(function(K) {
    wxDetailEditor = K.create('textarea[name="wxDetail"]', {
        resizeType : 1,
        /* allowPreviewEmoticons : false,
         allowImageUpload : false,*/
        allowImageRemote:false,
        imageUploadJson:"/admin/home/uploadFile",
        allowFileManager : true,
        items : [
            'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
            'insertunorderedlist', '|', 'emoticons', 'image', 'link']
    });
});

function btnProUpdate(){
    $("#pxDetail").val(pxDetailEditor.html());
    $("#wxDetail").val(wxDetailEditor.html());
    $.ajax({
        type: "post",
        url: "/admin/pro/updatePro",
        data: $('#fmUpdateProductForm').serialize(),
        dataType: 'json',
        success: function (data) {
            if(data.result){
                $.messager.alert("提示",data.msg);
                window.location.href = "/admin/pro/toList";
            }else{
                $.messager.alert("提示","操作失败,请稍后重试");
            }
        }
    });

}




