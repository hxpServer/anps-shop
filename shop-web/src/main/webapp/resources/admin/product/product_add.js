(function () {

    $("#proType").combobox({
        url:'/admin/pro/getProductConstent?par=2125',
        valueField:'id',
        textField:'text',
        onSelect : function(record){
            if(record.id == 2126){
                $("#productSers").hide();
                $("#jifenId").show();
                $(".isxnProductClass").textbox('disable');
            }else{
                $("#productSers").show();
                $("#jifenId").hide();
                $(".isxnProductClass").textbox('enable');
            }
        },
        onLoadSuccess: function () {
            var data = $('#proType').combobox('getData');
            if (data.length > 0) {
                $('#proType').combobox('select', data[0].id);
            }
        }
    });

    $("#stockManage").combobox({
        url:'/admin/pro/getProductConstent?par=2133',
        valueField:'id',
        textField:'text',
        onLoadSuccess: function () {
            var data = $('#stockManage').combobox('getData');
            if (data.length > 0) {
                $('#stockManage').combobox('select', data[0].id);
            }
        }
    });

    /*$("#mctCode").combobox({
        url:'/admin/pro/getProductConstent?par=2130',
        valueField:'id',
        textField:'text',
        onLoadSuccess: function () {
            var data = $('#mctCode').combobox('getData');
            if (data.length > 0) {
                $('#mctCode').combobox('select', data[0].id);
            }
        }
    });*/

    $("#unit").combobox({
        url:'/admin/pro/getProductConstent?par=2142',
        valueField:'id',
        textField:'text',
        onLoadSuccess: function () {
            var data = $('#unit').combobox('getData');
            if (data.length > 0) {
                $('#unit').combobox('select', data[0].id);
            }
        }
    });

    $("#mainTain").combobox({
        url:'/admin/pro/getProductConstent?par=2145',
        valueField:'id',
        textField:'text',
        onLoadSuccess: function () {
            var data = $('#mainTain').combobox('getData');
            if (data.length > 0) {
                $('#mainTain').combobox('select', data[0].id);
            }
        }
    });

    inituploader("","00",[]);

})();

$("#kdys").on("click",function(){
    $("#processTime").attr("disabled",true);
});

$("#smzi").on("click",function(){
    $("#processTime").attr("disabled",false);
});




var pxDetailEditor;
KindEditor.ready(function(K) {
    pxDetailEditor = K.create('textarea[name="pxDetail"]', {
        resizeType : 1,
        /* allowPreviewEmoticons : false,
         allowImageUpload : false,*/
        allowImageRemote:false,
        imageUploadJson:"/admin/home/uploadFile",
        allowFileManager : true,
        items : [
            'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
            'insertunorderedlist', '|', 'emoticons', 'image', 'link']
    });
});

var wxDetailEditor;
KindEditor.ready(function(K) {
    wxDetailEditor = K.create('textarea[name="wxDetail"]', {
        resizeType : 1,
        /* allowPreviewEmoticons : false,
         allowImageUpload : false,*/
        allowImageRemote:false,
        imageUploadJson:"/admin/home/uploadFile",
        allowFileManager : true,
        items : [
            'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
            'insertunorderedlist', '|', 'emoticons', 'image', 'link']
    });
});

/***
 *
 */
function btnProSave(){
    $("#pxDetail").val(pxDetailEditor.html());
    $("#wxDetail").val(wxDetailEditor.html());
    $.ajax({
        type: "post",
        url: "/admin/pro/savePro",
        data: $('#fmInsertProductForm').serialize(),
        dataType: 'json',
        success: function (data) {
            if(data.result){
                $.messager.alert("提示",data.msg);
                window.location.href = "/admin/pro/toList";
            }else{
                $.messager.alert("提示",data.errorMsg);
            }
        }
    });

}



