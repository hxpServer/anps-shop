package com.kld.common.framework.dto;

/**
 * Created by Administrator on 2015/8/16.
 */
public class CardExpensePointDTO {

    public long cardAsn; //卡积分
    public String orgCode; //交易受理机构
    public long amount; //交易额
    public String uiqueId;//消费唯一标识   (参考)订单时间+订单号+交易ID拼接的方式   13
    public String DetailAmounts;//子交易明细(组织机构1：消费积分；组织机构2：消费积分)
    public String MAC;//接口验证码
    public int tradeType; //交易类型  10积分商城积分消费 11积分商城积分换出；12 积分商城积分赠送

    public long getCardAsn() {
        return cardAsn;
    }

    public void setCardAsn(long cardAsn) {
        this.cardAsn = cardAsn;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getUiqueId() {
        return uiqueId;
    }

    public void setUiqueId(String uiqueId) {
        this.uiqueId = uiqueId;
    }

    public String getDetailAmounts() {
        return DetailAmounts;
    }

    public void setDetailAmounts(String detailAmounts) {
        DetailAmounts = detailAmounts;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public int getTradeType() {
        return tradeType;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    @Override
    public String toString() {
        return "CardExpensePointDTO{" +
                "cardAsn=" + cardAsn +
                ", orgCode='" + orgCode + '\'' +
                ", amount=" + amount +
                ", uiqueId='" + uiqueId + '\'' +
                ", DetailAmounts='" + DetailAmounts + '\'' +
                ", MAC='" + MAC + '\'' +
                ", tradeType=" + tradeType +
                '}';
    }
}
