package com.kld.common.framework.dto;

import java.util.List;

/**
 * hejinping 2016.3.23
 */
public class TreeNode {
    private String id;
    private String text;
    private String pid;
    private String state;
    private Integer level;
    private Integer haschild;
    private Integer oulevel;//机构level为关键字 查询储存此字段
    private List<TreeNode> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public Integer getHaschild() {
        return haschild;
    }

    public void setHaschild(Integer haschild) {
        this.haschild = haschild;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getOulevel() {
        return oulevel;
    }

    public void setOulevel(Integer oulevel) {
        this.oulevel = oulevel;
    }
}
