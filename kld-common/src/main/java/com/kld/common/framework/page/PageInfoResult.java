package com.kld.common.framework.page;

import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;

import java.util.ArrayList;

/**
 * Created by Dan on 2015/7/30.
 */
public class PageInfoResult {

    public static ResultMsg PageInfoMsg(PageInfo pageInfo){
        ResultMsg rmsg = new ResultMsg();
        rmsg.setResult(true);
        if(pageInfo.getTotal()==0 || pageInfo.getList()==null || pageInfo.getList().size()==0){
            rmsg.setRows(new ArrayList<Object>());
        }else {
            rmsg.setRows(pageInfo.getList());
        }
        rmsg.setTotal(pageInfo.getTotal());
        return rmsg;
    }
}
