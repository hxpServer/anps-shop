package com.kld.common.framework.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2015/8/20.
 */
public class CategoryTree {

    private String code;
    private String categoryname;
    private String picimg;
    private String memo;
    private Integer categorylevel;
    private String state;
    private String parentcode;
    private Integer haschild;
    private BigDecimal recommend;
    private BigDecimal categorystate;   //类别上下架状态
    private List<CategoryTree> children;

    public BigDecimal getCategorystate() {
        return categorystate;
    }

    public void setCategorystate(BigDecimal categorystate) {
        this.categorystate = categorystate;
    }

    public BigDecimal getRecommend() {
        return recommend;
    }

    public void setRecommend(BigDecimal recommend) {
        this.recommend = recommend;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getPicimg() {
        return picimg;
    }

    public void setPicimg(String picimg) {
        this.picimg = picimg;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getParentcode() {
        return parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public Integer getCategorylevel() {
        return categorylevel;
    }

    public void setCategorylevel(Integer categorylevel) {
        this.categorylevel = categorylevel;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getHaschild() {
        return haschild;
    }

    public void setHaschild(Integer haschild) {
        this.haschild = haschild;
    }

    public List<CategoryTree> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTree> children) {
        this.children = children;
    }
}
