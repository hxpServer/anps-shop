/**
 * 58.com Inc.
 * Copyright (c) 2005-2015 All Rights Reserved.
 */
package com.kld.common.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author yangjian
 * @version $Id: PropertiesUtil.java, v 0.1 2015-8-7 下午2:33:20 yangjian Exp $
 */
public class PropertiesUtil {

    public static Properties props = null;
    static{

        Resource moderes = new ClassPathResource("/config/system.properties");
        try {
            Properties modeprop = PropertiesLoaderUtils.loadProperties(moderes);
            String filename="/config/prop.properties";
            if("true".equals(modeprop.get("debug"))){
                filename="/config/debug.prop.properties";
            }
            Resource resource = new ClassPathResource(filename);
            props = PropertiesLoaderUtils.loadProperties(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}
