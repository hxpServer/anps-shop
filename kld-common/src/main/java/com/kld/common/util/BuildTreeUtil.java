package com.kld.common.util;
import com.kld.common.framework.dto.CategoryTree;
import com.kld.common.framework.dto.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jw on 2015/8/3.
 */
public class BuildTreeUtil {
    public static List<TreeNode> buildtree(List<TreeNode> nodes,String id,String object)
    {
        List<TreeNode> treeNodes=new ArrayList<TreeNode>();
        for (TreeNode treeNode : nodes) {
            TreeNode node=new TreeNode();
            node.setId(treeNode.getId());
            node.setLevel(treeNode.getLevel());
            node.setPid(treeNode.getPid());
            node.setText(treeNode.getText());
            if(id.equals(treeNode.getPid())){
                if("category".equals(object)){
                    if(treeNode.getLevel()!=2 && treeNode.getHaschild()>0){
                        node.setChildren(buildtree(nodes, node.getId(), object));
                        node.setState("closed");
                    }
                }else if("product".equals(object)){
                    if(treeNode.getLevel() != 3 && treeNode.getHaschild()>0) {
                        node.setChildren(buildtree(nodes, node.getId(), object));
                        node.setState("closed");
                    }
                }
                else{
                    node.setChildren(buildtree(nodes, node.getId(), object));
                }
                treeNodes.add(node);
            }
        }
        return treeNodes;
    }

    public static List<CategoryTree> buildcategorytree(List<CategoryTree> categoryTreeList,String parentcode)
    {
        List<CategoryTree> categoryTrees=new ArrayList<CategoryTree>();
        for (CategoryTree categoryTree : categoryTreeList) {
            CategoryTree categoryTreeNode=new CategoryTree();
            categoryTreeNode.setCode(categoryTree.getCode());
            categoryTreeNode.setCategoryname(categoryTree.getCategoryname());
            categoryTreeNode.setPicimg(categoryTree.getPicimg());
            categoryTreeNode.setMemo(categoryTree.getMemo());
            categoryTreeNode.setCategorylevel(categoryTree.getCategorylevel());
            categoryTreeNode.setParentcode(categoryTree.getParentcode());
            categoryTreeNode.setHaschild(categoryTree.getHaschild());
            categoryTreeNode.setRecommend(categoryTree.getRecommend());
            categoryTreeNode.setCategorystate(categoryTree.getCategorystate());
            if(parentcode.equals(categoryTree.getParentcode())){
                if(categoryTreeNode.getHaschild()!=0){
                    categoryTreeNode.setChildren(buildcategorytree(categoryTreeList, categoryTreeNode.getCode()));
                    categoryTreeNode.setState("closed");
                }
                categoryTrees.add(categoryTreeNode);
            }
        }
        return categoryTrees;
    }
}
