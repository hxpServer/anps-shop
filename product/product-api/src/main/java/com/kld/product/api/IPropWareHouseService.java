package com.kld.product.api;

import com.kld.product.po.PropWareHouse;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IPropWareHouseService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param propWareHouse
     * @return
     */
    int insert(PropWareHouse propWareHouse)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    PropWareHouse selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param propWareHouse
     * @return
     */
    int updateByPrimaryKey(PropWareHouse propWareHouse)throws Exception;

    /***
     * 查询集合
     * @param propWareHouse
     * @return
     */
    List<PropWareHouse> findPropWareHouseList(PropWareHouse propWareHouse)throws Exception;

}
