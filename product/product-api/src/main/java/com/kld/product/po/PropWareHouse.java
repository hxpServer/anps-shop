package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class PropWareHouse implements Serializable{
    private static final long serialVersionUID = -2332452061463963968L;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 仓库名称
     */
    private String wareName;

    /**
     * 仓库区域 省
     */
    private String provinceId;

    /**
     * 仓库区域 市
     */
    private String cityId;

    /**
     * 仓库 区县
     */
    private String countyId;

    /**
     * 仓库详细地址
     */
    private String address;

    /**
     * 库管姓名
     */
    private String keeper;

    /**
     * 库管电话
     */
    private String wareKeeperPhone;

    /**
     * 是否支持自提
     */
    private Integer isArayacak;

    /**
     * 服务范围(1:全国通用，2:指定地区使用)
     */
    private Integer svcScope;

    /**
     * 服务省范围
     */
    private String svcProvince;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 组织机构编码
     */
    private String ouCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWareName() {
        return wareName;
    }

    public void setWareName(String wareName) {
        this.wareName = wareName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getKeeper() {
        return keeper;
    }

    public void setKeeper(String keeper) {
        this.keeper = keeper;
    }

    public String getWareKeeperPhone() {
        return wareKeeperPhone;
    }

    public void setWareKeeperPhone(String wareKeeperPhone) {
        this.wareKeeperPhone = wareKeeperPhone;
    }

    public Integer getIsArayacak() {
        return isArayacak;
    }

    public void setIsArayacak(Integer isArayacak) {
        this.isArayacak = isArayacak;
    }

    public Integer getSvcScope() {
        return svcScope;
    }

    public void setSvcScope(Integer svcScope) {
        this.svcScope = svcScope;
    }

    public String getSvcProvince() {
        return svcProvince;
    }

    public void setSvcProvince(String svcProvince) {
        this.svcProvince = svcProvince;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOuCode() {
        return ouCode;
    }

    public void setOuCode(String ouCode) {
        this.ouCode = ouCode;
    }
}