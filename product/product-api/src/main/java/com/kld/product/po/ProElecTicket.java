package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProElecTicket implements Serializable{
    private static final long serialVersionUID = -8670366917664976132L;
    /**
     * ID
     */
    private Integer id;

    /**
     * 供应商ID
     */
    private Integer mctId;

    private String mctName;

    /**
     * 组织机构编码
     */
    private String ouCode;

    private int isDel;

    private Date createTime;

    private String creator;

    /**
     * 电子券类型名称
     */
    private String ticketName;

    /**
     * 电子券类型编码
     */
    private String code;

    /**
     * 电子券类型描述
     */
    private String remark;

    public String getMctName() {
        return mctName;
    }

    public void setMctName(String mctName) {
        this.mctName = mctName;
    }

    public int getIsDel() {
        return isDel;
    }

    public void setIsDel(int isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMctId() {
        return mctId;
    }

    public void setMctId(Integer mctId) {
        this.mctId = mctId;
    }

    public String getOuCode() {
        return ouCode;
    }

    public void setOuCode(String ouCode) {
        this.ouCode = ouCode;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}