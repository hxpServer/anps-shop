package com.kld.product.api;

import com.kld.product.po.ProSkus;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProSkusService {
    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proSkus
     * @return
     */
    int insert(ProSkus proSkus)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProSkus selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proSkus
     * @return
     */
    int updateByPrimaryKey(ProSkus proSkus)throws Exception;

    /***
     * 查询集合
     * @param proSkus
     * @return
     */
    List<ProSkus> findProSkusList(ProSkus proSkus)throws Exception;
}
