package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProPropValue implements Serializable{
    private static final long serialVersionUID = -5628670572001818277L;
    /**
     * 主键
     */
    private Integer proValueId;

    /**
     * 属性表_主键
     */
    private Integer propId;

    /**
     * 属性值名称
     */
    private String propValue;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态 0-禁用  1-启用
     */
    private Integer state;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;


    public Integer getProValueId() {
        return proValueId;
    }

    public void setProValueId(Integer proValueId) {
        this.proValueId = proValueId;
    }

    public Integer getPropId() {
        return propId;
    }

    public void setPropId(Integer propId) {
        this.propId = propId;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}