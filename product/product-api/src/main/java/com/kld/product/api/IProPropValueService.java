package com.kld.product.api;

import com.kld.product.po.ProPropValue;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProPropValueService {


    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proPropValue
     * @return
     */
    int insert(ProPropValue proPropValue)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProPropValue selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proPropValue
     * @return
     */
    int updateByPrimaryKey(ProPropValue proPropValue)throws Exception;

    /***
     * 查询集合
     * @param proPropValue
     * @return
     */
    List<ProPropValue> findProPropValueList(ProPropValue proPropValue)throws Exception;

}
