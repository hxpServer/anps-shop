package com.kld.product.dto;

import com.kld.product.po.ProProp;
import com.kld.product.po.ProPropValue;

import java.io.Serializable;
import java.util.List;

/**
 * Created by anpushang on 2016/4/6.
 */
public class PropertyDto implements Serializable {


    private static final long serialVersionUID = 1333500307265095104L;

    private ProProp proProp;

    private List<ProPropValue> proPropValueList;

    public ProProp getProProp() {
        return proProp;
    }

    public void setProProp(ProProp proProp) {
        this.proProp = proProp;
    }

    public List<ProPropValue> getProPropValueList() {
        return proPropValueList;
    }

    public void setProPropValueList(List<ProPropValue> proPropValueList) {
        this.proPropValueList = proPropValueList;
    }
}
