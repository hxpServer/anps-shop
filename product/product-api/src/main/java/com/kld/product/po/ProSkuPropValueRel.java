package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProSkuPropValueRel implements Serializable{
    private static final long serialVersionUID = 5383800073309064509L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 属性表_主键
     */
    private Integer propId;

    /**
     * 属性值_主键
     */
    private Integer propValued;

    /**
     * 商品id
     */
    private Integer proId;

    /**
     * 商品SKU id
     */
    private Integer skuId;

    /**
     * 属性名
     */
    private String propName;

    /**
     * 属性值
     */
    private String propValue;

    /**
     * 启用状态 0-禁用 1-启用
     */
    private Integer isUse;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPropId() {
        return propId;
    }

    public void setPropId(Integer propId) {
        this.propId = propId;
    }

    public Integer getPropValued() {
        return propValued;
    }

    public void setPropValued(Integer propValued) {
        this.propValued = propValued;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public Integer getIsUse() {
        return isUse;
    }

    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}