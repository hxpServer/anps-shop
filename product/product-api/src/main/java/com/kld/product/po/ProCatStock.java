package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProCatStock implements Serializable{
    private static final long serialVersionUID = 6545878039907332910L;
    /**
     * ID
     */
    private Integer id;

    /**
     * 卡劵号
     */
    private String cartNumber;

    /**
     * 商品ID
     */
    private Long proId;

    /**
     * 库存管理方
     */
    private Long stockManager;

    /**
     * 库存量
     */
    private Integer stockNum;

    /**
     * 导入时间
     */
    private Date expTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCartNumber() {
        return cartNumber;
    }

    public void setCartNumber(String cartNumber) {
        this.cartNumber = cartNumber;
    }

    public Long getProId() {
        return proId;
    }

    public void setProId(Long proId) {
        this.proId = proId;
    }

    public Long getStockManager() {
        return stockManager;
    }

    public void setStockManager(Long stockManager) {
        this.stockManager = stockManager;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Date getExpTime() {
        return expTime;
    }

    public void setExpTime(Date expTime) {
        this.expTime = expTime;
    }
}