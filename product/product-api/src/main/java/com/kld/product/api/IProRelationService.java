package com.kld.product.api;

import com.kld.product.po.ProRelation;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProRelationService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proRelation
     * @return
     */
    int insert(ProRelation proRelation)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProRelation selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proRelation
     * @return
     */
    int updateByPrimaryKey(ProRelation proRelation)throws Exception;

    /***
     * 查询集合
     * @param proRelation
     * @return
     */
    List<ProRelation> findProRelationList(ProRelation proRelation)throws Exception;
}
