package com.kld.product.api;

import com.github.pagehelper.PageInfo;
import com.kld.product.dto.PropertyDto;
import com.kld.product.po.ProProp;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProPropService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proProp
     * @return
     */
    int insert(ProProp proProp)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProProp selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proProp
     * @return
     */
    int updateByPrimaryKey(ProProp proProp)throws Exception;

    /***
     * 查询集合
     * @param proProp
     * @return
     */
    List<ProProp> findProProductList(ProProp proProp)throws Exception;

    /**
     * 判断是否可以删除
     * @param id
     * @return
     * @throws Exception
     */
    int getVerificationPassed(Integer id)throws Exception;


    PageInfo findProProductListPage(int pageNum, int pageSize, ProProp proProp)throws Exception;

    /**
     * 查询已选属性
     * @param cateId
     * @param isSpec
     * @return
     */
    List<ProProp> findPropByCateId(Integer cateId, int isSpec)throws Exception;

    /***
     * 根据分类ID查询属性信息
     * @param cateId
     * @return
     */
    List<PropertyDto> selectProp(Integer cateId,Integer isSpec)throws Exception;
}
