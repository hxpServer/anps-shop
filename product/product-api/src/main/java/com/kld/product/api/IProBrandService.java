package com.kld.product.api;

import com.github.pagehelper.PageInfo;
import com.kld.product.po.ProBrand;

import java.util.List;
import java.util.Map;

/**
 * des by Caoz 品牌
 * Created by anpushang on 2016/3/24.
 */
public interface IProBrandService {
    /***
     *  根据ID删除品牌
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proBrand
     * @return
     */
    int insert(ProBrand proBrand)throws Exception;

    /***
     * 根据ID查询品牌
     * @param id
     * @return
     */
    ProBrand selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改品牌
     * @param proBrand
     * @return
     */
    int updateByPrimaryKey(ProBrand proBrand)throws Exception;

    /***
     * 查询品牌集合
     * @param proBrand
     * @return
     */
    List<ProBrand> findProBrandList(ProBrand proBrand)throws Exception;

    PageInfo findProBrandListPage(int pageNum, int pageSize, ProBrand proBrand)throws Exception;;

    /***
     * 查询分类关联的品牌
     * @param cateId
     * @return
     */
    List<ProBrand> findProBrandByCateId(Integer cateId);
}
