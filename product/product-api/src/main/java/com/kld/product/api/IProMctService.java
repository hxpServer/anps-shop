package com.kld.product.api;

import com.kld.product.po.ProMct;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProMctService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proMct
     * @return
     */
    int insert(ProMct proMct)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProMct selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proMct
     * @return
     */
    int updateByPrimaryKey(ProMct proMct)throws Exception;

    /***
     * 查询集合
     * @param proMct
     * @return
     */
    List<ProMct> findProMctList(ProMct proMct)throws Exception;

}
