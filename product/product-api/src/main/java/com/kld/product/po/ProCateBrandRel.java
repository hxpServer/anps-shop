package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProCateBrandRel implements Serializable{
    private static final long serialVersionUID = 5133614305804304395L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 类目ID
     */
    private Integer cateId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;

    /**
     * 品牌ID
     */
    private Integer brandId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
}