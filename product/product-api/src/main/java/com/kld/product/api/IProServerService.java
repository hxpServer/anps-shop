package com.kld.product.api;

import com.kld.product.po.ProServer;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProServerService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proServer
     * @return
     */
    int insert(ProServer proServer)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProServer selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proServer
     * @return
     */
    int updateByPrimaryKey(ProServer proServer)throws Exception;

    /***
     * 查询集合
     * @param proServer
     * @return
     */
    List<ProServer> findProServerList(ProServer proServer)throws Exception;

    List<ProServer> selectByProId(Integer proId);
}
