package com.kld.product.api;

import com.github.pagehelper.PageInfo;
import com.kld.product.po.ProElecTicket;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProElecTicketService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proElecTicket
     * @return
     */
    int insert(ProElecTicket proElecTicket)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProElecTicket selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proElecTicket
     * @return
     */
    int updateByPrimaryKey(ProElecTicket proElecTicket)throws Exception;

    /***
     * 查询集合
     * @param proElecTicket
     * @return
     */
    List<ProElecTicket> findProElecTicketList(ProElecTicket proElecTicket)throws Exception;

     PageInfo findProElecTicketPageList(int pageNum,int pageSize,ProElecTicket proElecTicket)throws Exception;
}
