package com.kld.product.api;

import com.kld.product.po.ProBrand;
import com.kld.product.po.ProCateBrandRel;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProCateBrandRelService {

    int insert(ProCateBrandRel record)throws Exception;

    int insertList(List<ProCateBrandRel> proCateBrandRelList)throws Exception;

    int updateByPrimaryKey(ProCateBrandRel record)throws Exception;

    /***
     * 根据分类ID查询品牌信息
     * @param cateId
     * @return
     * @throws Exception
     */
    List<ProBrand> getBrandByCateId(Integer cateId)throws Exception;
}
