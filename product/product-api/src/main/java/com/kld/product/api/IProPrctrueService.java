package com.kld.product.api;

import com.kld.product.po.ProPrctrue;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProPrctrueService {
    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proPrctrue
     * @return
     */
    int insert(ProPrctrue proPrctrue)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProPrctrue selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proPrctrue
     * @return
     */
    int updateByPrimaryKey(ProPrctrue proPrctrue)throws Exception;

    /***
     * 查询集合
     * @param proPrctrue
     * @return
     */
    List<ProPrctrue> findProPrctrueList(ProPrctrue proPrctrue)throws Exception;

    List<ProPrctrue> selectByProId(Integer proId)throws Exception;
}
