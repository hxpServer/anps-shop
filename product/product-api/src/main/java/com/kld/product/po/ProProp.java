package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ProProp implements Serializable{
    private static final long serialVersionUID = 5436174440528388903L;
    /**
     * 主键标识
     */
    private Integer id;

    private List<ProPropValue> proPropValueList;
    /***
     * 用于前台显示
     */
    private String proPropValue;

    /**
     * 属性名称
     */
    private String propName;

    /**
     * 显示顺序
     */
    private Integer sort;

    /**
     * 选择类型 0-文本框 1-单选 2-多选
     */
    private Integer propType;

    /**
     * 是否规格属性 0-否 1-是
     */
    private Integer isSpec;

    /**
     * 是否必填 0-否 1-是
     */
    private Integer isNeed;

    /**
     * 状态 0-禁用 1-启用
     */
    private Integer state;

    /**
     * 操作时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 修改时间
     */
    private String modifyTime;

    /**
     * 修改人
     */
    private String modifier;

    private String[] sorts;

    private String [] propertyValues;

    private Integer[] proValueIdList;

    public String getProPropValue() {

        return proPropValue;
    }

    public void setProPropValue(String proPropValue) {
        this.proPropValue = proPropValue;
    }

    public Integer[] getProValueIdList() {
        return proValueIdList;
    }

    public void setProValueIdList(Integer[] proValueIdList) {
        this.proValueIdList = proValueIdList;
    }

    public String[] getPropertyValues() {
        return propertyValues;
    }

    public void setPropertyValues(String[] propertyValues) {
        this.propertyValues = propertyValues;
    }

    public String[] getSorts() {
        return sorts;
    }

    public void setSorts(String[] sorts) {
        this.sorts = sorts;
    }

    public List<ProPropValue> getProPropValueList() {
        return proPropValueList;
    }

    public void setProPropValueList(List<ProPropValue> proPropValueList) {
        this.proPropValueList = proPropValueList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getPropType() {
        return propType;
    }

    public void setPropType(Integer propType) {
        this.propType = propType;
    }

    public Integer getIsSpec() {
        return isSpec;
    }

    public void setIsSpec(Integer isSpec) {
        this.isSpec = isSpec;
    }

    public Integer getIsNeed() {
        return isNeed;
    }

    public void setIsNeed(Integer isNeed) {
        this.isNeed = isNeed;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

}