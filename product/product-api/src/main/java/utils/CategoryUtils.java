package utils;

import com.kld.product.dto.CategoryDto;
import com.kld.product.po.ProCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anpushang on 2016/3/31.
 */
public class CategoryUtils {

    public static List<CategoryDto> buildCategoryTree(List<ProCategory> categoryTreeList, Integer parentCode) {
        List<CategoryDto> categoryTrees = new ArrayList<CategoryDto>();
        for (ProCategory proCategory : categoryTreeList) {
            CategoryDto category = new CategoryDto();
            category.setId(proCategory.getId());
            category.setCateName(proCategory.getCateName());
            category.setPicImg(proCategory.getPicImg());
            category.setRemark(proCategory.getRemark());
            category.setLev(proCategory.getLev());
            category.setpId(proCategory.getpId());
            category.setSort(proCategory.getSort());
            category.setHasChild(proCategory.getHasChild());
            category.setReCommend(proCategory.getReCommend());
            category.setCateState(proCategory.getState());
            if (parentCode == proCategory.getpId()) {
                if (category.getHasChild() != 0) {
                    category.setChildren(buildCategoryTree(categoryTreeList, category.getId()));
                    category.setCategoryState("closed");
                }
                categoryTrees.add(category);
            }
        }
        return categoryTrees;
    }
}
