package com.kld.sys;

import com.kld.product.api.IProBrandService;
import com.kld.product.po.ProBrand;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public class ProBrandTest {

    private ClassPathXmlApplicationContext context;

    @Test
    public void save()throws Exception{
        context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
        context.start();
        IProBrandService proBrandService = (IProBrandService)context.getBean("proBrandService") ;
        ProBrand proBrand = new ProBrand();
        proBrand.setBrandName("苹果");
        proBrandService.insert(proBrand);

    }

    @Test
    public void update()throws Exception{
        context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
        context.start();
        IProBrandService proBrandService = (IProBrandService)context.getBean("proBrandService") ;
        ProBrand proBrand = new ProBrand();
        proBrand.setId(2);
        proBrand.setRemark("2323");
        proBrand.setState(1);
        proBrandService.updateByPrimaryKey(proBrand);
    }

    @Test
    public void find()throws Exception{
        context = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
        context.start();
        IProBrandService proBrandService = (IProBrandService)context.getBean("proBrandService") ;
        ProBrand proBrand = new ProBrand();
        proBrand.setState(1);
        List<ProBrand> proBrandList = proBrandService.findProBrandList(proBrand);
        for (ProBrand brand : proBrandList) {
            System.out.print(brand.getBrandName());
        }

    }
}
