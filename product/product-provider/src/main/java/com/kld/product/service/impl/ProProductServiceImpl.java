package com.kld.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.product.api.IProProductService;
import com.kld.product.dao.ProPrctrueDao;
import com.kld.product.dao.ProProductDao;
import com.kld.product.dao.ProServerDao;
import com.kld.product.dto.ProductDto;
import com.kld.product.po.ProPrctrue;
import com.kld.product.po.ProProduct;
import com.kld.product.po.ProServer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProProductServiceImpl implements IProProductService {

    @Autowired
    private ProProductDao proProductDao;
    @Autowired
    private ProServerDao proServerDao;
    @Autowired
    private ProPrctrueDao proPrctrueDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proProductDao.delete(id);
    }

    @Override
    public int insert(ProProduct proProduct) throws Exception {
        return proProductDao.insert(proProduct);
    }

    @Override
    public ProProduct selectByPrimaryKey(Integer id) throws Exception {
        return proProductDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProProduct proProduct) throws Exception {
        return proProductDao.update(proProduct);
    }

    @Override
    public List<ProProduct> findProProductList(ProProduct proProduct) throws Exception {
        return proProductDao.find(proProduct);
    }

    @Override
    public int findCategoryIsProductById(int cateId) {
        return proProductDao.get("findCategoryIsProductById",cateId);
    }


    @Override
    public int insertProduct(ProductDto productDto,String[] imgUrl) throws Exception {
        ProProduct proProduct = productDto.getProProduct();//商品信息
        ProServer proServer = productDto.getProServer();//商品服务
        proProduct.setPicUrl(imgUrl[0]);//设置商品主图
        proProduct.setIsDel(1);
        proProduct.setState(1);
        int count = proProductDao.insert(proProduct);
        if (count > 0){
            List<ProServer> proServerList = new ArrayList<ProServer>();
            Integer hxSvcId = productDto.getHxSvcId();
            if (hxSvcId !=null){//换货服务
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(hxSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.sethXInDate(proServer.gethXInDate());
                proServerList.add(proServer1);
            }
            Integer txSvcId = productDto.getTxSvcId();//退货
            if (txSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(txSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.settHInDate(proServer.gettHInDate());
                proServerList.add(proServer1);
            }
            Integer wxSvcId = productDto.getWxSvcId();//维修
            if (wxSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(wxSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.setMainTain(proServer.getMainTain());
                proServerList.add(proServer1);
            }

            Integer kdysSvcId = productDto.getKdysSvcId();//快递运输
            if (kdysSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(kdysSvcId);
                proServer1.setSvcType(2);
                proServer1.setProId(proProduct.getId());
                proServer1.setProcessMode(proServer.getProcessMode());
                proServerList.add(proServer1);
            }
            Integer smztSvcId = productDto.getSmztSvcId(); //上门自提
            if (smztSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(smztSvcId);
                proServer1.setSvcType(2);
                proServer1.setProcessMode(proServer.getProcessMode());
                proServer1.setProcessTime(proServer.getProcessTime());//最少自提时间
                proServer1.setProId(proProduct.getId());
                proServerList.add(proServer1);
            }
            if (!proServerList.isEmpty()){
                proServerDao.insert(proServerList);
            }



            //商品图片设置
            List<ProPrctrue> proPrctrueList = new ArrayList<ProPrctrue>();
            for(int x=1;x < imgUrl.length;x++){
                ProPrctrue prctrue = new ProPrctrue();
                prctrue.setCreateTime(new Date());
                prctrue.setProUrl(imgUrl[x]);
                prctrue.setSort(x);
                prctrue.setStatus(1);
                prctrue.setProId(proProduct.getId());//商品ID
                proPrctrueList.add(prctrue);
            }

            return proPrctrueDao.insert(proPrctrueList);
        }else{
            return -1;
        }
    }

    @Override
    public int updateProById(ProductDto productDto, String[] imgUrl) throws Exception {

        ProProduct proProduct = productDto.getProProduct();//商品信息
        ProServer proServer = productDto.getProServer();//商品服务
        proProduct.setPicUrl(imgUrl[0]);//设置商品主图
        int count = proProductDao.update(proProduct);
        if (count > 0){

            //删除商品和服务表的关系
            int resultCount = proServerDao.delete("deleteByProId",proProduct.getId());

            List<ProServer> proServerList = new ArrayList<ProServer>();
            Integer hxSvcId = productDto.getHxSvcId();
            if (hxSvcId !=null){//换货服务
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(hxSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.sethXInDate(proServer.gethXInDate());
                proServerList.add(proServer1);
            }
            Integer txSvcId = productDto.getTxSvcId();//退货
            if (txSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(txSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.settHInDate(proServer.gettHInDate());
                proServerList.add(proServer1);
            }
            Integer wxSvcId = productDto.getWxSvcId();//维修
            if (wxSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(wxSvcId);
                proServer1.setSvcType(1);
                proServer1.setProId(proProduct.getId());
                proServer1.setMainTain(proServer.getMainTain());
                proServerList.add(proServer1);
            }

            Integer kdysSvcId = productDto.getKdysSvcId();//快递运输
            if (kdysSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(kdysSvcId);
                proServer1.setSvcType(2);
                proServer1.setProId(proProduct.getId());
                proServer1.setProcessMode(proServer.getProcessMode());
                proServerList.add(proServer1);
            }
            Integer smztSvcId = productDto.getSmztSvcId(); //上门自提
            if (smztSvcId!=null){
                ProServer proServer1 = new ProServer();
                proServer1.setSvcId(smztSvcId);
                proServer1.setSvcType(2);
                proServer1.setProcessMode(proServer.getProcessMode());
                proServer1.setProcessTime(proServer.getProcessTime());//最少自提时间
                proServer1.setProId(proProduct.getId());
                proServerList.add(proServer1);
            }
            if (!proServerList.isEmpty()){
                proServerDao.insert(proServerList);
            }

            proPrctrueDao.delete("deleteByProId",proProduct.getId());

            //商品图片设置
            List<ProPrctrue> proPrctrueList = new ArrayList<ProPrctrue>();
            for(int x=1;x < imgUrl.length;x++){
                ProPrctrue prctrue = new ProPrctrue();
                prctrue.setCreateTime(new Date());
                prctrue.setProUrl(imgUrl[x]);
                prctrue.setSort(x);
                prctrue.setStatus(1);
                prctrue.setProId(proProduct.getId());//商品ID
                proPrctrueList.add(prctrue);
            }

            return proPrctrueDao.insert(proPrctrueList);
        }else{
            return -1;
        }
    }

    @Override
    public PageInfo findProProductListPage(int pageNum, int pageSize, ProProduct proProduct) throws Exception {
        PageHelper.startPage(pageNum, pageSize);
        List<ProProduct> proProductList = findProProductList(proProduct);
        PageInfo pageInfo = new PageInfo(proProductList);
        return pageInfo;
    }


}
