package com.kld.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.product.api.IProElecTicketService;
import com.kld.product.dao.ProElecTicketDao;
import com.kld.product.po.ProElecTicket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProElecTicketServiceImpl implements IProElecTicketService {

    @Autowired
    private ProElecTicketDao proElecTicketDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proElecTicketDao.delete(id);
    }

    @Override
    public int insert(ProElecTicket proElecTicket) throws Exception {
        return proElecTicketDao.insert(proElecTicket);
    }

    @Override
    public ProElecTicket selectByPrimaryKey(Integer id) throws Exception {
        return proElecTicketDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProElecTicket proElecTicket) throws Exception {
        return proElecTicketDao.update(proElecTicket);
    }

    @Override
    public List<ProElecTicket> findProElecTicketList(ProElecTicket proElecTicket) throws Exception {
        return proElecTicketDao.find(proElecTicket);
    }

    @Override
    public PageInfo findProElecTicketPageList(int pageNum, int pageSize, ProElecTicket proElecTicket) throws Exception {
        PageHelper.startPage(pageNum, pageSize);
        List<ProElecTicket> proElecTicketList = proElecTicketDao.find(proElecTicket);
        PageInfo pageInfo = new PageInfo(proElecTicketList);
        return pageInfo;
    }
}
