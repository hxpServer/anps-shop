package com.kld.product.service.impl;

import com.kld.product.api.IProPropValueService;
import com.kld.product.dao.ProPropValueDao;
import com.kld.product.po.ProPropValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProPropValueServiceImpl implements IProPropValueService {

    @Autowired
    private ProPropValueDao proPropValueDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proPropValueDao.delete(id);
    }

    @Override
    public int insert(ProPropValue proPropValue) throws Exception {
        return proPropValueDao.insert(proPropValue);
    }

    @Override
    public ProPropValue selectByPrimaryKey(Integer id) throws Exception {
        return proPropValueDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProPropValue proPropValue) throws Exception {
        return proPropValueDao.update(proPropValue);
    }

    @Override
    public List<ProPropValue> findProPropValueList(ProPropValue proPropValue) throws Exception {
        return proPropValueDao.find(proPropValue);
    }
}
