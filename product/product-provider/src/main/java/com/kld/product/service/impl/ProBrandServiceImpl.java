package com.kld.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.product.api.IProBrandService;
import com.kld.product.dao.ProBrandDao;
import com.kld.product.po.ProBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProBrandServiceImpl implements IProBrandService {

    @Autowired
    private ProBrandDao proBrandDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proBrandDao.delete(id);
    }

    @Override
    public int insert(ProBrand proBrand) throws Exception {
        return proBrandDao.insert(proBrand);
    }

    @Override
    public ProBrand selectByPrimaryKey(Integer id) throws Exception {
        return proBrandDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProBrand proBrand) throws Exception {
        return proBrandDao.update(proBrand);
    }

    @Override
    public List<ProBrand> findProBrandList(ProBrand proBrand) throws Exception {
        return proBrandDao.find("findProBrandList",proBrand);
    }

    @Override
    public PageInfo findProBrandListPage(int pageNum, int pageSize, ProBrand proBrand) throws Exception {
        PageHelper.startPage(pageNum, pageSize);
        List<ProBrand> proBrandList = findProBrandList(proBrand);
        PageInfo pageInfo = new PageInfo(proBrandList);
        return pageInfo;
    }

    @Override
    public List<ProBrand> findProBrandByCateId(Integer cateId) {
        return proBrandDao.find("findProBrandByCateId",cateId);
    }
}
