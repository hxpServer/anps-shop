package com.kld.product.service.impl;

import com.kld.product.api.IProCatStockService;
import com.kld.product.dao.ProCatStockDao;
import com.kld.product.po.ProCatStock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProCatStockServiceImpl implements IProCatStockService {

    @Autowired
    private ProCatStockDao proCatStockDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proCatStockDao.delete(id);
    }

    @Override
    public int insert(ProCatStock proCatStock) throws Exception {
        return proCatStockDao.insert(proCatStock);
    }

    @Override
    public ProCatStock selectByPrimaryKey(Integer id) throws Exception {
        return proCatStockDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProCatStock proCatStock) throws Exception {
        return proCatStockDao.update(proCatStock);
    }

    @Override
    public List<ProCatStock> findProCatStockList(ProCatStock proCatStock) throws Exception {
        return proCatStockDao.find(proCatStock);
    }
}
