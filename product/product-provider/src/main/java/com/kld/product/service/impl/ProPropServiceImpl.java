package com.kld.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.product.api.IProPropService;
import com.kld.product.dao.ProPropDao;
import com.kld.product.dao.ProPropValueDao;
import com.kld.product.dto.PropertyDto;
import com.kld.product.po.ProProp;
import com.kld.product.po.ProPropValue;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProPropServiceImpl implements IProPropService {

    @Autowired
    private ProPropDao proPropDao;
    @Autowired
    private ProPropValueDao proPropValueDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        proPropDao.delete(id);
        return proPropValueDao.delete("deleteByPropId",id);
    }

    @Override
    public int insert(ProProp proProp) throws Exception {
        //新增属性
        int resultId = proPropDao.insert(proProp);
        if (resultId > 0){
            if(proProp.getPropertyValues()!= null && proProp.getSorts()!= null){
                String[] sorts = proProp.getSorts();
                String[] propertyValues = proProp.getPropertyValues();
                ProPropValue proPropValue = new ProPropValue();
                List<ProPropValue> proPropValueList = new ArrayList<ProPropValue>();
                for(int i=0;i < proProp.getPropertyValues().length;i++){
                    proPropValue.setPropId(proProp.getId());
                    proPropValue.setState(1);
                    proPropValue.setPropValue(propertyValues[i]);
                    if(!StringUtils.isEmpty(sorts[i])){//判断排序是否为空
                        proPropValue.setSort(Integer.parseInt(sorts[i]));
                    }else{
                        proPropValue.setSort(0);
                    }
                    proPropValueList.add(proPropValue);
                }
                return proPropValueDao.insert(proPropValueList);
            }else {
                return -1;
            }
        }else{
            return -1;
        }
    }

    @Override
    public ProProp selectByPrimaryKey(Integer id) throws Exception {
        return proPropDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProProp proProp) throws Exception {

        String[] propertyValues = proProp.getPropertyValues();
        ProPropValue proPropValue = new ProPropValue();
        proPropValue.setPropId(proProp.getId());
        //属性关联的属性值
        List<ProPropValue> propValueList = proPropValueDao.find("findByPropId", proPropValue);
        int size = propValueList.size();
        Integer[] ids = new Integer[propertyValues.length];
        for(int i=0;i<size;i++){
            ids[i] = propValueList.get(i).getProValueId(); //原来ID赋值
        }
        int countSize = propertyValues.length - size ;
        for (int i = size;i <= countSize;i++){
            System.out.print("iii"+i);
            ids[i]= 0;
        }
        proProp.setProValueIdList(ids);

        int resultCount = proPropDao.update(proProp);

        if (resultCount > 0){
            String[] sorts = proProp.getSorts();
            if(proProp.getPropertyValues()!= null && sorts!= null){
                for(int i=0;i<proProp.getPropertyValues().length;i++){
                    Integer proValId = proProp.getProValueIdList()[i];
                    ProPropValue propValue = new ProPropValue();
                    propValue.setProValueId(proValId);
                    propValue.setPropId(proProp.getId());
                    propValue.setPropValue(proProp.getPropertyValues()[i]);
                    propValue.setSort(Integer.parseInt(sorts[i]));
                    if(null != proValId && proValId != 0){
                        proPropValueDao.update(propValue);
                    } else {
                        proPropValueDao.insert(propValue);
                    }
                }
                return resultCount;
            }else {
                return -1;
            }
        }else{
            return -1;
        }

    }

    @Override
    public List<ProProp> findProProductList(ProProp proProp) throws Exception {
        return proPropDao.find(proProp);
    }


    @Override
    public int getVerificationPassed(Integer id) throws Exception {
        return proPropDao.get("getVerificationPassed", id);
    }

    @Override
    public PageInfo findProProductListPage(int pageNum, int pageSize, ProProp proProp) throws Exception {
        PageHelper.startPage(pageNum, pageSize);
        List<ProProp> proProductList = findProProductList(proProp);
        for (ProProp prop : proProductList) {
            List<ProPropValue> proPropValueList = prop.getProPropValueList();
            if (proPropValueList !=null){
                String str = "";
                for (ProPropValue proPropValue : proPropValueList) {
                    str += proPropValue.getPropValue()+",";
                }
                prop.setProPropValue(str);
            }
        }


        PageInfo pageInfo = new PageInfo(proProductList);
        return pageInfo;
    }



    @Override
    public List<ProProp> findPropByCateId(Integer cateId, int isSpec) {
        Map<String,Object> map = new ConcurrentHashMap<String,Object>();
        map.put("cateId",cateId);
        map.put("isSpec",isSpec);
        return proPropDao.find("findPropByCateId",map);
    }



    @Override
    public List<PropertyDto> selectProp(Integer cateId,Integer isSpec) throws Exception {
        Map<String,Object> map = new ConcurrentHashMap<String,Object>();
        map.put("cateId",cateId);
        map.put("isSpec",isSpec);
        List<ProProp> proPropList = proPropDao.find("selectPropByCateId",map);
        List<PropertyDto> propertyDtoList = new ArrayList<PropertyDto>();
        for (ProProp proProp : proPropList) {
            List<ProPropValue> proPropValueList = proPropValueDao.find("selectPropByPropId",proProp.getId());
            PropertyDto propertyDto = new PropertyDto();
            propertyDto.setProProp(proProp);
            propertyDto.setProPropValueList(proPropValueList);
            propertyDtoList.add(propertyDto);
        }
        return propertyDtoList;
    }
}
