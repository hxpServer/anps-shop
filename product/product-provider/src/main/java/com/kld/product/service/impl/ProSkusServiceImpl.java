package com.kld.product.service.impl;

import com.kld.product.api.IProSkusService;
import com.kld.product.dao.ProSkusDao;
import com.kld.product.po.ProSkus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProSkusServiceImpl implements IProSkusService {

    @Autowired
    private ProSkusDao proSkusDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proSkusDao.delete(id);
    }

    @Override
    public int insert(ProSkus proSkus) throws Exception {
        return proSkusDao.insert(proSkus);
    }

    @Override
    public ProSkus selectByPrimaryKey(Integer id) throws Exception {
        return proSkusDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProSkus proSkus) throws Exception {
        return proSkusDao.update(proSkus);
    }

    @Override
    public List<ProSkus> findProSkusList(ProSkus proSkus) throws Exception {
        return proSkusDao.find(proSkus);
    }
}
