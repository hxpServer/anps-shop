package com.kld.product.service.impl;

import com.kld.product.api.IProRelationService;
import com.kld.product.dao.ProRelationDao;
import com.kld.product.po.ProRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProRelationServiceImpl implements IProRelationService {

    @Autowired
    private ProRelationDao proRelationDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proRelationDao.delete(id);
    }

    @Override
    public int insert(ProRelation proRelation) throws Exception {
        return proRelationDao.insert(proRelation);
    }

    @Override
    public ProRelation selectByPrimaryKey(Integer id) throws Exception {
        return proRelationDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProRelation proRelation) throws Exception {
        return proRelationDao.update(proRelation);
    }

    @Override
    public List<ProRelation> findProRelationList(ProRelation proRelation) throws Exception {
        return proRelationDao.find(proRelation);
    }
}
